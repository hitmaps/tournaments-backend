using API.Filters;
using API.Hubs;
using BusinessLogic;
using BusinessLogic.Batch.CloseRegistrations;
using BusinessLogic.Batch.SendMatchStartingNotifications;
using BusinessLogic.BracketManager;
using BusinessLogic.Challonge;
using BusinessLogic.Discord;
using BusinessLogic.Hitmaps;
using BusinessLogic.Hitmaps.GroupStage;
using BusinessLogic.Hitmaps.MatchAdministration;
using BusinessLogic.Hitmaps.Statistics;
using BusinessLogic.Hitmaps.Swiss;
using BusinessLogic.Hitmaps.Swiss.Standings;
using BusinessLogic.RrStats;
using BusinessLogic.SignalR;
using Challonge.Extensions.DependencyInjection;
using DataAccess.Data;
using DataAccess.Repositories;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Rollbar;
using Rollbar.NetCore.AspNet;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddApiVersioning(options =>
{
    options.DefaultApiVersion = new ApiVersion(1, 0);
    options.ReportApiVersions = true;
    options.AssumeDefaultVersionWhenUnspecified = true;
    options.ApiVersionReader = new HeaderApiVersionReader("x-api-version");
});
builder.Services.AddDbContext<TournamentDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("Tournament")));
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddSingleton<IDiscordService, DiscordService>();
builder.Services.AddSingleton<IChallongeService, ChallongeService>();
builder.Services.AddSingleton<HttpClientWrapper>();
builder.Services.AddSingleton<ThreadSafeExecutor>();
builder.Services.AddScoped<IRegistrationService, RegistrationService>();
builder.Services.AddScoped<IEventService, EventService>();
builder.Services.AddScoped<IBracketService, BracketService>();
builder.Services.AddScoped<IStageService, StageService>();
builder.Services.AddScoped<IGroupStageService, GroupStageService>();
builder.Services.AddScoped<ICompetitorService, CompetitorService>();
builder.Services.AddScoped<IMatchupService, MatchupService>();
builder.Services.AddScoped<IRrStatsService, RrStatsService>();
builder.Services.AddScoped<IVisualBracketsService, VisualBracketsService>();
builder.Services.AddScoped<IChallongeStandingsBuilder, ChallongeStandingsBuilder>();
builder.Services.AddScoped<IStatisticsService, StatisticsService>();
builder.Services.AddScoped<IMatchHistoryService, MatchHistoryService>();
builder.Services.AddScoped<IMatchAdminMatchupService, MatchAdminMatchupService>();
builder.Services.AddScoped<IMatchHubService, MatchHubService>();
builder.Services.AddScoped<IBracketManagerService, BracketManagerService>();
builder.Services.AddScoped<ISwissService, SwissService>();
builder.Services.AddScoped<MatchupRepository>();
builder.Services.AddScoped<SwissStandingsBuilder>();
builder.Services.AddChallonge(builder.Configuration["ChallongeUsername"], builder.Configuration["ChallongeApiKey"]);
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(o => o.AddPolicy("CorsPolicy", x => 
    x.AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials()
        .SetIsOriginAllowed(_ => true)));
builder.Services.AddHangfire(configuration => configuration
    .UseFilter(new AutomaticRetryAttribute { Attempts = 0 })
    .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
    .UseSimpleAssemblyNameTypeSerializer()
    .UseRecommendedSerializerSettings()
    .UseSqlServerStorage(builder.Configuration.GetConnectionString("Tournament"), new SqlServerStorageOptions
    {
        CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
        SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
        QueuePollInterval = TimeSpan.Zero,
        UseRecommendedIsolationLevel = true,
        DisableGlobalLocks = true,
        SchemaName = "Hangfire"
    }));
builder.Services.AddHangfireServer();

ConfigureRollbarInfrastructure();
builder.Services.AddRollbarLogger(loggerOptions =>
{
    loggerOptions.Filter =
        (_, loglevel) => loglevel >= LogLevel.Warning;
});
builder.Services.AddSignalR().AddJsonProtocol();

var app = builder.Build();

app.UseCors("CorsPolicy");

app.MapHub<MatchHub>("/ws/match-hub");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHangfireDashboard(options: new DashboardOptions
{
    IsReadOnlyFunc = _ => true,
    Authorization = new []
    {
        new AllowAllHangfireFilter()
    }
});

if (app.Environment.IsProduction())
{
    app.UseRollbarMiddleware();
}

// Apply EF migrations if in production, as we use command line otherwise
if (app.Environment.IsProduction())
{
    using var scope = app.Services.CreateScope();
    var db = scope.ServiceProvider.GetRequiredService<TournamentDbContext>();
    db.Database.Migrate();
    app.UseExceptionHandler("/error");
    
    // Enable recurring jobs for hangfire
    var discordService = scope.ServiceProvider.GetRequiredService<IDiscordService>();
    RecurringJob.AddOrUpdate("hitmaps-tournaments:Batch.SendMatchStartingNotifications",
        () => new SendMatchStartingNotifications(db, discordService).ProgramLogicAsync(), 
        Cron.Minutely);
    RecurringJob.AddOrUpdate("hitmaps-tournaments:Batch.CloseRegistrations",
        () => new CloseRegistrations(db, discordService).ProgramLogicAsync(),
        Cron.Minutely);
}
else
{
    RecurringJob.RemoveIfExists("hitmaps-tournaments:Batch.SendMatchStartingNotifications");
    RecurringJob.RemoveIfExists("hitmaps-tournaments:Batch.CloseRegistrations");
}

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

void ConfigureRollbarInfrastructure()
{
    RollbarInfrastructureConfig config = new RollbarInfrastructureConfig(
        builder.Configuration["RollbarAccessToken"],
        builder.Configuration["development"]
    );
    RollbarDataSecurityOptions dataSecurityOptions = new RollbarDataSecurityOptions();
    dataSecurityOptions.ScrubFields = new[]
    {
        "url",
        "method",
    };
    config.RollbarLoggerConfig.RollbarDataSecurityOptions.Reconfigure(dataSecurityOptions);

    RollbarInfrastructure.Instance.Init(config);
}