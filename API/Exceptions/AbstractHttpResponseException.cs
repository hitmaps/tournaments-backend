namespace API.Exceptions;

public abstract class AbstractHttpResponseException : Exception
{
    public abstract string GetMessage();

    public abstract int GetStatusCode();
}