using BusinessLogic.Hitmaps;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class MatchHistoryController : ApiControllerBase
{
    private readonly IMatchHistoryService matchHistoryService;

    public MatchHistoryController(IMatchHistoryService matchHistoryService)
    {
        this.matchHistoryService = matchHistoryService;
    }

    [HttpGet("events/{eventSlug}/participants/{discordId}/match-history")]
    public async Task<IActionResult> MatchHistory(string eventSlug, ulong discordId)
    {
        return Ok(await matchHistoryService.GetMatchHistoryAsync(eventSlug, discordId));
    }
}