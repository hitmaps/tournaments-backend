using BusinessLogic.Discord;
using BusinessLogic.Hitmaps;
using BusinessLogic.Models;
using BusinessLogic.Models.UpcomingMatches;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class EventController : ApiControllerBase
{
    private readonly IRegistrationService registrationService;
    private readonly IEventService eventService;
    private readonly IDiscordService discordService;
    private readonly IMatchupService matchupService;
    private readonly IVisualBracketsService visualBracketsService;

    public EventController(IRegistrationService registrationService,
        IEventService eventService,
        IDiscordService discordService,
        IMatchupService matchupService, 
        IVisualBracketsService visualBracketsService)
    {
        this.registrationService = registrationService;
        this.eventService = eventService;
        this.discordService = discordService;
        this.matchupService = matchupService;
        this.visualBracketsService = visualBracketsService;
    }

    [HttpGet("events")]
    public IActionResult GetAllEvents()
    {
        return Ok(eventService.GetAllEvents().Select(EventViewModel.FromDbModel).ToList());
    }

    [HttpGet("events/{slug}")]
    public IActionResult GetSpecificEvent(string slug)
    {
        const EventIncludes flags = EventIncludes.Brackets | 
                                    EventIncludes.Stages |
                                    EventIncludes.MapPool | 
                                    EventIncludes.BracketPointsForVictories | 
                                    EventIncludes.DiscordAdminRoleIds |
                                    EventIncludes.DetachEntity;
        var theEvent = eventService.GetEventForSlug(slug, flags);
        // The things we have to do to keep things snappy / performant.
        var eventWithOnlyCompetitors =
            eventService.GetEventForSlug(slug, EventIncludes.Competitors | EventIncludes.DetachEntity);
        theEvent.Brackets.ForEach(bracket => bracket.Competitors = eventWithOnlyCompetitors.Brackets.First(y => y.Id == bracket.Id).Competitors);
        return Ok(EventWithDetailsViewModel.FromDbModel(theEvent));
    }

    #region Registration / Withdrawal
    [HttpGet("events/{eventSlug}/@me")]
    [DiscordAuth]
    public async Task<IActionResult> FetchUserInfo(string eventSlug)
    {
        var discordSnowflake = (ulong)RouteData.Values["userSnowflake"]!;
        var theEvent = eventService.GetEventForSlug(eventSlug, EventIncludes.DiscordAdminRoleIds);
        var competitors = eventService.GetCompetitorsForEventAndSnowflake(eventSlug, discordSnowflake);
        var userInfo = await discordService.GetUserForGuildAsync(theEvent.DiscordGuildId, discordSnowflake);
        var connections = discordService.FetchUserConnectionsAsync(
            (string)RouteData.Values["userTokenType"]!,
            (string)RouteData.Values["userTokenValue"]!);

        return Ok(userInfo == null
            ? UserInfoViewModel.Null
            : new UserInfoViewModel
            {
                Snowflake = discordSnowflake.ToString(),
                Name = userInfo.Username,
                Discriminator = userInfo.Discriminator,
                ServerMember = true,
                RegisteredBrackets = competitors.Select(x => BracketViewModel.FromDbModel(x.Bracket)).ToList(),
                Connections = await connections,
                ServerRoles = userInfo.RoleIds.Select(x => x.ToString()).ToList(),
                TourneyBanned = theEvent.DiscordBannedCompetitorRoleId.HasValue && 
                                userInfo.RoleIds.Contains(theEvent.DiscordBannedCompetitorRoleId.Value),
                EventAdmin = userInfo.RoleIds.Any(userRole => theEvent.DiscordAdminRoleIds.Select(x => x.DiscordRoleId).Contains(userRole.ToString()))
            });
    }

    [HttpPost("events/{eventSlug}/register")]
    [DiscordAuth]
    public async Task<IActionResult> Register(string eventSlug, [FromBody] RegisterPlayerViewModel viewModel)
    {
        var discordSnowflake = (ulong)RouteData.Values["userSnowflake"]!;
        var competitor = await registrationService.RegisterUserAsync(eventSlug, viewModel, discordSnowflake);
        
        return new ObjectResult(CompetitorViewModel.FromDbModel(competitor))
        {
            StatusCode = StatusCodes.Status201Created
        };
    }

    [HttpPost("events/{eventSlug}/withdraw")]
    [DiscordAuth]
    public async Task<IActionResult> Withdraw(string eventSlug, [FromBody] WithdrawPlayerViewModel viewModel)
    {
        var discordSnowflake = (ulong)RouteData.Values["userSnowflake"]!;
        await registrationService.WithdrawUserAsync(eventSlug, viewModel, discordSnowflake);

        return NoContent();
    }
    #endregion

    [HttpGet("events/{eventSlug}/upcoming-matches")]
    [DiscordAuth(Required = false)]
    public async Task<IActionResult> GetUpcomingMatchups(string eventSlug)
    {
        var upcomingMatches = matchupService.GetUpcomingMatches(eventSlug);
        var avatarUrls = await discordService.GetUsersAsync(
            eventService.GetEventForSlug(eventSlug, EventIncludes.SplitQuery).DiscordGuildId);
        return Ok(new
        {
            Data = upcomingMatches
                .OrderBy(x => x.MatchScheduledAt)
                .Select(x => UpcomingMatchViewModel.FromDbModel(x, avatarUrls))
                .ToList()
        });
    }

    [HttpGet("events/{eventSlug}/casters")]
    public async Task<IActionResult> GetCastersForEvent(string eventSlug)
    {
        var discordUsers = await eventService.GetShoutcastersForEventAsync(eventSlug);
        var userList = discordUsers.Select(entry => ShoutcasterViewModel.FromDictionaryEntry(entry.Key, entry.Value))
            .ToList();
        userList.Sort((x, y) => string.Compare(x.DiscordUser.Name, 
            y.DiscordUser.Name, 
            StringComparison.OrdinalIgnoreCase));

        return Ok(new
        {
            Data = userList
        });
    }

    [HttpGet("events/{eventSlug}/admins")]
    public async Task<IActionResult> GetMatchAdminsForEvent(string eventSlug)
    {
        var theEvent = eventService.GetEventForSlug(eventSlug, EventIncludes.DiscordAdminRoleIds);

        return Ok(new
        {
            Data = (await discordService.GetUsersWithRoleAsync(theEvent.DiscordGuildId,
                    theEvent.DiscordAdminRoleIds.Select(x => x.DiscordRoleId).ToList()))
                .Select(DiscordUserViewModel.FromRestGuildUser)
                .OrderBy(x => x.Name)
                .ToList()
        });
    }

    [HttpGet("events/{eventSlug}/competitors")]
    public IActionResult GetAllCompetitorsForEvent(string eventSlug)
    {
        var theEvent = eventService.GetEventForSlug(eventSlug, EventIncludes.Competitors);

        return Ok(new
        {
            Data = theEvent.Brackets.SelectMany(x => x.Competitors.Select(CompetitorWithStreamInfoViewModel.FromDbModel))
        });
    }
}