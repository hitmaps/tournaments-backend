using BusinessLogic.Discord;
using BusinessLogic.Hitmaps;
using BusinessLogic.Models;
using BusinessLogic.Models.UpcomingMatches;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class HitmapsHomepageController : ApiControllerBase
{
    private readonly IEventService eventService;
    private readonly IMatchupService matchupService;
    private readonly IDiscordService discordService;

    public HitmapsHomepageController(IEventService eventService, IMatchupService matchupService, IDiscordService discordService)
    {
        this.eventService = eventService;
        this.matchupService = matchupService;
        this.discordService = discordService;
    }

    [HttpGet]
    [Route("hitmaps-homepage-info")]
    public async Task<IActionResult> GetInfoForHitmapsHomepage()
    {
        var now = DateTime.UtcNow;
        var events = eventService.GetAllEvents();
        var activeEvents = events.Where(x => x.RegistrationEndsAt < now && x.EventEndsAt > now).ToList();

        var viewModel = new HitmapsHomepageViewModel
        {
            RegisterableEvents = events.Where(x => x.RegistrationEndsAt > now && x.RegistrationOpensAt < now)
                .Select(EventViewModel.FromDbModel).ToList()
        };
        foreach (var theEvent in activeEvents)
        {
            var upcomingMatches = matchupService.GetUpcomingMatches(theEvent.Slug);
            var avatarUrls = await discordService.GetUsersAsync(
                eventService.GetEventForSlug(theEvent.Slug, EventIncludes.SplitQuery).DiscordGuildId);
            viewModel.UpcomingMatchEvents.Add(new UpcomingMatchEventViewModel
            {
                Event = EventViewModel.FromDbModel(theEvent),
                UpcomingMatches = upcomingMatches
                    .OrderBy(x => x.MatchScheduledAt)
                    .Select(x => UpcomingMatchViewModel.FromDbModel(x, avatarUrls))
                    .ToList()
            });
        }
        return Ok(viewModel);
    }
}