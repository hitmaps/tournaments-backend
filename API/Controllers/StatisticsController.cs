using BusinessLogic.Hitmaps.Statistics;
using DataAccess.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class StatisticsController : ApiControllerBase
{
    private readonly IStatisticsService statisticsService;

    public StatisticsController(IStatisticsService statisticsService)
    {
        this.statisticsService = statisticsService;
    }

    [HttpGet("events/{eventSlug}/statistics")]
    public async Task<IActionResult> GetStatistics(string eventSlug, [FromQuery] string statsKey)
    {
        return statsKey switch
        {
            StatsKey.MapPicks => Ok(statisticsService.GetMapCountsByDate(eventSlug, MissionType.Pick)),
            StatsKey.MapBans => Ok(statisticsService.GetMapCountsByDate(eventSlug, MissionType.Ban)),
            StatsKey.RandomMaps => Ok(statisticsService.GetMapCounts(eventSlug, MissionType.Random)),
            StatsKey.Shoutcasters => Ok(await statisticsService.GetShoutcasterStatisticsAsync(eventSlug)),
            StatsKey.MatchHistory => Ok(statisticsService.GetMatchHistoryForEvent(eventSlug)),
            StatsKey.MatchAdmins => Ok(await statisticsService.GetMatchAdminStatisticsAsync(eventSlug)),
            _ => BadRequest(new { Message = "Invalid statsKey provided." })
        };
    }
}