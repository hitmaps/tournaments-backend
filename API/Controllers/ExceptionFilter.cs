using API.Exceptions;
using BusinessLogic.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace API.Controllers;

public class ExceptionFilter : IActionFilter, IOrderedFilter
{
    public int Order => int.MaxValue - 10;

    public void OnActionExecuting(ActionExecutingContext context)
    {
        
    }

    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.Exception is AccessForbiddenException)
        {
            context.Result = new ObjectResult("Access is forbidden for this request.")
            {
                StatusCode = StatusCodes.Status403Forbidden
            };
            context.ExceptionHandled = true;
            return;
        }
        
        if (context.Exception is not AbstractHttpResponseException exception)
        {
            return;
        }
        
        context.Result = new ObjectResult(exception.GetMessage())
        {
            StatusCode = exception.GetStatusCode()
        };

        context.ExceptionHandled = true;
    }
}