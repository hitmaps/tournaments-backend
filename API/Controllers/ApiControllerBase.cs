using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

[ApiController]
[Route("api")]
public class ApiControllerBase : Controller
{
    internal ulong GetDiscordSnowflake()
    {
        return (ulong)RouteData.Values["userSnowflake"]!;
    }
}