using BusinessLogic.Hitmaps;
using BusinessLogic.Hitmaps.GroupStage;
using BusinessLogic.Models;
using BusinessLogic.RrStats;
using DataAccess.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class MatchController : ApiControllerBase
{
    private readonly IMatchupService matchupService;
    private readonly IRrStatsService rrStatsService;
    private readonly IGroupStageService groupStageService;

    public MatchController(IMatchupService matchupService, IRrStatsService rrStatsService, IGroupStageService groupStageService)
    {
        this.matchupService = matchupService;
        this.rrStatsService = rrStatsService;
        this.groupStageService = groupStageService;
    }

    [HttpGet("matches/{matchId:int}/metadata")]
    public async Task<IActionResult> GetMatchMetadataAsync(int matchId)
    {
        var matchup = matchupService.GetMatchupForMetadata(matchId);
        var groupName = string.Empty;
        if (matchup.GroupStage)
        {
            var groupStageInfo = groupStageService.GetGroupStageInfoForStage(matchup.Stage.Id);
            var groupId =
                groupStageInfo.Groups.First(x => x.CompetitorIds.Contains(matchup.CompetitorInfo[0].Competitor.Id));
            groupName = ((char)(65 + groupId.GroupId)).ToString();
        }
        var accolades = new Dictionary<Competitor, string>();

        foreach (var competitor in matchup.CompetitorInfo)
        {
            accolades[competitor.Competitor] =
                await rrStatsService.GetAccoladeForPlayerAsync(competitor.Competitor.DiscordId);
        }

        return Ok(new MatchupMetadataViewModel
        {
            Competitors = matchup.CompetitorInfo.Select(x => CompetitorWithAccoladeAndStreamUrlViewModel.FromDbModel(x.Competitor, accolades[x.Competitor])).ToList(),
            GroupName = groupName
        });
    }

    [HttpPatch("matches/{matchId:int}")]
    [DiscordAuth]
    public IActionResult SetGameModeIdForMatch(int matchId, [FromBody] UpdateGameModeIdRequestModel model)
    {
        try
        {
            matchupService.SetGameModeId(matchId, model.GameModeId);
        } catch (InvalidOperationException e)
        {
            return Conflict(new
            {
                e.Message
            });
        }

        return NoContent();
    }

    [HttpGet("matches/{matchId:int}/verify-authority")]
    [DiscordAuth]
    public async Task<IActionResult> VerifyMatchAuthority(int matchId)
    {
        await matchupService.VerifyMatchAuthorityAsync(matchId, GetDiscordSnowflake());

        return NoContent();
    }

    private ulong GetDiscordSnowflake()
    {
        return (ulong)RouteData.Values["userSnowflake"]!;
    }

    [HttpPost("matches/{matchId:int}/score")]
    [DiscordAuth]
    public async Task<IActionResult> PostScore(int matchId, [FromBody] PostScoreRequestModel model)
    {
        await matchupService.SendScoreToChallongeAsync(matchId, model, GetDiscordSnowflake());

        return NoContent();
    }

    [HttpGet("matches/{matchId:int}/map-stats")]
    public async Task<IActionResult> GetMapStats(int matchId, [FromQuery] string location)
    {
        return Ok(await rrStatsService.GetStatsForMatchAsync(matchId, location));
    }
}