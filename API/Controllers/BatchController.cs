using BusinessLogic.Batch.CloseRegistrations;
using BusinessLogic.Batch.SendMatchStartingNotifications;
using BusinessLogic.Discord;
using DataAccess.Data;
using Hangfire;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

[Route("batch/")]
public class BatchController : ApiControllerBase
{
    private readonly TournamentDbContext dbContext;
    private readonly IDiscordService discordService;
    private readonly bool isDevelopment;

    public BatchController(TournamentDbContext dbContext, IDiscordService discordService, IWebHostEnvironment hostingEnvironment)
    {
        this.dbContext = dbContext;
        this.discordService = discordService;
        isDevelopment = hostingEnvironment.IsDevelopment();
    }

    [HttpGet("send-match-starting-notifications")]
    public IActionResult SendMatchStartingNotifications()
    {
        if (!isDevelopment)
        {
            return NotFound();
        }
        
        BackgroundJob.Enqueue(() => new SendMatchStartingNotifications(dbContext, discordService).ProgramLogicAsync());
        
        return Accepted();
    }

    [HttpGet("close-registrations")]
    public IActionResult CloseRegistrations()
    {
        if (!isDevelopment)
        {
            return NotFound();
        }
        
        BackgroundJob.Enqueue(() => new CloseRegistrations(dbContext, discordService).ProgramLogicAsync());
        
        return Accepted();
    }
}