using BusinessLogic.Hitmaps;
using BusinessLogic.Models;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class CompetitorController : ApiControllerBase
{
    private readonly ICompetitorService competitorService;
    private readonly IEventService eventService;

    public CompetitorController(ICompetitorService competitorService, IEventService eventService)
    {
        this.competitorService = competitorService;
        this.eventService = eventService;
    }

    [HttpGet("competitors/recent-registration")]
    public IActionResult FetchMostRecentRegistration([FromQuery] ulong snowflake, [FromQuery] string platform)
    {
        var previousRegistration = competitorService.FetchMostRecentRegistration(snowflake, platform);

        return previousRegistration == null ? NoContent() : Ok(PreviousRegistrationViewModel.FromDbModel(previousRegistration));
    }

    [HttpPut("events/{eventSlug}/competitors/{competitorId:int}")]
    [DiscordAuth]
    public async Task<IActionResult> EditCompetitor(string eventSlug, int competitorId, EditCompetitorRequestModel requestModel)
    {
        await eventService.AssertEventManagementPermissionBySlugAsync(eventSlug, (ulong) RouteData.Values["userSnowflake"]!);
        
        await competitorService.EditCompetitorAsync(competitorId, requestModel);
        
        return NoContent();
    }
}