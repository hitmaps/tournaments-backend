using BusinessLogic.Discord;
using Microsoft.AspNetCore.Mvc.Filters;

namespace API.Controllers;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
public class DiscordAuthAttribute : ActionFilterAttribute
{

    public virtual bool Required { get; set; } = true;
    public override async Task OnActionExecutionAsync(ActionExecutingContext actionContext, ActionExecutionDelegate next)
    {
        try
        {
            // 1. Do we have the header?
            if (!actionContext.HttpContext.Request.Headers.TryGetValue("x-discord-token", out var headerValue) ||
                string.IsNullOrWhiteSpace(headerValue.ToString()))
            {
                if (Required)
                {
                    actionContext.Result = new ResultWithBody(StatusCodes.Status401Unauthorized,
                        "A X-Discord-Token is required for this request.");                    
                }
                
                return;
            }

            // 2. Is the token properly formed?
            var splitDiscordToken = headerValue.ToString().Split(' ');
            if (splitDiscordToken.Length != 2)
            {
                actionContext.Result = new ResultWithBody(StatusCodes.Status400BadRequest,
                    "The X-Discord-Token is malformed.");
                return;
            }

            // 3. Is the token still valid?
            var discordService = actionContext.HttpContext.RequestServices.GetService<IDiscordService>()!;
            var snowflake = await discordService.VerifyTokenAsync(splitDiscordToken[0], splitDiscordToken[1]);
            if (snowflake == null)
            {
                actionContext.Result = new ResultWithBody(StatusCodes.Status401Unauthorized, 
                    "The X-Discord-Token is invalid.");
                return;
            }

            actionContext.RouteData.Values.Add("userSnowflake", ulong.Parse(snowflake));
            actionContext.RouteData.Values.Add("userTokenType", splitDiscordToken[0]);
            actionContext.RouteData.Values.Add("userTokenValue", splitDiscordToken[1]);
            actionContext.RouteData.Values.Add("userContext", new DiscordUserContext(splitDiscordToken[0], splitDiscordToken[1]));
        }
        finally
        {
            await base.OnActionExecutionAsync(actionContext, next);
        }
    }
}