using BusinessLogic.Hitmaps;
using BusinessLogic.Hitmaps.Swiss;
using BusinessLogic.Hitmaps.Swiss.Standings;
using BusinessLogic.Models.Stage;
using Hangfire;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers.Stages;

public class SwissController : ApiControllerBase
{
    private readonly ISwissService swissService;
    private readonly SwissStandingsBuilder swissStandingsBuilder;

    public SwissController(ISwissService swissService, SwissStandingsBuilder swissStandingsBuilder)
    {
        this.swissStandingsBuilder = swissStandingsBuilder;
        this.swissService = swissService;
    }

    [HttpPatch("stages/{stageId:int}/swiss/players")]
    [DiscordAuth(Required = true)]
    public async Task<IActionResult> UpdatePlayerParticipation(int stageId,
        [FromBody] UpdateSwissPlayerParticipationRequestModel requestModel)
    {
        await swissService.UpdatePlayerParticipationAsync(stageId, requestModel, GetDiscordSnowflake());
        
        return NoContent();
    }

    [HttpPost("stages/{stageId:int}/swiss/rounds")]
    [DiscordAuth(Required = true)]
    public IActionResult StartRound(int stageId, [FromBody] StartSwissRoundRequestModel requestModel)
    {
        BackgroundJob.Enqueue(() => swissService.StartSwissRoundAsync(stageId, requestModel, GetDiscordSnowflake()));
        
        return Accepted();
    }
    
    [HttpGet("stages/{stageId:int}/swiss/standings")]
    public IActionResult GetStandings(int stageId)
    {
        return Ok(swissStandingsBuilder.GetStandings(stageId));
    }
}