using System.Text.Json;
using System.Text.Json.Serialization;
using BusinessLogic.Batch.SyncMatchInfoWithBracket;
using BusinessLogic.Hitmaps;
using BusinessLogic.Models;
using BusinessLogic.Models.GroupStage;
using BusinessLogic.Models.Stage;
using DataAccess.Models;
using Hangfire;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class StageController : ApiControllerBase
{
    private readonly IStageService _stageService;
    private readonly IEventService _eventService;
    private readonly IMatchupService _matchupService;
    private readonly ICompetitorService _competitorService;

    public StageController(IStageService stageService, IEventService eventService, IMatchupService matchupService, ICompetitorService competitorService)
    {
        _stageService = stageService;
        _eventService = eventService;
        _matchupService = matchupService;
        _competitorService = competitorService;
    }

    [HttpPost]
    [Route("stages/{stageId:int}/group-stage")]
    public async Task<ActionResult> BuildGroupStage(int stageId, [FromBody] BuildGroupStageRequestModel requestModel)
    {
        await _stageService.BuildGroupStageAsync(stageId, requestModel);
        
        return NoContent();
    }
    
    [HttpPut("stages/{stageId:int}/matches/{matchId:long}")]
    [DiscordAuth]
    public async Task<IActionResult> ScheduleMatch(int stageId, long matchId, [FromBody] ScheduleMatchRequestModel requestModel)
    {
        await _eventService.AssertEventManagementPermissionByStageIdAsync(stageId,
            (ulong) RouteData.Values["userSnowflake"]!);

        var matchInfo = await _matchupService.ScheduleMatchAsync(stageId, matchId, requestModel); 
        
        return Ok(BasicScheduledMatchViewModel.FromDbModel(matchInfo));
    }

    [HttpPatch("stages/{stageId:int}/matches/{matchId:long}/state")]
    [DiscordAuth]
    public async Task<IActionResult> PostponeMatch(int stageId, long matchId, [FromBody] MatchStateModel matchState)
    {
        if (matchState.State != MatchInfo.StateScheduled && matchState.State != MatchInfo.StatePostponed && matchState.State != MatchInfo.StateNeedsScheduling)
        {
            return BadRequest(new
            {
                Message = "State must be one of: [\"Needs Scheduling\", \"Scheduled\", \"Postponed\"]"
            });
        }

        await _matchupService.SetMatchState(stageId, matchId, matchState);
        
        return NoContent();
    }

    [HttpDelete("stages/{stageId:int}/matches/{matchId:long}")]
    public async Task<IActionResult> ResetMatch(int stageId, long matchId)
    {
        await _matchupService.ResetMatchAsync(stageId, matchId);

        return NoContent();
    }

    [HttpPatch("stages/{stageId:int}/matches/{matchId:long}/forfeit")]
    [DiscordAuth]
    public async Task<IActionResult> ForfeitMatch(int stageId, long matchId,
        [FromBody] ForfeitMatchRequestModel requestModel)
    {
        if (requestModel.ForfeitPlayerIds.Count == 0)
        {
            return BadRequest(new
            {
                Message = "You must select at least one player to forfeit"
            });
        }
        
        await _stageService.ForfeitMatchAsync(stageId, matchId, requestModel, GetDiscordSnowflake());
        
        return NoContent();
    }

    [HttpPatch("stages/{stageId:int}/matches/{matchId:long}/casters")]
    [DiscordAuth]
    public IActionResult SetCasters(int stageId, long matchId,
        [FromBody] SetCastersRequestModel requestModel)
    {
        _matchupService.SetCasters(stageId, matchId, requestModel);

        return NoContent();
    }

    [HttpPatch("stages/{stageId:int}/matches/{matchId:long}/admin")]
    [DiscordAuth]
    public IActionResult SetMatchAdmin(int stageId, long matchId, [FromBody] SetMatchAdminRequestModel requestModel)
    {
        _matchupService.SetMatchAdmin(stageId, matchId, requestModel);

        return NoContent();
    }

    [HttpPatch("stages/{stageId:int}/finalize")]
    [DiscordAuth]
    public async Task<IActionResult> FinalizeStage(int stageId)
    {
        await _stageService.FinalizeStageAsync(stageId, GetDiscordSnowflake());

        return NoContent();
    }

    [HttpGet("stages/{stageId:int}/seedings")]
    public IActionResult GetSeedingsAsync(int stageId)
    {
        var seedings = _stageService.GetSeedings(stageId);
        var competitors = _stageService.GetCompetitorsForStage(stageId);

        return Ok(seedings.OrderBy(x => x.Seeding).Select(x => new StageSeedingViewModel
        {
            CompetitorId = x.CompetitorId,
            Name = competitors.First(y => y.Id == x.CompetitorId).ChallongeName!,
            Seed = x.Seeding,
            StageId = x.StageId
        }));
    }

    [HttpPut("stages/{stageId:int}/seedings")]
    [DiscordAuth]
    public async Task<IActionResult> SetSeedings(int stageId, [FromBody] List<StageSeedingViewModel> seedings)
    {
        await _stageService.UpdateSeedingsAsync(stageId, seedings, GetDiscordSnowflake());

        return NoContent();
    }

    [HttpGet("stages/{stageId:int}/bracket-preview")]
    public async Task<IActionResult> GetBracketPreview(int stageId)
    {
        var bracketData = await _stageService.GetBracketPreviewAsync(stageId);
        return Ok(JsonSerializer.Deserialize<dynamic>(bracketData));
    }

    [HttpPatch("stages/{stageId:int}")]
    [DiscordAuth]
    public async Task<IActionResult> UpdateStage(int stageId, [FromBody] UpdateStageRequestModel requestModel)
    {
        var stage = await _stageService.GetStageAsync(stageId);
        if (stage == null)
        {
            return BadRequest(new { Message = "Stage not found" });
        }
        
        await _stageService.UpdateStageInfoAsync(stageId, requestModel, GetDiscordSnowflake());
        if (requestModel.State == State.Underway.Value && stage.IsBracketStage())
        {
            BackgroundJob.Enqueue<SyncMatchInfoWithBracket>(x => x.SyncMatchInfo(stageId));
        }

        return NoContent();
    }

    [HttpGet("stages/{stageId:int}/bracket")]
    public async Task<IActionResult> GetBracket(int stageId)
    {
        return Ok(await _stageService.GetBracketAsync<dynamic>(stageId));
    }

    [HttpGet("stages/{stageId:int}/match-sync")]
    [DiscordAuth]
    public IActionResult ResyncMatches(int stageId)
    {
        BackgroundJob.Enqueue<SyncMatchInfoWithBracket>(x => x.SyncMatchInfo(stageId));

        return Accepted();
    }

    [HttpGet("stages/{stageId:int}/competitors/{discordId}/match-history")]
    public IActionResult FetchMatchHistory(int stageId, ulong discordId)
    {
        var competitor = _competitorService.FetchCompetitorByDiscordAndStage(discordId, stageId);

        // Don't care about shoutcasts
        return Ok(competitor.ScheduledMatches
            .Where(x => x.Match.Stage.Id == stageId && 
                        x.Match.State != MatchInfo.StateNeedsScheduling && 
                        x.Match.CompetitorInfo.All(ci => !ci.Forfeit))
            .Select(x => ScheduledMatchViewModel.FromDbModel(x.Match, [])).ToList());
    }
}