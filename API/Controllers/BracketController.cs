
using BusinessLogic.Discord;
using BusinessLogic.Hitmaps;
using BusinessLogic.Models;
using Challonge.Objects;
using Discord.Rest;
using Microsoft.AspNetCore.Mvc;
using MatchInfo = DataAccess.Models.MatchInfo;

namespace API.Controllers;

public class BracketController : ApiControllerBase
{
    private readonly IEventService eventService;
    private readonly IBracketService bracketService;
    private readonly IMatchupService matchupService;
    private readonly IDiscordService discordService;
    private readonly ICompetitorService competitorService;
    private readonly IVisualBracketsService visualBracketsService;
    private readonly IChallongeStandingsBuilder challongeStandingsBuilder;

    public BracketController(IEventService eventService,
        IBracketService bracketService,
        IMatchupService matchupService,
        IDiscordService discordService,
        ICompetitorService competitorService,
        IVisualBracketsService visualBracketsService,
        IChallongeStandingsBuilder challongeStandingsBuilder)
    {
        this.eventService = eventService;
        this.bracketService = bracketService;
        this.matchupService = matchupService;
        this.discordService = discordService;
        this.competitorService = competitorService;
        this.visualBracketsService = visualBracketsService;
        this.challongeStandingsBuilder = challongeStandingsBuilder;
    }

    [HttpGet("brackets/{bracketId:int}")]
    public async Task<IActionResult> GetBracketDataAsync(int bracketId)
    {
        var bracket = bracketService.GetBracketInformation(bracketId);
        var users = await discordService.GetUsersAsync(bracket.Event.DiscordGuildId);

        return Ok(BracketWithMatchesAndCompetitorsViewModel.FromDbModel(bracket, users));
    }

    [HttpGet("brackets/{bracketId:int}/challonge")]
    public async Task<IActionResult> GetChallongeData(int bracketId)
    {
        var challongeTournamentAsync = await bracketService.GetChallongeTournamentAsync(bracketId);
        return Ok(challongeTournamentAsync);
    }

    [HttpGet("brackets/{bracketId:int}/group-standings")]
    public async Task<IActionResult> GetGroupStandingsAsync(int bracketId)
    {
        return Ok(await challongeStandingsBuilder.GetStandingsAsync(bracketId));
    }
    
    // Overlays
    [HttpGet("brackets/{bracketId:int}/visual-bracket")]
    public IActionResult GetBracketsOverlay(int bracketId)
    {
        return new ContentResult
        {
            Content = visualBracketsService.GetVisualBracket(bracketId),
            ContentType = "text/html"
        };
    }
}