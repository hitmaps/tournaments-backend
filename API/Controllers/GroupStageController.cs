using BusinessLogic.Hitmaps.GroupStage;
using DataAccess.Data;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class GroupStageController : ApiControllerBase
{
    private readonly IGroupStageService _groupStageService;

    public GroupStageController(IGroupStageService groupStageService)
    {
        _groupStageService = groupStageService;
    }

    [HttpGet]
    [Route("group-stage/{stageId:int}")]
    public IActionResult GetGroupStageInfo(int stageId)
    {
        return Ok(GroupStageViewModel.FromBusinessModel(_groupStageService.GetGroupStageInfoForStage(stageId)));
    }

    [Obsolete("Should be combined with swiss standings endpoint")]
    [HttpGet("stages/{stageId:int}/round-robin/standings")]
    public IActionResult GetStandings(int stageId)
    {
        return Ok(_groupStageService.GetStandingsForGroupStage(stageId));
    }
}