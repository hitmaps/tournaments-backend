using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class ResultWithBody : JsonResult
{
    public ResultWithBody(int statusCode, string message) : base(new { message })
    {
        StatusCode = statusCode;
    }
}