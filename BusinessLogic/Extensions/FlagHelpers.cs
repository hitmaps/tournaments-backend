namespace BusinessLogic.Extensions;

public class FlagHelpers
{
    public static bool IsFlagSet<T>(T actual, T expected) where T : Enum
    {
        return ((int)(object)actual & (int)(object)expected!) > 0;
    }
}