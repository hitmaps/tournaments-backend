namespace BusinessLogic.Extensions;

public static class DateTimeExtensions
{
    public static long ToUnixTimestamp(this DateTime dateTime)
    {
        return (long)dateTime.Subtract(DateTime.UnixEpoch).TotalSeconds;
    }

    public static string ToApiDateTimeFormat(this DateTime dateTime)
    {
        return dateTime.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
    }

    public static string ToApiDateFormat(this DateTime dateTime)
    {
        return dateTime.ToString("yyyy-MM-dd");
    }
}