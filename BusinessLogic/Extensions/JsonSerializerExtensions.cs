
using System.Text.Json;

namespace BusinessLogic.Extensions;

public static class JsonSerializerExtensions
{
    public static string SerializeToJsonCamelCase<T>(this T value, JsonSerializerOptions? settings = null)
    {
        settings ??= new JsonSerializerOptions();
        settings.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
        
        return JsonSerializer.Serialize(value, settings);
    }

    public static T? DeserializeFromJsonCaseInsensitive<T>(this string json, JsonSerializerOptions? settings = null)
    {
        settings ??= new JsonSerializerOptions();
        settings.PropertyNameCaseInsensitive = true;
        
        return JsonSerializer.Deserialize<T>(json, settings);
    }
}