using BusinessLogic.Extensions;
using DataAccess.Models;

namespace BusinessLogic.Models;

public class CompletedMapSelectionViewModel : MapSelectionViewModel
{
    public DateTime? MapStartedAt { get; set; }
    public DateTime? ResultVerifiedAt { get; set; }
    public DateTime? WinnerFinishedAt { get; set; }
    public string? WinnerDiscordId { get; set; }
    public string? MapJson { get; set; }

    public static CompletedMapSelectionViewModel FromDbModel(MapSelection mapSelection, bool showRandomMaps = false)
    {
        return new CompletedMapSelectionViewModel
        {
            CompetitorId = mapSelection.Competitor?.Id,
            CompetitorName = mapSelection.Competitor?.ChallongeName,
            SelectionType = mapSelection.MissionType.ToString(),
            MapSlug = FlagHelpers.IsFlagSet(mapSelection.MissionType, MissionType.Random) && !showRandomMaps ? 
                null : 
                mapSelection.Mission.HitmapsSlug,
            RoundType = mapSelection.RoundType,
            State = mapSelection.State,
            MapStartedAt = mapSelection.MapStartedAt,
            ResultVerifiedAt = mapSelection.ResultVerifiedAt,
            WinnerFinishedAt = mapSelection.WinnerFinishedAt,
            WinnerDiscordId = mapSelection.Winner?.DiscordId.ToString(),
            MapJson = mapSelection.MapJson
        };
    }
}