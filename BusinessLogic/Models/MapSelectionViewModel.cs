using BusinessLogic.Extensions;
using DataAccess.Models;

namespace BusinessLogic.Models;

public class MapSelectionViewModel
{
    public int? CompetitorId { get; set; }
    public string? CompetitorName { get; set; }
    public string SelectionType { get; set; } = null!;
    public string? MapSlug { get; set; }
    public string? RoundType { get; set; }
    public string State { get; set; } = null!;
    public int? WinnerId { get; set; }

    public static MapSelectionViewModel FromDbModel(MapSelection mapSelection, bool showRandomMaps = false)
    {
        return new MapSelectionViewModel
        {
            CompetitorId = mapSelection.Competitor?.Id,
            CompetitorName = mapSelection.Competitor?.ChallongeName,
            SelectionType = mapSelection.MissionType.ToString(),
            MapSlug = FlagHelpers.IsFlagSet(mapSelection.MissionType, MissionType.Random) && !showRandomMaps ? 
                null : 
                mapSelection.Mission.HitmapsSlug,
            RoundType = mapSelection.RoundType,
            State = mapSelection.State,
            WinnerId = mapSelection.Winner?.Id
        };
    }
}