using System.Text.Json;
using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Models;

public class StageWithMatchesViewModel
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string Type { get; set; } = null!;
    public string State { get; set; } = null!;
    public bool AllMatchesComplete { get; set; }
    public dynamic Settings { get; set; } = null!;
    public List<ScheduledMatchViewModel> Matches { get; set; } = new();
    public List<PointsForVictoryViewModel> PointsForVictory { get; set; } = new();

    public static StageWithMatchesViewModel FromDbModel(DataAccess.Models.Stage stage, List<RestGuildUser> guildUsers)
    {
        return new StageWithMatchesViewModel
        {
            Id = stage.Id,
            Name = stage.Name,
            Type = stage.Type,
            State = stage.State,
            Settings = JsonSerializer.Deserialize<dynamic>(stage.SettingsJson)!,
            Matches = stage.ScheduledMatches.Select(x => ScheduledMatchViewModel.FromDbModel(x, guildUsers)).ToList(),
            AllMatchesComplete = stage.ScheduledMatches.All(x => x.State == MatchInfo.StateComplete),
            PointsForVictory = stage.PointsForVictories.Select(PointsForVictoryViewModel.FromDbModel).ToList()
        };
    }
}