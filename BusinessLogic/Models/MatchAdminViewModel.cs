using Discord.Rest;

namespace BusinessLogic.Models;

public class MatchAdminViewModel
{
    public string DiscordId { get; set; } = null!;
    public string Name { get; set; } = null!;

    public static MatchAdminViewModel? FromDbModel(ulong? matchAdminDiscordId, List<RestGuildUser> users)
    {
        if (!matchAdminDiscordId.HasValue)
        {
            return null;
        }

        return new MatchAdminViewModel
        {
            DiscordId = matchAdminDiscordId.Value.ToString(),
            Name = users.FirstOrDefault(x => x.Id == matchAdminDiscordId.Value)?.Username ?? "[REDACTED]"
        };
    }
}