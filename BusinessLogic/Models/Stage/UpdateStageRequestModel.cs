namespace BusinessLogic.Models.Stage;

public class UpdateStageRequestModel
{
    public string? State { get; set; }
}