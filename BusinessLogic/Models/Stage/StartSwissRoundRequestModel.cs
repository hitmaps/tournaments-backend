namespace BusinessLogic.Models.Stage;

public class StartSwissRoundRequestModel
{
    public int Round { get; set; }
}