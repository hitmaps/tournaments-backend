namespace BusinessLogic.Models.Stage;

public class UpdateSwissPlayerParticipationRequestModel
{
    public int Round { get; set; }
    public int CompetitorId { get; set; }
    public bool Participating { get; set; }
}