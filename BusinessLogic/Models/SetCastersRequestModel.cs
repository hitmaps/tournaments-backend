namespace BusinessLogic.Models;

public class SetCastersRequestModel
{
    public string? MainCastDiscordId { get; set; } = null!;
    public string? MainCastStreamUrl { get; set; } = null!;
    public List<string> CocastDiscordIds { get; set; } = new();
}