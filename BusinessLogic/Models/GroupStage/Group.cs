namespace BusinessLogic.Models.GroupStage;

public class Group
{
    public int GroupId { get; set; }
    public List<int> CompetitorIds { get; set; } = new();
}