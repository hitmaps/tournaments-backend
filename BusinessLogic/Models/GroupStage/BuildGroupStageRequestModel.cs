namespace BusinessLogic.Models.GroupStage;

public class BuildGroupStageRequestModel
{
    public List<Group> Groups { get; set; } = new();
}