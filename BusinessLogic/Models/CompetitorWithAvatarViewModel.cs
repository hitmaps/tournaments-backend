using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Models;

public class CompetitorWithAvatarViewModel : CompetitorViewModel
{
    public string AvatarUrl { get; set; } = null!;
    
    public static CompetitorWithAvatarViewModel FromDbModel(Competitor dbModel, List<RestGuildUser> users)
    {
        return new CompetitorWithAvatarViewModel
        {
            Id = dbModel.Id,
            BracketId = dbModel.Bracket.Id,
            ChallongePlayerId = dbModel.ChallongePlayerId,
            ChallongeName = dbModel.ChallongeName,
            DiscordId = dbModel.DiscordId.ToString(),
            AvatarUrl = GetAvatarUrlForUser(dbModel.DiscordId, users),
            CountryCode = dbModel.CountryCode
        };
    }

    private static string GetAvatarUrlForUser(ulong discordId, List<RestGuildUser> users)
    {
        return users.FirstOrDefault(x => x.Id == discordId)?.GetAvatarUrl() ?? string.Empty;
    }
}