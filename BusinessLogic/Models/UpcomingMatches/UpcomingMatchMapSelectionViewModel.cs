using DataAccess.Models;

namespace BusinessLogic.Models.UpcomingMatches;

public class UpcomingMatchMapSelectionViewModel
{
    public int? CompetitorId { get; set; }
    public string SelectionType { get; set; } = null!;
    public string? MapSlug { get; set; }
    public string? MissionName { get; set; }
    public string? MissionLocation { get; set; }

    public static UpcomingMatchMapSelectionViewModel FromDbModel(MapSelection mapSelection)
    {
        var viewModel = new UpcomingMatchMapSelectionViewModel
        {
            CompetitorId = mapSelection.Competitor?.Id,
            SelectionType = mapSelection.MissionType.ToString(),
            MapSlug = mapSelection.Mission.HitmapsSlug,
            MissionName = mapSelection.Mission.Name,
            MissionLocation = mapSelection.Mission.Location
        };

        // ReSharper disable once InvertIf
        if (!mapSelection.Revealed)
        {
            viewModel.MapSlug = null;
            viewModel.MissionName = null;
            viewModel.MissionLocation = null;
        }

        return viewModel;
    }
}