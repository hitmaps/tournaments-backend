using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Models.UpcomingMatches;

public class UpcomingMatchViewModel
{
    public int Id { get; set; }
    public long? ChallongeMatchId { get; set; }
    public string MatchScheduledAt { get; set; } = null!;
    public string? MatchCompletedAt { get; set; }
    public List<UpcomingMatchCompetitorViewModel> Competitors { get; set; } = new();
    public Guid? GameModeMatchId { get; set; }
    public List<UpcomingMatchMapSelectionViewModel> Maps { get; set; }
    
    public int? PointsForVictory { get; set; }
    public MatchCastViewModel? Cast { get; set; }
    public MatchAdminViewModel? MatchAdmin { get; set; }

    public static UpcomingMatchViewModel FromDbModel(MatchInfo model, List<RestGuildUser> users)
    {
        return new UpcomingMatchViewModel
        {
            Id = model.Id,
            ChallongeMatchId = model.ChallongeMatchId,
            MatchScheduledAt = model.MatchScheduledAt!.Value.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            MatchCompletedAt = model.MatchCompletedAt?.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            Competitors = model.CompetitorInfo.Select(x => UpcomingMatchCompetitorViewModel.FromDbModel(x.Competitor, users)).ToList(),
            GameModeMatchId = model.GameModeMatchId,
            Maps = model.MapSelections.Select(UpcomingMatchMapSelectionViewModel.FromDbModel).ToList(),
            PointsForVictory = model.GroupStage ? 
                null : 
                model.Stage.PointsForVictories.FirstOrDefault(x => x.Round == model.Round)?.Points,
            Cast = MatchCastViewModel.FromDbCollection(model.Casters, users),
            MatchAdmin = MatchAdminViewModel.FromDbModel(model.MatchAdminDiscordId, users)
        };
    }
}