namespace BusinessLogic.Models.UpcomingMatches;

public class UpcomingMatchEventViewModel
{
    public EventViewModel Event { get; set; } = null!;
    public List<UpcomingMatchViewModel> UpcomingMatches { get; set; } = new();
}