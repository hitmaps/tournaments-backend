using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Models.UpcomingMatches;

public class UpcomingMatchCompetitorViewModel
{
    public int Id { get; set; }
    public string? ChallongeName { get; set; }
    public string? AvatarUrl { get; set; }
    public string? CountryCode { get; set; }

    public static UpcomingMatchCompetitorViewModel FromDbModel(Competitor dbModel, List<RestGuildUser> users)
    {
        return new UpcomingMatchCompetitorViewModel
        {
            Id = dbModel.Id,
            ChallongeName = dbModel.ChallongeName,
            AvatarUrl = GetAvatarForUser(dbModel.DiscordId, users),
            CountryCode = dbModel.CountryCode
        };
    }

    private static string GetAvatarForUser(ulong discordId, List<RestGuildUser> users)
    {
        return users.FirstOrDefault(x => x.Id == discordId)?.GetDisplayAvatarUrl() ?? string.Empty;
    }
}