namespace BusinessLogic.Models;

public record State(string Value)
{
    public static State Complete { get; } = new("Complete");
    public static State Pending { get; } = new("Pending");
    public static State Underway { get; } = new("Underway");
    public static State Waiting { get; } = new("Waiting");
    public override string ToString() => Value;

    public static State FromValue(string? value)
    {
        return value switch
        {
            "Complete" => Complete,
            "Pending" => Pending,
            "Underway" => Underway,
            "Waiting" => Waiting,
            _ => throw new ArgumentException($"Could not find state for value '{value}'")
        };
    }
}