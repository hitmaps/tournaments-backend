using BusinessLogic.Discord;

namespace BusinessLogic.Models;

public class UserInfoViewModel
{
    public string Snowflake { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string Discriminator { get; set; } = null!;
    public bool ServerMember { get; set; }
    public List<BracketViewModel> RegisteredBrackets { get; set; } = new();
    public List<DiscordConnection> Connections { get; set; } = new();
    public List<string> ServerRoles { get; set; } = new();
    public bool TourneyBanned { get; set; }
    public bool EventAdmin { get; set; }

    public static readonly UserInfoViewModel Null = new()
    {
        Snowflake = string.Empty,
        Name = "Unknown",
        Discriminator = "0000",
        ServerMember = false,
        TourneyBanned = false,
        EventAdmin = false
    };
}