namespace BusinessLogic.Models.MatchHistory;

public class MatchupViewModel
{
    public long ChallongeId { get; set; }
    public bool GroupStage { get; set; }
    public int? Round { get; set; }
    public string Status { get; set; } = null!; // complete, scheduled, not-scheduled, not-schedulable
    public string? OpponentDiscordId { get; set; }
    public string? MatchScheduledAt { get; set; }
    public List<MatchupMapSelectionViewModel> Maps { get; set; } = new();
}