using DataAccess.Models;

namespace BusinessLogic.Models.MatchHistory;

public class MatchupMapSelectionViewModel
{
    public string SelectionType { get; set; } = null!;
    public string? MissionLocation { get; set; }
    public string? MissionName { get; set; }
    public string? ChosenByDiscordId { get; set; }
    public string? MapJson { get; set; }

    public static MatchupMapSelectionViewModel? FromDbModel(MapSelection? mapSelection)
    {
        if (mapSelection == null)
        {
            return null;
        }
        
        var viewModel = new MatchupMapSelectionViewModel
        {
            SelectionType = mapSelection.MissionType.ToString(),
            ChosenByDiscordId = mapSelection.Competitor?.DiscordId.ToString(),
            MissionLocation = mapSelection.Mission.Location,
            MissionName = mapSelection.Mission.Name,
            MapJson = mapSelection.MapJson
        };

        if (!mapSelection.Revealed)
        {
            viewModel.MissionLocation = null;
            viewModel.MissionName = null;
        }

        return viewModel;
    }
}