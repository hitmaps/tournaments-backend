namespace BusinessLogic.Models.MatchHistory;

public class MatchHistoryViewModel
{
    public string TournamentName { get; set; } = null!;
    public List<MatchupViewModel> Matches { get; set; } = new();
}

