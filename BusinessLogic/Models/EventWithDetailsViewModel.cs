using DataAccess.Models;

namespace BusinessLogic.Models;

public class EventWithDetailsViewModel
{
    public List<string> AdminRoles { get; set; } = new();
    public EventViewModel Event { get; set; } = null!;
    public List<BracketViewModel> Brackets { get; set; } = new();
    
    public static EventWithDetailsViewModel FromDbModel(Event theEvent)
    {
        return new EventWithDetailsViewModel
        {
            AdminRoles = theEvent.DiscordAdminRoleIds.Select(x => x.DiscordRoleId).ToList(),
            Event = EventViewModel.FromDbModel(theEvent),
            Brackets = theEvent.Brackets.Select(BracketViewModel.FromDbModel).ToList()
        };
    }
}