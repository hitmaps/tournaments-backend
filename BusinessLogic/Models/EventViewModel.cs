using BusinessLogic.Extensions;
using DataAccess.Models;

namespace BusinessLogic.Models;

public class EventViewModel
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
    public string EventType { get; set; } = null!;
    public string Slug { get; set; } = null!;
    public string IsoRegistrationStartDate { get; set; }
    public string IsoRegistrationEndDate { get; set; }
    public string IsoEventEndDate { get; set; }
    public string DiscordSignupChannel { get; set; } = null!;
    public string Rules { get; set; } = null!;
    public string FormattedRules { get; set; } = null!;
    public string OverlayBackgroundUrl { get; set; } = null!;
    public string OverlayLogoUrl { get; set; } = null!;
    public string State { get; set; } = null!;
    public bool UsesChallonge { get; set; }
    
    public static EventViewModel FromDbModel(Event theEvent)
    {
        return new EventViewModel
        {
            Id = theEvent.Id,
            Name = theEvent.Name,
            TileUrl = theEvent.TileUrl,
            EventType = theEvent.Type,
            Slug = theEvent.Slug,
            IsoRegistrationStartDate = theEvent.RegistrationOpensAt.ToApiDateTimeFormat(),
            IsoRegistrationEndDate = theEvent.RegistrationEndsAt.ToApiDateTimeFormat(),
            IsoEventEndDate = theEvent.EventEndsAt.ToApiDateTimeFormat(),
            DiscordSignupChannel = theEvent.DiscordSignupChannelId.ToString(),
            Rules = theEvent.Rules,
            FormattedRules = theEvent.Rules.Replace("\r", "").Replace("\n", "<br>"),
            OverlayBackgroundUrl = theEvent.OverlayBackgroundUrl,
            OverlayLogoUrl = theEvent.OverlayLogoUrl,
            State = theEvent.State,
            UsesChallonge = theEvent.UsesChallonge
        };
    }
}