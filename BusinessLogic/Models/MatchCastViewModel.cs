using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Models;

public class MatchCastViewModel
{
    public CasterViewModel MainCaster { get; set; } = null!;
    public string MainCasterUrl { get; set; } = null!;
    public List<CasterViewModel> Cocasters { get; set; } = new();

    public static MatchCastViewModel? FromDbCollection(List<Caster> caster, List<RestGuildUser> guildUsers)
    {
        if (!caster.Any())
        {
            return null;
        }
        
        var mainCaster = caster.First(x => x.Type == CasterType.Main);
        return new MatchCastViewModel
        {
            MainCaster = new CasterViewModel
            {
                DiscordId = mainCaster.DiscordId.ToString(),
                Name = GetDiscordNameForId(mainCaster.DiscordId, guildUsers)
            },
            MainCasterUrl = mainCaster.StreamUrl!,
            Cocasters = caster.Where(x => x.Type == CasterType.Cocast)
                .Select(x => new CasterViewModel
                {
                    DiscordId = x.DiscordId.ToString(),
                    Name = GetDiscordNameForId(x.DiscordId, guildUsers)
                })
                .ToList()
        };
    }

    private static string GetDiscordNameForId(ulong discordId, IEnumerable<RestGuildUser> users)
    {
        return users.FirstOrDefault(x => x.Id == discordId)?.Username ?? "[REDACTED]";
    }
} 