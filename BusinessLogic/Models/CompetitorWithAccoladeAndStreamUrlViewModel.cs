using BusinessLogic.RrStats;
using DataAccess.Models;

namespace BusinessLogic.Models;

public class CompetitorWithAccoladeAndStreamUrlViewModel : CompetitorViewModel
{
    public string Accolade { get; set; } = null!;
    public string StreamUrl { get; set; } = null!;

    public static CompetitorWithAccoladeAndStreamUrlViewModel FromDbModel(Competitor dbModel, string accolade)
    {
        return new CompetitorWithAccoladeAndStreamUrlViewModel
        {
            Id = dbModel.Id,
            BracketId = dbModel.Bracket.Id,
            ChallongePlayerId = dbModel.ChallongePlayerId,
            ChallongeName = dbModel.ChallongeName,
            DiscordId = dbModel.DiscordId.ToString(),
            CountryCode = dbModel.CountryCode,
            Accolade = accolade,
            StreamUrl = dbModel.StreamUrl
        };
    }
}