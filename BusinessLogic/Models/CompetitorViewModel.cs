using DataAccess.Models;

namespace BusinessLogic.Models;

public class CompetitorViewModel
{
    public int Id { get; set; }
    public int BracketId { get; set; }
    public string DiscordId { get; set; } = null!;
    public long? ChallongePlayerId { get; set; }
    public string? ChallongeName { get; set; }
    public string? CountryCode { get; set; }

    public static CompetitorViewModel FromDbModel(Competitor dbModel)
    {
        return new CompetitorViewModel
        {
            Id = dbModel.Id,
            BracketId = dbModel.Bracket.Id,
            ChallongePlayerId = dbModel.ChallongePlayerId,
            ChallongeName = dbModel.ChallongeName,
            DiscordId = dbModel.DiscordId.ToString(),
            CountryCode = dbModel.CountryCode
        };
    }
}