using System.Text.Json;
using DataAccess.Models;

namespace BusinessLogic.Models;

public class BracketViewModel
{
    public int Id { get; set; }
    public string? ChallongeName { get; set; }
    public int ParticipantLimit { get; set; }
    public string Platform { get; set; } = null!;
    public string PlatformIcons { get; set; } = null!;
    public List<MapPoolViewModel> Maps { get; set; } = new();
    public int CurrentParticipantCount { get; set; }
    public List<StageViewModel> Stages { get; set; } = new();

    public static BracketViewModel FromDbModel(Bracket bracket)
    {
        return new BracketViewModel
        {
            Id = bracket.Id,
            ChallongeName = bracket.ChallongeName,
            ParticipantLimit = bracket.ParticipantLimit,
            Platform = bracket.Platform,
            PlatformIcons = bracket.PlatformIcons,
            Maps = bracket.Maps.Select(MapPoolViewModel.FromDbModel).ToList(),
            CurrentParticipantCount = bracket.Competitors.Count,
            Stages = bracket.Stages.Select(stage => new StageViewModel
            {
                Id = stage.Id,
                Name = stage.Name,
                Type = stage.Type,
                State = stage.State,
                Settings = JsonSerializer.Deserialize<dynamic>(stage.SettingsJson)!
            }).ToList()
        };
    }
}