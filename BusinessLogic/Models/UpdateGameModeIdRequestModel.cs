namespace BusinessLogic.Models;

public class UpdateGameModeIdRequestModel
{
    public Guid GameModeId { get; set; }
}