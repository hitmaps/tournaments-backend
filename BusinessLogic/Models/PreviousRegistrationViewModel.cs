using DataAccess.Models;

namespace BusinessLogic.Models;

public class PreviousRegistrationViewModel
{
    public string? ChallongeName { get; set; }
    public string? CountryCode { get; set; }
    public string StreamUrl { get; set; } = null!;

    public static PreviousRegistrationViewModel FromDbModel(Competitor competitor)
    {
        return new PreviousRegistrationViewModel
        {
            ChallongeName = competitor.ChallongeName,
            CountryCode = competitor.CountryCode,
            StreamUrl = competitor.StreamUrl
        };
    }
}