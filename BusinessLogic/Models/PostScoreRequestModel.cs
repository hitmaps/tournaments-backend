namespace BusinessLogic.Models;

public class PostScoreRequestModel
{
    public List<ScoreHistoryModel> ScoreHistory { get; set; } = new();
}