namespace BusinessLogic.Models;

public class MatchupMetadataViewModel
{
    public List<CompetitorWithAccoladeAndStreamUrlViewModel> Competitors { get; set; } = new();
    public string? GroupName { get; set; }
}