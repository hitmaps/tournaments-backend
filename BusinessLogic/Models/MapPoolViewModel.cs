using DataAccess.Models;

namespace BusinessLogic.Models;

public class MapPoolViewModel
{
    public int Id { get; set; }
    public string HitmapsSlug { get; set; } = null!;
    public string Location { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string Game { get; set; } = null!;
    public bool AllowedToPick { get; set; }
    public bool AllowedToBan { get; set; }
    public bool AllowedForRandom { get; set; }

    public static MapPoolViewModel FromDbModel(MissionForPool map)
    {
        return new MapPoolViewModel
        {
            Id = map.Id,
            HitmapsSlug = map.HitmapsSlug,
            Location = map.Location,
            Name = map.Name,
            Game = map.Game,
            AllowedToPick = IsFlagSet(map.Type, MissionType.Pick),
            AllowedToBan = IsFlagSet(map.Type, MissionType.Ban),
            AllowedForRandom = IsFlagSet(map.Type, MissionType.Random)
        };
    }

    private static bool IsFlagSet(MissionType type, MissionType expected)
    {
        return (type & expected) == expected;
    }
}