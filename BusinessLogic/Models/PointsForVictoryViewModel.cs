using DataAccess.Models;

namespace BusinessLogic.Models;

public class PointsForVictoryViewModel
{
    public int Id { get; set; }
    public int Round { get; set; }
    public int Points { get; set; }

    public static PointsForVictoryViewModel FromDbModel(PointsForVictory model)
    {
        return new PointsForVictoryViewModel
        {
            Id = model.Id,
            Round = model.Round,
            Points = model.Points
        };
    }
}