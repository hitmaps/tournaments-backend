using DataAccess.Models;

namespace BusinessLogic.Models;

public class MatchCompetitorViewModel
{
    public int Id { get; set; }
    public int BracketId { get; set; }
    public string DiscordId { get; set; } = null!;
    public long? ChallongePlayerId { get; set; }
    public string? ChallongeName { get; set; }
    public string? CountryCode { get; set; }
    public int Order { get; set; }

    public static MatchCompetitorViewModel FromDbModel(MatchCompetitorInfo matchCompetitorInfo)
    {
        return new MatchCompetitorViewModel
        {
            Id = matchCompetitorInfo.Competitor.Id,
            BracketId = matchCompetitorInfo.Competitor.Bracket.Id,
            ChallongePlayerId = matchCompetitorInfo.Competitor.ChallongePlayerId,
            ChallongeName = matchCompetitorInfo.Competitor.ChallongeName,
            DiscordId = matchCompetitorInfo.Competitor.DiscordId.ToString(),
            CountryCode = matchCompetitorInfo.Competitor.CountryCode,
            Order = matchCompetitorInfo.Order
        };
    }
}