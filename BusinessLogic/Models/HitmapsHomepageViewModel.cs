using BusinessLogic.Models.UpcomingMatches;

namespace BusinessLogic.Models;

public class HitmapsHomepageViewModel
{
    public List<EventViewModel> RegisterableEvents { get; set; } = new();
    public List<UpcomingMatchEventViewModel> UpcomingMatchEvents { get; set; } = new();
}