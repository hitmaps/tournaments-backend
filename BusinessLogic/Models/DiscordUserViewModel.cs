using Discord.Rest;

namespace BusinessLogic.Models;

public class DiscordUserViewModel
{
    public string DiscordId { get; set; } = null!; //-- Browsers can't handle ulong
    public string Name { get; set; } = null!;

    public static DiscordUserViewModel FromRestGuildUser(RestGuildUser restGuildUser)
    {
        return new DiscordUserViewModel
        {
            DiscordId = restGuildUser.Id.ToString(),
            Name = restGuildUser.Username
        };
    }
}