namespace BusinessLogic.Models;

public class CasterViewModel
{
    public string DiscordId { get; set; } = null!;
    public string Name { get; set; } = null!;
}