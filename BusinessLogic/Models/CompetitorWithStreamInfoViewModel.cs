using DataAccess.Models;

namespace BusinessLogic.Models;

public class CompetitorWithStreamInfoViewModel : CompetitorViewModel
{
    public string StreamUrl { get; set; } = null!;

    public static CompetitorWithStreamInfoViewModel FromDbModel(Competitor dbModel)
    {
        return new CompetitorWithStreamInfoViewModel
        {
            Id = dbModel.Id,
            BracketId = dbModel.Bracket.Id,
            ChallongePlayerId = dbModel.ChallongePlayerId,
            ChallongeName = dbModel.ChallongeName,
            DiscordId = dbModel.DiscordId.ToString(),
            CountryCode = dbModel.CountryCode,
            StreamUrl = dbModel.StreamUrl
        };
    }
}