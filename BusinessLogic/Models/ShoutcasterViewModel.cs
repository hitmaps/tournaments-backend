using Discord.Rest;

namespace BusinessLogic.Models;

public class ShoutcasterViewModel
{
    public DiscordUserViewModel DiscordUser { get; set; } = null!;
    public string? StreamUrl { get; set; }

    public static ShoutcasterViewModel FromDictionaryEntry(RestGuildUser discordUser, string? streamUrl)
    {
        return new ShoutcasterViewModel
        {
            DiscordUser = DiscordUserViewModel.FromRestGuildUser(discordUser),
            StreamUrl = streamUrl
        };
    }
}