using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Models;

public class BasicScheduledMatchViewModel
{
    public int Id { get; set; }
    public long? ChallongeMatchId { get; set; }
    public string? MatchScheduledAt { get; set; }
    public string? MatchCompletedAt { get; set; }
    public List<MatchCompetitorViewModel> Competitors { get; set; } = new();
    public Guid? GameModeMatchId { get; set; }
    public List<MapSelectionViewModel> Maps { get; set; }
    public bool GroupStage { get; set; }
    public string State { get; set; } = null!;
    public int? Round { get; set; }
    public List<int> ForfeitPlayers { get; set; } = [];

    public static BasicScheduledMatchViewModel FromDbModel(MatchInfo model)
    {
        return new BasicScheduledMatchViewModel
        {
            Id = model.Id,
            ChallongeMatchId = model.ChallongeMatchId,
            MatchScheduledAt = model.MatchScheduledAt?.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            MatchCompletedAt = model.MatchCompletedAt?.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            Competitors = model.CompetitorInfo.Select(MatchCompetitorViewModel.FromDbModel).ToList(),
            GameModeMatchId = model.GameModeMatchId,
            Maps = model.MapSelections.OrderBy(x => x.Id).Select(x => MapSelectionViewModel.FromDbModel(x)).ToList(),
            GroupStage = model.GroupStage,
            State = model.State,
            Round = model.Round,
            ForfeitPlayers = model.CompetitorInfo.Where(x => x.Forfeit).Select(x => x.Competitor.Id).ToList()
        };
    }
}
