namespace BusinessLogic.Models;

public class ScoreHistoryModel
{
    public int Score0 { get; set; }
    public int Score1 { get; set; }
}