namespace BusinessLogic.Models;

public class StageViewModel
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string Type { get; set; } = null!;
    public string State { get; set; } = null!;
    public dynamic Settings { get; set; } = null!;
}