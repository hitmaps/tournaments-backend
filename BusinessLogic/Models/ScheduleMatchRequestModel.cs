namespace BusinessLogic.Models;

public class ScheduleMatchRequestModel
{
    public DateTime MatchScheduledAt { get; set; }
    public List<int> CompetitorIds { get; set; } = new();
    public List<MapSelectionViewModel> MapSelections { get; set; } = new();
}