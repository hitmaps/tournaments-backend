namespace BusinessLogic.Models;

public class RegisterPlayerViewModel
{
    public string Platform { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string? Country { get; set; }
    public string StreamPlatform { get; set; } = null!;
    public string? ProfileName { get; set; }
    public string TimeZone { get; set; } = null!;
}