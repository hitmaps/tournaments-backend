namespace BusinessLogic.Models;

public class ForfeitMatchRequestModel
{
    public List<int> ForfeitPlayerIds { get; set; } = [];
}