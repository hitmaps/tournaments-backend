namespace BusinessLogic.Models;

public class EditCompetitorRequestModel
{
    public string Country { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string StreamUrl { get; set; } = null!;
}