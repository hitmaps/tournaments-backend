namespace BusinessLogic.Models;

public class SetMatchAdminRequestModel
{
    public string? MatchAdminDiscordId { get; set; }
}