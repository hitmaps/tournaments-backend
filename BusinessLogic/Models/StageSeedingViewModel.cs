namespace BusinessLogic.Models;

public class StageSeedingViewModel
{
    public int StageId { get; set; }
    public int Seed { get; set; }
    public int CompetitorId { get; set; }
    public string Name { get; set; } = null!;
}