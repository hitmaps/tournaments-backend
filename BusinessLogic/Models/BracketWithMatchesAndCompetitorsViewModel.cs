using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Models;

public class BracketWithMatchesAndCompetitorsViewModel
{
    public int Id { get; set; }
    public string? ChallongeName { get; set; }
    public int ParticipantLimit { get; set; }
    public string Platform { get; set; } = null!;
    public string PlatformIcons { get; set; } = null!;
    public List<MapPoolViewModel> Maps { get; set; } = new();
    public List<CompetitorWithAvatarViewModel> Competitors { get; set; } = new();
    public List<StageWithMatchesViewModel> Stages { get; set; } = new();

    public static BracketWithMatchesAndCompetitorsViewModel FromDbModel(Bracket bracket, List<RestGuildUser> users)
    {
        return new BracketWithMatchesAndCompetitorsViewModel
        {
            Id = bracket.Id,
            ChallongeName = bracket.ChallongeName,
            ParticipantLimit = bracket.ParticipantLimit,
            Platform = bracket.Platform,
            PlatformIcons = bracket.PlatformIcons,
            Maps = bracket.Maps.Select(MapPoolViewModel.FromDbModel).ToList(),
            Competitors = bracket.Competitors
                .Select(x => CompetitorWithAvatarViewModel.FromDbModel(x, users))
                .OrderBy(x => x.ChallongeName)
                .ToList(),
            Stages = bracket.Stages.Select(x => StageWithMatchesViewModel.FromDbModel(x, users)).ToList()
        };
    }
}