using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Models;

public class CompletedMatchViewModel
{
    public int Id { get; set; }
    public long? ChallongeMatchId { get; set; }
    public string MatchScheduledAt { get; set; } = null!;
    public string MatchCompletedAt { get; set; } = null!;
    public List<CompetitorViewModel> Competitors { get; set; } = new();
    public Guid? GameModeMatchId { get; set; }
    public List<CompletedMapSelectionViewModel> Maps { get; set; }
    public string Platform { get; set; } = null!;
    public int? Round { get; set; }

    public static CompletedMatchViewModel FromDbModel(MatchInfo model)
    {
        return new CompletedMatchViewModel
        {
            Id = model.Id,
            ChallongeMatchId = model.ChallongeMatchId,
            MatchScheduledAt = model.MatchScheduledAt!.Value.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            MatchCompletedAt = model.MatchCompletedAt!.Value.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            Competitors = model.CompetitorInfo.Select(x => CompetitorViewModel.FromDbModel(x.Competitor)).ToList(),
            GameModeMatchId = model.GameModeMatchId,
            Maps = model.MapSelections.OrderBy(x => x.Id).Select(x => CompletedMapSelectionViewModel.FromDbModel(x, true)).ToList(),
            Platform = model.Stage.Bracket.Platform,
            Round = model.Round
        };
    }
}
