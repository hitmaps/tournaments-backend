using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Models;

public class ScheduledMatchViewModel : BasicScheduledMatchViewModel
{
    public MatchCastViewModel? Cast { get; set; } 
    public MatchAdminViewModel? MatchAdmin { get; set; }

    public static ScheduledMatchViewModel FromDbModel(MatchInfo model, List<RestGuildUser> guildUsers)
    {
        return new ScheduledMatchViewModel
        {
            Id = model.Id,
            ChallongeMatchId = model.ChallongeMatchId,
            MatchScheduledAt = model.MatchScheduledAt?.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            MatchCompletedAt = model.MatchCompletedAt?.ToString("yyyy-MM-ddTHH:mm:ssZ"),
            Competitors = model.CompetitorInfo.Select(MatchCompetitorViewModel.FromDbModel).ToList(),
            GameModeMatchId = model.GameModeMatchId,
            Maps = model.MapSelections.OrderBy(x => x.Id).Select(x => MapSelectionViewModel.FromDbModel(x)).ToList(),
            Cast = MatchCastViewModel.FromDbCollection(model.Casters, guildUsers),
            MatchAdmin = MatchAdminViewModel.FromDbModel(model.MatchAdminDiscordId, guildUsers),
            GroupStage = model.GroupStage,
            State = model.State,
            Round = model.Round,
            ForfeitPlayers = model.CompetitorInfo.Where(x => x.Forfeit).Select(x => x.Competitor.Id).ToList()
        };
    }
}
