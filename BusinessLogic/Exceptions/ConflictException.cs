namespace BusinessLogic.Exceptions;

public class ConflictException<T> : Exception
{
    public ConflictException(T expected, T actual) : base($"Expected '{expected}', got '{actual}'")
    {
    }
}