namespace BusinessLogic.Exceptions;

public class NotFoundException<T, TU> : Exception
{
    public NotFoundException(TU value) : base($"Could not find {typeof(T).Name} with value '{value}'")
    {
    }
}