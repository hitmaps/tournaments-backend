namespace BusinessLogic.Challonge;

public class UnofficialChallongeApiTournamentResponse
{
    public List<RoundLabels> Rounds { get; set; } = new();
}