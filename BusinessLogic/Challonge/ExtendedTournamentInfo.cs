using Challonge.Objects;

namespace BusinessLogic.Challonge;

public class ExtendedTournamentInfo
{
    public Tournament Bracket { get; set; } = null!;
    public List<Match> Matches { get; set; } = new();
    public List<Participant> Participants { get; set; } = new();
    public List<RoundLabels> RoundLabels { get; set; } = new();
}