namespace BusinessLogic.Challonge;

public class RoundLabels
{
    public int Number { get; set; }
    public string Title { get; set; } = null!;
}