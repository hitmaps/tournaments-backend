using BusinessLogic.Models;
using Challonge.Objects;

namespace BusinessLogic.Challonge;

public interface IChallongeService
{
    Task<long> RegisterUserAsync(string tournamentName, string playerName);

    Task WithdrawUserAsync(string tournamentName, long participantId);
    Task<Tournament> FetchTournamentAsync(string tournamentName);

    Task<ExtendedTournamentInfo> FetchTournamentsWithMatchesAsync(string tournamentName);
    Task<ExtendedTournamentInfo> FetchTournamentsWithExtendedInfoAsync(string tournamentName);

    Task<IEnumerable<Participant>> FetchParticipantsAsync(string tournamentName);

    Task<Match> FetchMatchAsync(string tournamentName, long matchId);

    Task SendScoreAsync(string tournamentName, long matchId, List<ScoreHistoryModel> scores, long? winnerId);
    Task<List<RoundLabels>> FetchRoundLabelsAsync(long challongeTournamentId);
}