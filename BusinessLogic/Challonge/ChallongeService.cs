using System.Net;
using System.Text.Json;
using BusinessLogic.Models;
using Challonge.Api;
using Challonge.Objects;
using Rollbar;

namespace BusinessLogic.Challonge;

public class ChallongeService : IChallongeService
{
    private readonly IChallongeClient _client;
    private readonly HttpClientWrapper _httpClientWrapper;

    private readonly Dictionary<long, ExpirableEntry<List<RoundLabels>>> cachedRoundLabels = new();

    public ChallongeService(IChallongeClient client, HttpClientWrapper httpClientWrapper)
    {
        _client = client;
        _httpClientWrapper = httpClientWrapper;
    }

    public async Task<long> RegisterUserAsync(string tournamentName, string playerName)
    {
        return (await _client.CreateParticipantAsync(tournamentName, new ParticipantInfo
        {
            Name = playerName
        })).Id;
    }

    public async Task WithdrawUserAsync(string tournamentName, long participantId)
    {
        var participant = await _client.GetParticipantAsync(tournamentName, participantId);
        
        await _client.DeleteParticipantAsync(participant);
    }

    public async Task<Tournament> FetchTournamentAsync(string tournamentName)
    {
        return await _client.GetTournamentByUrlAsync(tournamentName);
    }

    public async Task<ExtendedTournamentInfo> FetchTournamentsWithMatchesAsync(string tournamentName)
    {
        var tournament = await _client.GetTournamentByUrlAsync(tournamentName);
        return new ExtendedTournamentInfo
        {
            Bracket = tournament,
            Matches = (await _client.GetMatchesAsync(tournament)).ToList()
        };
    }

    public async Task<ExtendedTournamentInfo> FetchTournamentsWithExtendedInfoAsync(string tournamentName)
    {
        var tournament = await _client.GetTournamentByUrlAsync(tournamentName);
        return new ExtendedTournamentInfo
        {
            Bracket = tournament,
            Matches = (await _client.GetMatchesAsync(tournament)).ToList(),
            Participants = (await _client.GetParticipantsAsync(tournament)).ToList(),
            RoundLabels = await FetchRoundLabelsAsync(tournament.Id)
        };
    }

    public async Task<IEnumerable<Participant>> FetchParticipantsAsync(string tournamentName)
    {
        return await _client.GetParticipantsAsync(tournamentName);
    }

    public async Task<Match> FetchMatchAsync(string tournamentName, long matchId)
    {
        var tournament = await _client.GetTournamentByUrlAsync(tournamentName);
        
        return await _client.GetMatchAsync(tournament, matchId);
    }

    public async Task SendScoreAsync(string tournamentName, long matchId, List<ScoreHistoryModel> scores, long? winnerId)
    {
        var tournament = await _client.GetTournamentByUrlAsync(tournamentName);
        var match = await _client.GetMatchAsync(tournament, matchId);
        await _client.UpdateMatchAsync(match, new MatchInfo(IsMatchTie(scores))
        {
            Scores = scores.Select(x => new Score(x.Score0, x.Score1)),
            WinnerId = winnerId
        });
    }

    private static bool IsMatchTie(IReadOnlyCollection<ScoreHistoryModel> scores)
    {
        return scores.Sum(x => x.Score0) == scores.Sum(x => x.Score1);
    }

    public async Task<List<RoundLabels>> FetchRoundLabelsAsync(long challongeTournamentId)
    {

        if (cachedRoundLabels.ContainsKey(challongeTournamentId) &&
            !cachedRoundLabels[challongeTournamentId].IsExpired())
        {
            return cachedRoundLabels[challongeTournamentId].Value;
        }
        
        using var request = new HttpRequestMessage(HttpMethod.Get,
            $"https://challonge.com/tournaments/{challongeTournamentId}");
        request.Headers.TryAddWithoutValidation("Accept", "application/json");

        try
        {
            var response = await _httpClientWrapper.GetClient().SendAsync(request);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return new List<RoundLabels>();
            }

            var roundLabels = JsonSerializer.Deserialize<UnofficialChallongeApiTournamentResponse>(
                await response.Content.ReadAsStreamAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });
            cachedRoundLabels[challongeTournamentId] = new ExpirableEntry<List<RoundLabels>>(roundLabels!.Rounds, TimeSpan.FromDays(1));
            return roundLabels.Rounds;
        }
        catch (Exception e)
        {
            RollbarLocator.RollbarInstance.AsBlockingLogger(TimeSpan.FromSeconds(1)).Error(e);
            return new List<RoundLabels>();
        }
    }
}