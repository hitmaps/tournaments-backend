namespace BusinessLogic;

public class ThreadSafeExecutor
{
    private readonly object lockObject = new();

    public void DoInThreadSafety(Action func)
    {
        lock (lockObject)
        {
            func.Invoke();
        }
    }

    public Task DoInThreadSafety(Func<Task> func)
    {
        lock (lockObject)
        {
            return func.Invoke();
        }
    }
}