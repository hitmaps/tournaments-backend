namespace BusinessLogic.Discord;

public class DiscordUserContext
{
    public string TokenType { get; }
    public string TokenValue { get; }

    public DiscordUserContext(string tokenType, string tokenValue)
    {
        TokenType = tokenType;
        TokenValue = tokenValue;
    }
}