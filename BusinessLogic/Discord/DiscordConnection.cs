namespace BusinessLogic.Discord;

public class DiscordConnection
{
    public string Id { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string Type { get; set; } = null!;
    public bool? Revoked { get; set; }
}