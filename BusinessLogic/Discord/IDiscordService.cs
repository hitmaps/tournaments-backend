using Discord;
using Discord.Rest;

namespace BusinessLogic.Discord;

public interface IDiscordService
{
    Task<string?> VerifyTokenAsync(string tokenType, string token);
    Task<List<DiscordConnection>> FetchUserConnectionsAsync(string tokenType, string token);
    Task<DiscordRestClient> GetClientAsync();
    Task<List<RestGuildUser>> GetUsersAsync(ulong guildId);
    Task<RestUserMessage> SendMessageAsync(ulong guildId, ulong channelId, string? message = null, Embed? embed = null, MessageComponent? component = null);
    Task DeleteMessageAsync(ulong guildId, ulong channelId, ulong messageId);
    Task AddRoleToUserAsync(ulong guildId, ulong snowflake, ulong roleId);
    Task RemoveRoleFromUserAsync(ulong guildId, ulong snowflake, ulong roleId);
    Task<string> GetAvatarUrlAsync(ulong guildId, ulong snowflake);
    Task<bool> DoesUserHaveRolesAsync(string guildId, string userId, List<string> roleIds);

    Task<RestGuildUser?> GetUserForGuildAsync(ulong guildId, ulong snowflake);

    Task<List<RestGuildUser>> GetUsersWithRoleAsync(ulong guildId, List<string> roleIds);
}