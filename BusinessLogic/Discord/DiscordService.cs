using System.Net;
using System.Net.Http.Headers;
using System.Text.Json;
using Discord;
using Discord.Rest;
using Microsoft.Extensions.Configuration;

namespace BusinessLogic.Discord;

public class DiscordService : IDiscordService
{
    private readonly IConfiguration _configuration;
    private DiscordRestClient? _client;
    private readonly HttpClientWrapper _httpClientWrapper;
    private readonly Dictionary<ulong, ExpirableEntry<RestGuild>> guildCache = new();
    private readonly Dictionary<ulong, ExpirableEntry<List<RestGuildUser>>> userCache = new();
    private readonly object lockObject = new();

    public DiscordService(IConfiguration configuration, HttpClientWrapper httpClientWrapper)
    {
        _configuration = configuration;
        _httpClientWrapper = httpClientWrapper;
    }

    public async Task<string?> VerifyTokenAsync(string tokenType, string token)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            "https://discordapp.com/api/v9/users/@me");
        request.Headers.Authorization = new AuthenticationHeaderValue(tokenType, token);
        var response = await _httpClientWrapper.GetClient().SendAsync(request);

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return null;
        }

        return JsonSerializer.Deserialize<User>(await response.Content.ReadAsStreamAsync(),
            new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            })?.Id;
    }

    public async Task<List<DiscordConnection>> FetchUserConnectionsAsync(string tokenType, string token)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            "https://discordapp.com/api/v9/users/@me/connections");
        request.Headers.Authorization = new AuthenticationHeaderValue(tokenType, token);
        var response = await _httpClientWrapper.GetClient().SendAsync(request);

        if (response.StatusCode != HttpStatusCode.OK)
        {
            return new List<DiscordConnection>();
        }

        return JsonSerializer.Deserialize<List<DiscordConnection>>(await response.Content.ReadAsStreamAsync(),
            new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            })!;
    }

    public async Task<DiscordRestClient> GetClientAsync()
    {
        // ReSharper disable once InvertIf
        if (_client == null)
        {
            _client = new DiscordRestClient();
            await _client.LoginAsync(TokenType.Bot, _configuration["DiscordBotToken"]);
        }

        return _client;
    }

    public async Task<RestUserMessage> SendMessageAsync(ulong guildId, ulong channelId, string? message = null, 
        Embed? embed = null, 
        MessageComponent? component = null)
    {
        var guild = await GetGuildAsync(guildId);
        var channel = await guild.GetTextChannelAsync(channelId);
        
        return await channel.SendMessageAsync(message, embed: embed, components: component);
    }

    public async Task DeleteMessageAsync(ulong guildId, ulong channelId, ulong messageId)
    {
        var guild = await GetGuildAsync(guildId);
        var channel = await guild.GetTextChannelAsync(channelId);
        await channel.DeleteMessageAsync(messageId);
    }

    private async Task<RestGuild> GetGuildAsync(ulong guildId)
    {
        return await FetchInLock(async () =>
        {
            if (guildCache.ContainsKey(guildId) && !guildCache[guildId].IsExpired())
            {
                return guildCache[guildId].Value;
            }

            var guild = await (await GetClientAsync()).GetGuildAsync(guildId);

            guildCache[guildId] = new ExpirableEntry<RestGuild>(guild, TimeSpan.FromMinutes(10));

            return guild;
        });
    }
    
    private T FetchInLock<T>(Func<T> function)
    {
        lock (lockObject)
        {
            return function.Invoke();
        }
    }
    
    public async Task<List<RestGuildUser>> GetUsersAsync(ulong guildId)
    {
        return await FetchInLock(async () =>
        {
            if (userCache.ContainsKey(guildId) && !userCache[guildId].IsExpired())
            {
                return userCache[guildId].Value;
            }

            var guild = await GetGuildAsync(guildId);
            var users = (await guild.GetUsersAsync().FlattenAsync()).ToList();

            userCache[guildId] = new ExpirableEntry<List<RestGuildUser>>(users, TimeSpan.FromMinutes(10));

            return users;
        });
    }

    public async Task AddRoleToUserAsync(ulong guildId, ulong snowflake, ulong roleId)
    {
        var guild = await GetGuildAsync(guildId);
        var member = await guild.GetUserAsync(snowflake);

        await member.AddRoleAsync(roleId);
    }

    public async Task RemoveRoleFromUserAsync(ulong guildId, ulong snowflake, ulong roleId)
    {
        var guild = await GetGuildAsync(guildId);
        var member = await guild.GetUserAsync(snowflake);

        await member.RemoveRoleAsync(roleId);
    }

    public async Task<string> GetAvatarUrlAsync(ulong guildId, ulong snowflake)
    {
        var guild = await GetGuildAsync(guildId);
        var user = await guild.GetUserAsync(snowflake);

        return user.GetDisplayAvatarUrl();
    }

    public async Task<bool> DoesUserHaveRolesAsync(string guildId, string userId, List<string> roleIds)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            $"https://discordapp.com/api/v9/guilds/{guildId}/members/{userId}");
        request.Headers.Authorization = new AuthenticationHeaderValue("Bot", _configuration["DiscordBotToken"]);
        var response = await _httpClientWrapper.GetClient().SendAsync(request);

        try
        {
            response.EnsureSuccessStatusCode();
        }
        catch (HttpRequestException)
        {
            return false;
        }

        var guildMember = JsonSerializer.Deserialize<GuildMember>(await response.Content.ReadAsStreamAsync(),
            new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });

        return guildMember != null && roleIds.All(guildMember.Roles.Contains);
    }

    public async Task<RestGuildUser?> GetUserForGuildAsync(ulong guildId, ulong snowflake)
    {
        var guild = await GetGuildAsync(guildId);
        
        return await guild.GetUserAsync(snowflake);
    }

    public async Task<List<RestGuildUser>> GetUsersWithRoleAsync(ulong guildId, List<string> roleIds)
    {
        var users = await GetUsersAsync(guildId);

        return users.Where(x => roleIds.Any(y => x.RoleIds.Contains(ulong.Parse(y)))).ToList();
    }
}