using BusinessLogic.Discord;
using BusinessLogic.Hitmaps;
using DataAccess.Data;
using DataAccess.Models;
using Discord;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Batch.PostMatchCompleteMessage;

public class PostMatchCompleteMessage
{
    private readonly TournamentDbContext dbContext;
    private readonly IDiscordService discordService;

    public PostMatchCompleteMessage(TournamentDbContext dbContext, IDiscordService discordService)
    {
        this.dbContext = dbContext;
        this.discordService = discordService;
    }

    public async Task Run(int matchId)
    {
        //@formatter:off
        var matchInfo = dbContext.Matchups
            .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
                .ThenInclude(x => x.Competitor)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Winner)
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
                    .ThenInclude(x => x.Event)
            .FirstOrDefault(x => x.Id == matchId);
        //@formatter:on
        var theEvent = matchInfo?.Stage.Bracket.Event;
        if (matchInfo == null || theEvent?.DiscordMatchupChannelId == null)
        {
            return;
        }

        var competitors = matchInfo.CompetitorInfo.Select(x => x.Competitor).ToList();
        var drawCount = GetMapCount(matchInfo, null);
        var competitorOneWins = GetMapCount(matchInfo, competitors[0].Id);
        var competitorTwoWins = GetMapCount(matchInfo, competitors[1].Id);
        var competitorOneScore = competitorOneWins * 2 + drawCount;
        var competitorTwoScore = competitorTwoWins * 2 + drawCount;
        var embed = new EmbedBuilder()
            .WithTitle(
                $":checkered_flag: {competitorOneScore} - {competitorTwoScore} | {string.Join(" v ", competitors.Select(x => x.ChallongeName).ToList())}");

        var playedMaps = matchInfo.MapSelections.Where(x => x.State == MapSelectionState.Complete.Value).ToList();
        if (playedMaps.Any())
        {
            var formattedPlayedMaps = new List<string>();
            foreach (var playedMap in playedMaps)
            {
                var result = playedMap.Winner == null ? "Draw" : $"Won by <@{playedMap.Winner.DiscordId}>";
                if (playedMap is { WinnerFinishedAt: not null, MapStartedAt: not null })
                {
                    var elapsedTime = playedMap.WinnerFinishedAt.Value - playedMap.MapStartedAt.Value;
                    result += $" [{(int)elapsedTime.TotalMinutes}:{elapsedTime.Seconds:00} RTA]";
                }
                formattedPlayedMaps.Add($"**{playedMap.Mission.Name} ({playedMap.Mission.Location})**: {result}");
            }
            embed.AddField("Played Maps", string.Join("\n", formattedPlayedMaps));
        }

        var unplayedMaps = matchInfo.MapSelections.Where(x => x.State == MapSelectionState.Ready.Value).ToList();
        if (unplayedMaps.Any())
        {
            var formattedUnplayedMaps = unplayedMaps.Select(playedMap => $"{playedMap.Mission.Name} ({playedMap.Mission.Location})").ToList();
            embed.AddField("Unplayed Maps", string.Join("\n", formattedUnplayedMaps));
        }

        await discordService.SendMessageAsync(theEvent.DiscordGuildId, theEvent.DiscordMatchupChannelId.Value, 
            $"{string.Join(" / ", competitors.Select(x => $"<@{x.DiscordId}>"))} - Match is complete!",
            embed.Build());
    }
    
    private static int GetMapCount(MatchInfo matchInfo, int? winnerId)
    {
        return winnerId == null
            ? matchInfo.MapSelections.Count(x => x.State == MapSelectionState.Complete.Value && x.Winner == null)
            : matchInfo.MapSelections.Count(x => x.State == MapSelectionState.Complete.Value && x.Winner?.Id == winnerId.Value);
    }
}