﻿using BusinessLogic.Discord;
using BusinessLogic.Extensions;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
namespace BusinessLogic.Batch.SendMatchStartingNotifications;

public class SendMatchStartingNotifications
{
    private readonly TournamentDbContext dbContext;
    private readonly IDiscordService discordService;

    private const string MessageToPost = "{0}:\n\n" +
                                        "Your {1} match starts at {2} ({3}).\n\n" +
                                        "Be live on-stream at the **{4}** loadout menu.\n\n" +
                                        "Upon 5-star SA completion of your roulette spin, click \"Done\" on your spin link page.\n\n" +
                                        "Good luck to you both!";

    public SendMatchStartingNotifications(TournamentDbContext dbContext, IDiscordService discordService)
    {
        this.dbContext = dbContext;
        this.discordService = discordService;
    }

    public async Task ProgramLogicAsync()
    {
        // 1. Get all matches that are starting in fifteen minutes or less, but not already in-progress
        //@formatter:off
        var matches = dbContext.Matchups
            .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
                .ThenInclude(x => x.Competitor)
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
                    .ThenInclude(x => x.Event)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Where(x => x.Stage.Bracket.Event.DiscordMatchupChannelId.HasValue && 
                        x.State == MatchInfo.StateScheduled &&
                        !x.FifteenMinuteWarningPosted &&
                        !x.MatchCompletedAt.HasValue).ToList();
        //@formatter:on

        matches = matches.Where(x => (x.MatchScheduledAt!.Value - DateTime.UtcNow).TotalMinutes is <= 15 and > 0)
            .ToList();

        foreach (var match in matches)
        {
            var formattedCompetitors = string.Join(" / ", match.CompetitorInfo.Select(x => $"<@{x.Competitor.DiscordId}>").ToList());
            var firstMapPick = match.MapSelections.First(x => x.MissionType == MissionType.Pick);
            var missionNameLocation = $"{firstMapPick.Mission.Name} ({firstMapPick.Mission.Location})";
            var formattedMessage = string.Format(MessageToPost,
                formattedCompetitors,
                match.Stage.Bracket.Event.Name,
                $"<t:{match.MatchScheduledAt!.Value.ToUnixTimestamp()}:t>",
                $"<t:{match.MatchScheduledAt!.Value.ToUnixTimestamp()}:R>",
                missionNameLocation);
            
            // Post match warning
            await discordService.SendMessageAsync(match.Stage.Bracket.Event.DiscordGuildId,
                match.Stage.Bracket.Event.DiscordMatchupChannelId!.Value,
                formattedMessage);

            // Update match record
            match.FifteenMinuteWarningPosted = true;
            dbContext.Update(match);
        }

        await dbContext.SaveChangesAsync();
        Console.WriteLine($"Process completed successfully.  Found {matches.Count} matches to post.");
    } 
}