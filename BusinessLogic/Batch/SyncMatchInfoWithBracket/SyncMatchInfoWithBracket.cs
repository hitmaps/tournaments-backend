using System.Security.Cryptography;
using BusinessLogic.BracketManager;
using BusinessLogic.Hitmaps;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Bracket = BusinessLogic.BracketManager.Bracket;

namespace BusinessLogic.Batch.SyncMatchInfoWithBracket;

public class SyncMatchInfoWithBracket
{
    private readonly TournamentDbContext dbContext;
    private readonly IStageService stageService;

    public SyncMatchInfoWithBracket(TournamentDbContext dbContext, IStageService stageService)
    {
        this.dbContext = dbContext;
        this.stageService = stageService;
    }

    public async Task SyncMatchInfo(int stageId)
    {
        // @formatter:off
        var stage = await dbContext.Stages
            .Include(x => x.Bracket)
                .ThenInclude(x => x.Competitors)
            .Include(x => x.ScheduledMatches)
                .ThenInclude(x => x.CompetitorInfo)
                    .ThenInclude(x => x.Competitor)
            .FirstAsync(x => x.Id == stageId);
        // @formatter:on

        var bracket = await stageService.GetBracketAsync<Bracket>(stageId);
        var bracketCompetitorToHitmapsCompetitor = AlignCompetitors(bracket.Participant, stage.Bracket.Competitors);

        var anyChanges = false;
        foreach (var match in bracket.Match)
        {
            var matchInfo =
                await dbContext.Matchups
                    .Include(x => x.Stage)
                    .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
                    .FirstOrDefaultAsync(x => x.Stage.Id == stageId && x.ChallongeMatchId == match.Id) ??
                new MatchInfo
                {
                    Stage = stage,
                    ChallongeMatchId = match.Id,
                    GroupStage = false,
                    Round = bracket.Round.First(x => x.Id == match.RoundId).Number,
                    State = MatchInfo.StateNeedsScheduling
                };

            if (matchInfo.CompetitorInfo.Count == 2)
            {
                // Both competitors are already synced. Skipping.
                continue;
            }

            if (match.OpponentOne.Id.HasValue && matchInfo.CompetitorInfo.All(x => x.Order != 1))
            {
                anyChanges = true;
                dbContext.Update(new MatchCompetitorInfo
                {
                    Match = matchInfo,
                    Competitor = bracketCompetitorToHitmapsCompetitor[match.OpponentOne.Id.Value],
                    Order = 1,
                    Forfeit = false,
                    PublicId = GenerateRandomHexString()
                });
            }

            // ReSharper disable once InvertIf
            if (match.OpponentTwo.Id.HasValue && matchInfo.CompetitorInfo.All(x => x.Order != 2))
            {
                anyChanges = true;
                dbContext.Update(new MatchCompetitorInfo
                {
                    Match = matchInfo,
                    Competitor = bracketCompetitorToHitmapsCompetitor[match.OpponentTwo.Id.Value],
                    Order = 2,
                    Forfeit = false,
                    PublicId = GenerateRandomHexString()
                });
            }
        }

        if (anyChanges)
        {
            await dbContext.SaveChangesAsync();
        }
    }

    private static Dictionary<int, Competitor> AlignCompetitors(List<BracketParticipant> bracketParticipant, List<Competitor> competitors)
    {
        var results = new Dictionary<int, Competitor>();
        foreach (var participant in bracketParticipant)
        {
            var competitor = competitors.First(x => x.ChallongeName == participant.Name);
            results[participant.Id] = competitor;
        }

        return results;
    }
    
    private static string GenerateRandomHexString()
    {
        var value = RandomNumberGenerator.GetBytes(8);

        return string.Join("", value.Select(x => x.ToString("x2")));
    }
}