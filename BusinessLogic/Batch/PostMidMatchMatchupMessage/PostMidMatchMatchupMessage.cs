using BusinessLogic.Discord;
using BusinessLogic.Extensions;
using BusinessLogic.Hitmaps;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Batch.PostMidMatchMatchupMessage;

public class PostMidMatchMatchupMessage
{
    private readonly TournamentDbContext dbContext;
    private readonly IDiscordService discordService;

    public PostMidMatchMatchupMessage(TournamentDbContext dbContext, IDiscordService discordService)
    {
        this.dbContext = dbContext;
        this.discordService = discordService;
    }

    public async Task ProgramLogicAsync(int matchupId, bool postTimeMessage, bool postScoreMessage)
    {
        //@formatter:off
        var matchup = dbContext.Matchups
            .AsSplitQuery()
            .Include("Stage.Bracket.Event")
            .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
                .ThenInclude(x => x.Competitor)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Winner)
            .FirstOrDefault(x => x.Id == matchupId);
        //@formatter:on
        var theEvent = matchup?.Stage.Bracket.Event;
        if (matchup == null || theEvent?.DiscordMatchupChannelId == null || (!postScoreMessage && !postTimeMessage))
        {
            return;
        }

        var firstMap = matchup.MapSelections.OrderBy(x => x.Id).First(x => x.MissionType == MissionType.Pick);
        var lastCompletedMap = matchup.MapSelections.Where(x => x.ResultVerifiedAt.HasValue)
            .OrderByDescending(x => x.ResultVerifiedAt!.Value)
            .FirstOrDefault();
        var currentMap = matchup.MapSelections.Where(x => x.State == MapSelectionState.Ready.Value).OrderBy(x => x.Id).FirstOrDefault();
        var competitors = matchup.CompetitorInfo
            .OrderBy(x => x.Order)
            .ToList();
        var heading = string.Join(" / ", competitors.Select(x => $"<@{x.Competitor.DiscordId}>").ToList()) + "\n";
        var message = string.Empty;
        if (postScoreMessage && lastCompletedMap != null)
        {
            var drawCount = GetMapCount(matchup, null);
            var competitorOneWins = GetMapCount(matchup, competitors[0].Competitor.Id);
            var competitorTwoWins = GetMapCount(matchup, competitors[1].Competitor.Id);

            var lastWinner = $"{lastCompletedMap.Mission.Name} ended in a tie.";
            if (lastCompletedMap.Winner != null)
            {
                var rtaString = string.Empty;
                if (lastCompletedMap.WinnerFinishedAt.HasValue && lastCompletedMap.MapStartedAt.HasValue)
                {
                    var elapsedTime = lastCompletedMap.WinnerFinishedAt.Value - lastCompletedMap.MapStartedAt.Value;
                    rtaString = $" [{(int)elapsedTime.TotalMinutes}:{elapsedTime.Seconds:00} RTA]";
                }
                
                lastWinner = $"**{lastCompletedMap.Winner.ChallongeName}** won {lastCompletedMap.Mission.Name}{rtaString}.";
            }
                
            message +=
                $"{lastWinner} Score **{competitorOneWins * 2 + drawCount}-{competitorTwoWins * 2 + drawCount}**.\n";
        }

        if (postTimeMessage && // Should we post it to begin with?
            currentMap != null && 
            (firstMap.Id != currentMap.Id || matchup.MatchScheduledAt != currentMap.MapStartedAt) && // Is it not the first map, or the first map at a different time? 
            currentMap.MapStartedAt > DateTime.UtcNow) // Is the spin already live? If so, don't post.
        {
            message += $"**{currentMap.Mission.Name} ({currentMap.Mission.Location})** is next at **<t:{currentMap.MapStartedAt!.Value.ToUnixTimestamp()}:t>**.";
        }

        if (message == string.Empty)
        {
            return;
        }

        var messageId = await discordService.SendMessageAsync(theEvent.DiscordGuildId, 
            theEvent.DiscordMatchupChannelId.Value,
            $"{heading}\n{message}");
        if (postScoreMessage && lastCompletedMap != null)
        {
            lastCompletedMap.MapCompleteMessageId = messageId.Id;
            dbContext.Update(lastCompletedMap);
            await dbContext.SaveChangesAsync();
        }
    }

    private static int GetMapCount(MatchInfo matchInfo, int? winnerId)
    {
        return winnerId == null
            ? matchInfo.MapSelections.Count(x => x.State == MapSelectionState.Complete.Value && x.Winner == null)
            : matchInfo.MapSelections.Count(x => x.State == MapSelectionState.Complete.Value && x.Winner?.Id == winnerId.Value);
    }
}