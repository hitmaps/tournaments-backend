using BusinessLogic.Discord;
using BusinessLogic.Models;
using DataAccess.Data;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Batch.CloseRegistrations;

public class CloseRegistrations
{
    private readonly TournamentDbContext dbContext;
    private readonly IDiscordService discordService;

    public CloseRegistrations(TournamentDbContext dbContext, IDiscordService discordService)
    {
        this.dbContext = dbContext;
        this.discordService = discordService;
    }

    public async Task ProgramLogicAsync()
    {
        // Get events in 'Pending' state and registration is closed
        // @formatter:off
        var closedEvents = await dbContext.Events
            .Where(x => x.State == State.Pending.Value && x.RegistrationEndsAt < DateTime.UtcNow)
            .ToListAsync();
        // @formatter:on

        foreach (var closedEvent in closedEvents)
        {
            closedEvent.State = State.Underway.Value;

            if (closedEvent.SignupMessageId.HasValue)
            {
                _ = discordService.DeleteMessageAsync(closedEvent.DiscordGuildId, closedEvent.DiscordSignupChannelId,
                    closedEvent.SignupMessageId.Value);
            }

            dbContext.Update(closedEvent);
            await dbContext.SaveChangesAsync();
        }
    }
}