using Newtonsoft.Json;

namespace BusinessLogic.RrStats;

public class RrStatsMapRecordModel
{
    public string Map { get; set; } = null!;
    public int Time { get; set; }
    public List<string> Players { get; set; } = new();
    public List<string> Competitions { get; set; } = new();
}