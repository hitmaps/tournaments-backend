namespace BusinessLogic.RrStats;

public interface IRrStatsService
{
    Task<RrStatsModel> GetStatsForMatchAsync(int matchId, string locationName);

    Task<string> GetAccoladeForPlayerAsync(ulong discordId);
}