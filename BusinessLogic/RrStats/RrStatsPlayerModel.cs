namespace BusinessLogic.RrStats;

public class RrStatsPlayerModel
{
    public int WinsGlobal { get; set; }
    public int LossesGlobal { get; set; }
    public int TiesGlobal { get; set; }
    public int WinsVsOpponent { get; set; }
    public int LossesVsOpponent { get; set; }
    public int TiesVsOpponent { get; set; }
}