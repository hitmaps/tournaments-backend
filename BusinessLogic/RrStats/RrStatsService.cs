using System.Net;
using System.Text.Json;
using BusinessLogic.RrStats.Raw;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Rollbar;

namespace BusinessLogic.RrStats;

public class RrStatsService : IRrStatsService
{
    private readonly TournamentDbContext dbContext;
    private readonly HttpClientWrapper httpClientWrapper;
    private readonly IConfiguration configuration;

    public RrStatsService(TournamentDbContext dbContext, 
        HttpClientWrapper httpClientWrapper, 
        IConfiguration configuration)
    {
        this.dbContext = dbContext;
        this.httpClientWrapper = httpClientWrapper;
        this.configuration = configuration;
    }

    public async Task<RrStatsModel> GetStatsForMatchAsync(int matchId, string locationName)
    {
        var matchInfo = dbContext.Matchups
            .Include(x => x.Stage)
            .ThenInclude(x => x.Bracket)
            .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
            .ThenInclude(x => x.Competitor)
            .First(x => x.Id == matchId);

        var firstCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 1).Competitor;
        var secondCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 2).Competitor;

        return new RrStatsModel
        {
            Player0Stats = await GetMapStatsForCompetitorAsync(firstCompetitor, secondCompetitor, locationName),
            Player0PersonalBest = await GetPersonalBestAsync(firstCompetitor, locationName),
            Player1Stats = await GetMapStatsForCompetitorAsync(secondCompetitor, firstCompetitor, locationName),
            Player1PersonalBest = await GetPersonalBestAsync(secondCompetitor, locationName),
            MapRecord = await GetMapRecordAsync(locationName)
        };
    }

    private async Task<RrStatsPlayerModel> GetMapStatsForCompetitorAsync(Competitor competitor, Competitor opponent, string locationName)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            $"https://rrstats.currymaker.net/api/v2/player/{competitor.DiscordId}");
        request.Headers.TryAddWithoutValidation("Authorization", configuration["RrStatsToken"]);
        var response = await httpClientWrapper.GetClient().SendAsync(request);

        var playerModel = new RrStatsPlayerModel();
        if (response.StatusCode != HttpStatusCode.OK)
        {
            return playerModel;
        }

        var deserializedResponse = JsonSerializer.Deserialize<List<RawPlayerResponse>>(
            await response.Content.ReadAsStreamAsync(),
            new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        
        var playerDiscord = competitor.DiscordId.ToString();
        var opponentDiscord = opponent.DiscordId.ToString();
        deserializedResponse!.ForEach(match =>
        {
            match.PlayedMaps.Where(x => x.Map == GetRrStatsLocationForFullLocation(locationName)).ToList().ForEach(map =>
            {
                // Wins
                if ((match.PlayerOneDiscord == playerDiscord && map.Winner == 1) ||
                    (match.PlayerTwoDiscord == playerDiscord && map.Winner == 2))
                {
                    playerModel.WinsGlobal++;

                    if ((match.PlayerOneDiscord == playerDiscord && match.PlayerTwoDiscord == opponentDiscord) ||
                        (match.PlayerOneDiscord == opponentDiscord && match.PlayerTwoDiscord == playerDiscord))
                    {
                        playerModel.WinsVsOpponent++;
                    }
                }
                
                // Losses
                if ((match.PlayerOneDiscord == playerDiscord && map.Winner == 2) ||
                    (match.PlayerTwoDiscord == playerDiscord && map.Winner == 1))
                {
                    playerModel.LossesGlobal++;

                    if ((match.PlayerOneDiscord == playerDiscord && match.PlayerTwoDiscord == opponentDiscord) ||
                        (match.PlayerOneDiscord == opponentDiscord && match.PlayerTwoDiscord == playerDiscord))
                    {
                        playerModel.LossesVsOpponent++;
                    }
                }
                
                // Ties
                // ReSharper disable once InvertIf
                if ((match.PlayerOneDiscord == playerDiscord || match.PlayerTwoDiscord == playerDiscord) && map.Winner == 0)
                {
                    playerModel.TiesGlobal++;

                    if ((match.PlayerOneDiscord == playerDiscord && match.PlayerTwoDiscord == opponentDiscord) ||
                        (match.PlayerOneDiscord == opponentDiscord && match.PlayerTwoDiscord == playerDiscord))
                    {
                        playerModel.TiesVsOpponent++;
                    }
                }
            });
        });
        
        return playerModel;
    }

    private async Task<int?> GetPersonalBestAsync(Competitor competitor, string locationName)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            $"https://rrstats.currymaker.net/api/v2/statistics/{competitor.DiscordId}");
        request.Headers.TryAddWithoutValidation("Authorization", configuration["RrStatsToken"]);
        var response = await httpClientWrapper.GetClient().SendAsync(request);
        if (response.StatusCode != HttpStatusCode.OK)
        {
            return null;
        }
        
        var deserializedResponse = JsonSerializer.Deserialize<RawPlayerStatisticsResponse>(
            await response.Content.ReadAsStreamAsync(),
            new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            })!;
        var arrayIndex = GetRrStatsLocationForFullLocation(locationName);
        if (deserializedResponse.MapPbs.Count < arrayIndex)
        {
            RollbarLocator.RollbarInstance.Warning($"Could not find RRStats personal best at map index {arrayIndex}; array only has {deserializedResponse.MapPbs.Count} entries.", 
                new Dictionary<string, object?>
                {
                    { "Competitor", competitor.ChallongeName },
                    { "Location Name", locationName }
                });
            return null;
        }

        var mapPbIndex = deserializedResponse.MapPbs[arrayIndex];
        if (mapPbIndex.Map == -1)
        {
            // No personal best
            return null;
        }

        return (int?) mapPbIndex.Match!.PlayedMaps[mapPbIndex.Map].TimeTaken;
    }

    private static int GetRrStatsLocationForFullLocation(string locationName) => locationName switch
    {
        "Paris" => 0,
        "Sapienza" => 1,
        "Marrakesh" => 2,
        "Bangkok" => 3,
        "Colorado" => 4,
        "Hokkaido" => 5,
        "Miami" => 6,
        "Santa Fortuna" => 7,
        "Mumbai" => 8,
        "Whittleton Creek" => 9,
        "Isle of Sgàil" => 10,
        "New York" => 11,
        "Haven Island" => 12,
        "Dubai" => 13,
        "Dartmoor" => 14,
        "Berlin" => 15,
        "Chongqing" => 16,
        "Mendoza" => 17,
        "Ambrose Island" => 18
    };

    private async Task<RrStatsMapRecordModel> GetMapRecordAsync(string locationName)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            $"https://rrstats.currymaker.net/api/v2/map/{locationName}");
        request.Headers.TryAddWithoutValidation("Authorization", configuration["RrStatsToken"]);
        var response = await httpClientWrapper.GetClient().SendAsync(request);

        var recordModel = new RrStatsMapRecordModel();
        if (response.StatusCode != HttpStatusCode.OK)
        {
            return recordModel;
        }

        return JsonSerializer.Deserialize<RrStatsMapRecordModel>(
            await response.Content.ReadAsStreamAsync(),
            new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            })!;
    }

    public async Task<string> GetAccoladeForPlayerAsync(ulong discordId)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get,
            $"https://rrstats.currymaker.net/api/v2/accolade/{discordId}");
        request.Headers.TryAddWithoutValidation("Authorization", configuration["RrStatsToken"]);
        var response = await httpClientWrapper.GetClient().SendAsync(request);

        // ReSharper disable once InvertIf
        if (!response.IsSuccessStatusCode)
        {
            RollbarLocator.RollbarInstance.Warning($"Could not find accolade for {discordId}");
            return string.Empty;
        }

        return await response.Content.ReadAsStringAsync();
    }
}