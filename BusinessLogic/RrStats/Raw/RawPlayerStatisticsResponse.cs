using Newtonsoft.Json;

namespace BusinessLogic.RrStats.Raw;

public class RawPlayerStatisticsResponse
{
    public List<RawPersonalBestsResponse> MapPbs { get; set; } = new();
}

public class RawPersonalBestsResponse
{
    public RawPersonalBestsMatchResponse? Match { get; set; }
    public int Map { get; set; }
}

public class RawPersonalBestsMatchResponse
{
    public List<RawPlayerMapResponse> PlayedMaps { get; set; } = new();
}