namespace BusinessLogic.RrStats.Raw;

public class RawPlayerResponse
{
    public List<RawPlayerMapResponse> PlayedMaps { get; set; } = new();
    public string? PlayerOneDiscord { get; set; }
    public string? PlayerTwoDiscord { get; set; }
}