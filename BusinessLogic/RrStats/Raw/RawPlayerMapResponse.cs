using Newtonsoft.Json;

namespace BusinessLogic.RrStats.Raw;

public class RawPlayerMapResponse
{
    public int Map { get; set; }
    public int Winner { get; set; } // 0 == Draw
    public decimal TimeTaken { get; set; }
}