namespace BusinessLogic.RrStats;

public class RrStatsModel
{
    public RrStatsPlayerModel Player0Stats { get; set; } = null!;
    public int? Player0PersonalBest { get; set; }
    public RrStatsPlayerModel Player1Stats { get; set; } = null!;
    public int? Player1PersonalBest { get; set; }
    public RrStatsMapRecordModel MapRecord { get; set; } = null!;
}