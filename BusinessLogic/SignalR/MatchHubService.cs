using BusinessLogic.Discord;
using BusinessLogic.Extensions;
using BusinessLogic.SignalR.ResponseModels;
using DataAccess.Models;
using Discord.Rest;
using Microsoft.AspNetCore.SignalR;

namespace BusinessLogic.SignalR;

public class MatchHubService : IMatchHubService
{
    private readonly IHubContext<MatchHub, IMatchHub> _hubContext;
    private readonly IDiscordService _discordService;

    public MatchHubService(IHubContext<MatchHub, IMatchHub> hubContext, IDiscordService discordService)
    {
        _hubContext = hubContext;
        _discordService = discordService;
    }

    public async Task SendUpdatedMatchupAsync(int matchupId, MatchInfoResponseModel response)
    {
        // Re-set server time
        response.Timer.ServerTime = DateTime.UtcNow.ToApiDateTimeFormat();
        
        await _hubContext.Clients.Group(string.Format(MatchHub.MatchGroupFormat, matchupId)).MatchUpdate(response);
    }

    public async Task SendUpdatedMatchupAsync(int matchupId, MatchInfo matchInfo, List<RestGuildUser>? discordUsers = null)
    {
        discordUsers ??= await _discordService.GetUsersAsync(matchInfo.Stage.Bracket.Event.DiscordGuildId);

        await SendUpdatedMatchupAsync(matchupId, new MatchInfoResponseModel
        {
            StageType = matchInfo.Stage.Type,
            MapSelections = matchInfo.MapSelections.Select(MatchInfoMapSelectionViewModel.FromDbModel)
                .ToList(),
            Competitors = matchInfo.CompetitorInfo
                .Select(x => MatchInfoCompetitorViewModel.FromDbModel(x, discordUsers)).ToList(),
            Timer = TimerViewModel.FromDbModel(matchInfo),
            PointsForVictory = matchInfo.Stage.PointsForVictories.FirstOrDefault(x => x.Round == matchInfo.Round)?.Points
        });
    }

    public async Task SendMessageAsync(int matchupId, string competitorPublicId, Message message)
    {
        await _hubContext.Clients
            .Group(string.Format(MatchHub.MatchParticipantMessageGroupFormat, matchupId, competitorPublicId))
            .NewMessage(MessageResponseModel.FromDbModel(message));
    }

    public async Task SendUpdatedObjectivesAsync(int matchupId, UpdatedObjectivesResponseModel response)
    {
        await _hubContext.Clients.Group(string.Format(MatchHub.MatchAdminGroupFormat, matchupId))
            .ObjectiveUpdate(response);
    }

    public async Task SendGlobalScoreUpdateAsync(int matchupId, GlobalScoreUpdateResponseModel response)
    {
        await _hubContext.Clients.Group(string.Format(MatchHub.GlobalScoreUpdateGroupFormat, matchupId))
            .GlobalScoreUpdate(response);
    }
}