using BusinessLogic.Extensions;
using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.SignalR.ResponseModels;

public class MatchInfoResponseModel
{
    public string StageType { get; set; } = null!;
    public List<MatchInfoMapSelectionViewModel> MapSelections { get; set; } = new();
    public List<MatchInfoCompetitorViewModel> Competitors { get; set; } = new();
    public TimerViewModel Timer { get; set; } = new();
    public int? PointsForVictory { get; set; }
}

public class MatchInfoMapSelectionViewModel
{
    public int Id { get; set; }
    public int? CompetitorId { get; set; }
    public string? CompetitorName { get; set; }
    public string SelectionType { get; set; } = null!;
    public string? MapSlug { get; set; }
    public string? Location { get; set; }
    public string? MapName { get; set; }
    public string? RoundType { get; set; }
    public string State { get; set; } = null!;
    public string? MapJson { get; set; }
    public int? WinnerId { get; set; }

    public static MatchInfoMapSelectionViewModel FromDbModel(MapSelection mapSelection)
    {
        var viewModel = new MatchInfoMapSelectionViewModel
        {
            Id = mapSelection.Id,
            CompetitorId = mapSelection.Competitor?.Id,
            CompetitorName = mapSelection.Competitor?.ChallongeName,
            SelectionType = mapSelection.MissionType.ToString(),
            MapSlug = mapSelection.Mission.HitmapsSlug,
            Location = mapSelection.Mission.Location,
            MapName = mapSelection.Mission.Name,
            RoundType = mapSelection.RoundType,
            State = mapSelection.State,
            MapJson = mapSelection.MapJson,
            WinnerId = mapSelection.Winner?.Id
        };

        // ReSharper disable once InvertIf
        if (!mapSelection.Revealed)
        {
            viewModel.MapSlug = null;
            viewModel.Location = null;
            viewModel.MapName = null;
        }

        return viewModel;
    }
}

public class MatchInfoCompetitorViewModel
{
    public int Id { get; set; }
    public string DiscordId { get; set; } = null!;
    public string AvatarUrl { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string? CountryCode { get; set; }
    public string? LastPing { get; set; }
    public string? CompleteTime { get; set; }
    public bool Forfeit { get; set; }
    public string PublicId { get; set; } = null!;
    public string StreamUrl { get; set; } = null!;

    public static MatchInfoCompetitorViewModel FromDbModel(MatchCompetitorInfo competitorInfo, List<RestGuildUser> discordUsers)
    {
        return new MatchInfoCompetitorViewModel
        {
            Id = competitorInfo.Competitor.Id,
            DiscordId = competitorInfo.Competitor.DiscordId.ToString(),
            AvatarUrl = GetAvatarForUser(competitorInfo.Competitor.DiscordId, discordUsers),
            Name = competitorInfo.Competitor.ChallongeName!,
            CountryCode = competitorInfo.Competitor.CountryCode,
            LastPing = competitorInfo.LastPing?.ToApiDateTimeFormat(),
            CompleteTime = competitorInfo.CompleteTime?.ToApiDateTimeFormat(),
            Forfeit = competitorInfo.Forfeit,
            PublicId = competitorInfo.PublicId,
            StreamUrl = competitorInfo.Competitor.StreamUrl
        };
    }
    
    private static string GetAvatarForUser(ulong discordId, List<RestGuildUser> users)
    {
        return users.FirstOrDefault(x => x.Id == discordId)?.GetDisplayAvatarUrl() ?? string.Empty;
    }
}

public class TimerViewModel
{
    public int? PausedRemainingTimeInSeconds { get; set; }
    public bool Paused { get; set; }
    public string? StartTime { get; set; }
    public string? EndTime { get; set; }
    public Guid? Fingerprint { get; set; }
    public string ServerTime { get; set; } = DateTime.UtcNow.ToApiDateTimeFormat();
    public int? MatchDurationInMinutes { get; set; }

    public static TimerViewModel FromDbModel(MatchInfo matchInfo)
    {
        return new TimerViewModel
        {
            PausedRemainingTimeInSeconds = matchInfo.TimerPausedRemainingTimeInSeconds,
            Paused = matchInfo.TimerPaused,
            StartTime = matchInfo.TimerStartsAt?.ToApiDateTimeFormat(),
            EndTime = matchInfo.TimerEndsAt?.ToApiDateTimeFormat(),
            Fingerprint = matchInfo.TimerFingerprint,
            MatchDurationInMinutes = !matchInfo.TimerStartsAt.HasValue || !matchInfo.TimerEndsAt.HasValue
                ? null
                : (int)(matchInfo.TimerEndsAt.Value - matchInfo.TimerStartsAt.Value).TotalMinutes
        };
    }
}