using BusinessLogic.SignalR.RequestModels;

namespace BusinessLogic.SignalR.ResponseModels;

public class UpdatedObjectivesResponseModel
{
    public List<Objective> Objectives { get; set; } = new();
}