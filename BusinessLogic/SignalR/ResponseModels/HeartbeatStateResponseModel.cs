namespace BusinessLogic.SignalR.ResponseModels;

public record HeartbeatStateResponseModel(string State)
{
    public static HeartbeatStateResponseModel Sync = new("SYNC");
    public static HeartbeatStateResponseModel Desync = new("DESYNC");
    public static HeartbeatStateResponseModel NotFound = new("NOTFOUND");
}