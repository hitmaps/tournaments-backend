namespace BusinessLogic.SignalR.ResponseModels;

public class GlobalScoreUpdateResponseModel
{
    public int MatchId { get; set; }
    public string Location { get; set; } = null!;
    public int? TimeInSeconds { get; set; }
    public List<GlobalScoreUpdateCompetitor> Competitors { get; set; } = new();
}

public class GlobalScoreUpdateCompetitor
{
    public string Name { get; set; } = null!;
    public int OldScore { get; set; }
    public int NewScore { get; set; }
}