using BusinessLogic.Extensions;
using DataAccess.Models;

namespace BusinessLogic.SignalR.ResponseModels;

public class MessageResponseModel
{
    public int Id { get; set; }
    public int CompetitorId { get; set; }
    public string SentAt { get; set; } = null!;
    public string Content { get; set; } = null!;
    public string? ReceivedAt { get; set; }
    public string? AcknowledgedAt { get; set; }

    public static MessageResponseModel FromDbModel(Message message)
    {
        return new MessageResponseModel
        {
            Id = message.Id,
            CompetitorId = message.CompetitorInfo.Competitor.Id,
            SentAt = message.SentAt.ToApiDateTimeFormat(),
            Content = message.Content,
            ReceivedAt = message.ReceivedAt?.ToApiDateTimeFormat(),
            AcknowledgedAt = message.AcknowledgedAt?.ToApiDateTimeFormat()
        };
    }
}