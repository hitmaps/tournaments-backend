namespace BusinessLogic.SignalR.ResponseModels;

public class HeartbeatUpdateResponseModel
{
    public int CompetitorId { get; set; }
    public string? LastPing { get; set; }
}