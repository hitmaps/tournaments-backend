namespace BusinessLogic.SignalR.RequestModels;

public class HeartbeatRequestModel : MatchIdOnlyRequestModel
{
    public string PublicId { get; set; } = null!;
    public Guid TimerFingerprint { get; set; }
}