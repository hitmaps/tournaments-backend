namespace BusinessLogic.SignalR.RequestModels;

public class CompetitorMessageGroupRequestModel : MatchIdOnlyRequestModel
{
    public string CompetitorPublicId { get; set; } = null!;
}