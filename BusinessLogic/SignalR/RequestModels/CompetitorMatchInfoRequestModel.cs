using BusinessLogic.SignalR.RequestModels;

namespace API.Hubs.RequestModels;

public class CompetitorMatchInfoRequestModel : MatchIdOnlyRequestModel
{
    public string PublicId { get; set; } = null!;
}