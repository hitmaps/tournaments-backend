namespace BusinessLogic.SignalR.RequestModels;

public class CompetitorUpdateMessageStateRequestModel : MatchIdOnlyRequestModel
{
    public string PublicId { get; set; } = null!;
    public string State { get; set; } = null!;
}