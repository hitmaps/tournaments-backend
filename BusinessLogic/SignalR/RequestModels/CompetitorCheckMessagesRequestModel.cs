namespace BusinessLogic.SignalR.RequestModels;

public class CompetitorCheckMessagesRequestModel : MatchIdOnlyRequestModel
{
    public string PublicId { get; set; } = null!;
}