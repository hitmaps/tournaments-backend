namespace BusinessLogic.SignalR.RequestModels;

public class ProcessCompletionRequestModel : AuthenticatedRequestModel
{
    public int MapSelectionId { get; set; }
    public int? CompetitorId { get; set; }
    public string VerifyResult { get; set; } = null!;
}