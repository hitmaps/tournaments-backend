namespace BusinessLogic.SignalR.RequestModels;

public class RemoveLastResultRequestModel : AuthenticatedRequestModel
{
    public int MapSelectionId { get; set; }
}