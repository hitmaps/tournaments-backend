namespace BusinessLogic.SignalR.RequestModels;

public class SendSpinRequestModel : AuthenticatedRequestModel
{
    public int CurrentMapSelectionId { get; set; }
    public DateTime StartTime { get; set; }
    public int? RoundDuration { get; set; }
    public string RoundPayload { get; set; } = null!;
}