namespace BusinessLogic.SignalR.RequestModels;

public class CompetitorNotifyCompletionRequestModel : MatchIdOnlyRequestModel
{
    public string PublicId { get; set; } = null!;
    public Guid TimerFingerprint { get; set; }
}