namespace BusinessLogic.SignalR.RequestModels;

public class AdminSendMessageRequestModel : AuthenticatedRequestModel
{
    public int CompetitorId { get; set; }
    public string Message { get; set; } = null!;
}