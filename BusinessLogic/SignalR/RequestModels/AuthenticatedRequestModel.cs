namespace BusinessLogic.SignalR.RequestModels;

public class AuthenticatedRequestModel : MatchIdOnlyRequestModel
{
    public string DiscordToken { get; set; } = null!;
}