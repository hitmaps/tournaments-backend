namespace BusinessLogic.SignalR.RequestModels;

public class AdminUpdateObjectivesModel : AuthenticatedRequestModel
{
    public List<Objective> Objectives { get; set; } = new();
}

public class Objective
{
    public int ObjectiveIndex { get; set; }
    public int CompetitorIndex { get; set; }
    public bool Completed { get; set; }
}