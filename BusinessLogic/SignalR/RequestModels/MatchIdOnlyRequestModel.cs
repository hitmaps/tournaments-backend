namespace BusinessLogic.SignalR.RequestModels;

public class MatchIdOnlyRequestModel
{
    public int MatchId { get; set; }
}