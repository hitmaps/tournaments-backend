using API.Hubs.RequestModels;
using BusinessLogic.Discord;
using BusinessLogic.Exceptions;
using BusinessLogic.Extensions;
using BusinessLogic.Hitmaps;
using BusinessLogic.Hitmaps.MatchAdministration;
using BusinessLogic.SignalR.RequestModels;
using BusinessLogic.SignalR.ResponseModels;
using DataAccess.Models;
using Discord.Rest;
using Microsoft.AspNetCore.SignalR;

namespace BusinessLogic.SignalR;

public class MatchHub : Hub<IMatchHub>
{
    public const string MatchGroupFormat = "match-{0}"; // Match Updates
    public const string MatchAdminGroupFormat = "match-admin-{0}"; // Objectives, Player Done
    public const string MatchParticipantMessageGroupFormat = "match-participant-message-{0}-{1}"; // Participant receive message
    public const string MatchAdminMessageAckGroupFormat = "match-admin-message-ack-{0}"; // Participant acks message
    public const string GlobalScoreUpdateGroupFormat = "global-score-update"; // Global scoring updates
    private readonly IMatchupService _matchupService;
    private readonly IDiscordService _discordService;
    private readonly IEventService _eventService;
    private readonly IMatchAdminMatchupService _matchAdminMatchupService;

    public MatchHub(IMatchupService matchupService, 
        IDiscordService discordService, 
        IEventService eventService, 
        IMatchAdminMatchupService matchAdminMatchupService)
    {
        _matchupService = matchupService;
        _discordService = discordService;
        _eventService = eventService;
        _matchAdminMatchupService = matchAdminMatchupService;
    }

    #region Groups
    public async Task JoinMatchGroup(MatchIdOnlyRequestModel requestModel)
    {
        await Groups.AddToGroupAsync(Context.ConnectionId, string.Format(MatchGroupFormat, requestModel.MatchId.ToString()));
    }
    public async Task LeaveMatchGroup(MatchIdOnlyRequestModel requestModel)
    {
        await Groups.RemoveFromGroupAsync(Context.ConnectionId, string.Format(MatchGroupFormat, requestModel.MatchId.ToString()));        
    }

    public async Task AdminJoinMatchGroup(MatchIdOnlyRequestModel requestModel)
    {
        await Groups.AddToGroupAsync(Context.ConnectionId, string.Format(MatchAdminGroupFormat, requestModel.MatchId.ToString()));
    }

    public async Task AdminLeaveMatchGroup(MatchIdOnlyRequestModel requestModel)
    {
        // We don't need to authenticate removals
        await Groups.RemoveFromGroupAsync(Context.ConnectionId, string.Format(MatchAdminGroupFormat, requestModel.MatchId.ToString()));
    }

    public async Task CompetitorJoinMessageGroup(CompetitorMessageGroupRequestModel requestModel)
    {
        await Groups.AddToGroupAsync(Context.ConnectionId, 
            string.Format(MatchParticipantMessageGroupFormat, 
                requestModel.MatchId.ToString(), 
                requestModel.CompetitorPublicId));
    }

    public async Task CompetitorLeaveMessageGroup(CompetitorMessageGroupRequestModel requestModel)
    {
        await Groups.RemoveFromGroupAsync(Context.ConnectionId, 
            string.Format(MatchParticipantMessageGroupFormat,
                requestModel.MatchId.ToString(),
                requestModel.CompetitorPublicId));
    }
    public async Task AdminJoinMessageAck(AuthenticatedRequestModel requestModel)
    {
        await VerifyMatchPermissionAsync(requestModel.MatchId, requestModel.DiscordToken);
        await Groups.AddToGroupAsync(Context.ConnectionId,
            string.Format(MatchAdminMessageAckGroupFormat, requestModel.MatchId.ToString()));
    }

    public async Task AdminLeaveMessageAck(MatchIdOnlyRequestModel requestModel)
    {
        // We don't need to authenticate removals
        await Groups.RemoveFromGroupAsync(Context.ConnectionId,
            string.Format(MatchAdminMessageAckGroupFormat, requestModel.MatchId.ToString()));
    }

    public async Task JoinGlobalScoreUpdate()
    {
        await Groups.AddToGroupAsync(Context.ConnectionId, GlobalScoreUpdateGroupFormat);
    }

    public async Task LeaveGlobalScoreUpdate()
    {
        await Groups.RemoveFromGroupAsync(Context.ConnectionId, GlobalScoreUpdateGroupFormat);
    }
    #endregion

    public HeartbeatStateResponseModel SendHeartbeat(HeartbeatRequestModel requestModel)
    {
        var matchInfo = _matchupService.GetMatchupForHeartbeat(requestModel.MatchId, requestModel.PublicId);

        if (matchInfo == null)
        {
            return HeartbeatStateResponseModel.NotFound;
        }

        var competitor = matchInfo.CompetitorInfo.First(x => x.PublicId == requestModel.PublicId);
        Clients.Group(string.Format(MatchAdminGroupFormat, requestModel.MatchId.ToString())).HeartbeatUpdate(new HeartbeatUpdateResponseModel
        {
            CompetitorId = competitor.Competitor.Id,
            LastPing = competitor.LastPing?.ToApiDateTimeFormat()
        });

        return matchInfo.TimerFingerprint != requestModel.TimerFingerprint ? 
            HeartbeatStateResponseModel.Desync : 
            HeartbeatStateResponseModel.Sync;
    }

    public async Task<MatchInfoResponseModel> AdminMatchInfo(MatchIdOnlyRequestModel requestModel)
    {
        var matchInfo = await _matchupService.GetMatchupForMatchHubAsync(requestModel.MatchId);
        var discordUsers = await _discordService.GetUsersAsync(matchInfo.Stage.Bracket.Event.DiscordGuildId);

        return new MatchInfoResponseModel
        {
            StageType = matchInfo.Stage.Type,
            MapSelections = matchInfo.MapSelections.Select(MatchInfoMapSelectionViewModel.FromDbModel).ToList(),
            Competitors = matchInfo.CompetitorInfo.Select(x => MatchInfoCompetitorViewModel.FromDbModel(x, discordUsers)).ToList(),
            Timer = TimerViewModel.FromDbModel(matchInfo),
            PointsForVictory = matchInfo.Stage.PointsForVictories.FirstOrDefault(x => x.Round == matchInfo.Round)?.Points
        };
    }
    
    public async Task<MatchInfoResponseModel> CompetitorMatchInfo(CompetitorMatchInfoRequestModel requestModel)
    {
        var matchInfo = await _matchupService.GetMatchupForMatchHubAsync(requestModel.MatchId);
        if (matchInfo.CompetitorInfo.All(x => x.PublicId != requestModel.PublicId))
        {
            throw new AccessForbiddenException();
        }

        return new MatchInfoResponseModel
        {
            StageType = matchInfo.Stage.Type,
            MapSelections = matchInfo.MapSelections.Select(MatchInfoMapSelectionViewModel.FromDbModel).ToList(),
            Competitors = matchInfo.CompetitorInfo.Select(x => MatchInfoCompetitorViewModel.FromDbModel(x, new List<RestGuildUser>())).ToList(),
            Timer = TimerViewModel.FromDbModel(matchInfo),
            PointsForVictory = matchInfo.Stage.PointsForVictories.FirstOrDefault(x => x.Round == matchInfo.Round)?.Points
        };
    }

    private async Task<MatchInfo> VerifyMatchPermissionAsync(int matchId, string requestDiscordToken)
    {
        var discordToken = VerifyDiscordTokenAsync(requestDiscordToken);
        var matchInfo = await _matchupService.GetMatchupForMatchHubAsync(matchId);
        await _eventService.AssertEventManagementPermissionByStageIdAsync(matchInfo.Stage.Id, ulong.Parse((await discordToken)!));

        return matchInfo;
    }

    public async Task SendSpin(SendSpinRequestModel requestModel)
    {
        var matchInfo = await VerifyMatchPermissionAsync(requestModel.MatchId, requestModel.DiscordToken);
        
        await _matchAdminMatchupService.SendSpinAsync(matchInfo, requestModel);
    }

    public async Task ClearSpin(ClearSpinRequestModel requestModel)
    {
        var matchInfo = await VerifyMatchPermissionAsync(requestModel.MatchId, requestModel.DiscordToken);
        
        await _matchAdminMatchupService.ClearSpinAsync(matchInfo);
    }

    public async Task ProcessCompletion(ProcessCompletionRequestModel requestModel)
    {
        var matchInfo = await VerifyMatchPermissionAsync(requestModel.MatchId, requestModel.DiscordToken);
        
        await _matchAdminMatchupService.ProcessCompletionAsync(matchInfo, requestModel);
    }

    public async Task RemoveLastResult(RemoveLastResultRequestModel requestModel)
    {
        var matchInfo = await VerifyMatchPermissionAsync(requestModel.MatchId, requestModel.DiscordToken);
        
        await _matchAdminMatchupService.RemoveLastResultAsync(matchInfo, requestModel);
    }

    public async Task<MessageResponseModel> AdminSendMessage(AdminSendMessageRequestModel requestModel)
    {
        var matchInfo = await VerifyMatchPermissionAsync(requestModel.MatchId, requestModel.DiscordToken);
        
        var message = await _matchAdminMatchupService.SendMessageAsync(matchInfo, requestModel);
        
        return MessageResponseModel.FromDbModel(message);
    }

    public async Task AdminUpdateObjectives(AdminUpdateObjectivesModel requestModel)
    {
        var matchInfo = await VerifyMatchPermissionAsync(requestModel.MatchId, requestModel.DiscordToken);
        
        await _matchAdminMatchupService.UpdateObjectivesAsync(matchInfo, requestModel);
    }

    public async Task AdminFinalizeMatch(AuthenticatedRequestModel requestModel)
    {
        var matchInfo = await VerifyMatchPermissionAsync(requestModel.MatchId, requestModel.DiscordToken);

        await _matchAdminMatchupService.FinalizeMatchAsync(matchInfo);
    }
    
    #region Competitor Endpoints
    public MessageResponseModel? CompetitorCheckMessages(CompetitorCheckMessagesRequestModel requestModel)
    {
        var message = _matchupService.GetPendingMessageForCompetitor(requestModel.MatchId, requestModel.PublicId);

        return message == null ? null : MessageResponseModel.FromDbModel(message);
    }

    public async Task CompetitorUpdateMessageState(CompetitorUpdateMessageStateRequestModel requestModel)
    {
        var message = _matchupService.UpdateMessageStateForCompetitor(requestModel.MatchId, requestModel.PublicId, requestModel.State);

        if (message.AcknowledgedAt.HasValue)
        {
            await Clients.Group(string.Format(MatchAdminMessageAckGroupFormat, requestModel.MatchId)).MessageAcknowledged(MessageResponseModel.FromDbModel(message));            
        }
    }

    public async Task CompetitorNotifyCompletion(CompetitorNotifyCompletionRequestModel requestModel)
    {
        await _matchupService.MarkPlayerAsCompleteAsync(requestModel.MatchId, requestModel, DateTime.UtcNow);
    }
    #endregion

    public UpdatedObjectivesResponseModel LoadObjectives(MatchIdOnlyRequestModel match)
    {
        return _matchAdminMatchupService.GetObjectives(match.MatchId);
    }

    private async Task<string?> VerifyDiscordTokenAsync(string token)
    {
        var splitDiscordToken = token.Split(' ');
        
        return await _discordService.VerifyTokenAsync(splitDiscordToken[0], splitDiscordToken[1]);
    }
}