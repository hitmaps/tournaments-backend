using BusinessLogic.SignalR.ResponseModels;

namespace BusinessLogic.SignalR;

public interface IMatchHub
{
    public Task MatchUpdate(MatchInfoResponseModel response);
    public Task HeartbeatUpdate(HeartbeatUpdateResponseModel response);
    public Task NewMessage(MessageResponseModel response);
    public Task MessageAcknowledged(MessageResponseModel response);
    public Task ObjectiveUpdate(UpdatedObjectivesResponseModel response);
    public Task GlobalScoreUpdate(GlobalScoreUpdateResponseModel response);
}