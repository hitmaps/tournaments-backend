using BusinessLogic.SignalR.ResponseModels;
using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.SignalR;

public interface IMatchHubService
{
    Task SendUpdatedMatchupAsync(int matchupId, MatchInfoResponseModel response);
    Task SendUpdatedMatchupAsync(int matchupId, MatchInfo matchInfo, List<RestGuildUser>? discordUsers = null);
    Task SendMessageAsync(int matchupId, string competitorPublicId, Message message);
    Task SendUpdatedObjectivesAsync(int matchupId, UpdatedObjectivesResponseModel response);
    Task SendGlobalScoreUpdateAsync(int matchupId, GlobalScoreUpdateResponseModel response);
}