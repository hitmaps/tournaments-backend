using BusinessLogic.Batch.PostMatchCompleteMessage;
using BusinessLogic.Batch.PostMidMatchMatchupMessage;
using BusinessLogic.Batch.SyncMatchInfoWithBracket;
using BusinessLogic.BracketManager;
using BusinessLogic.Discord;
using BusinessLogic.Exceptions;
using BusinessLogic.Models;
using BusinessLogic.SignalR;
using BusinessLogic.SignalR.RequestModels;
using BusinessLogic.SignalR.ResponseModels;
using DataAccess.Data;
using DataAccess.Models;
using Hangfire;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Hitmaps.MatchAdministration;

public class MatchAdminMatchupService : IMatchAdminMatchupService
{
    private readonly TournamentDbContext _dbContext;
    private readonly IMatchHubService _matchHubService;
    private readonly IDiscordService _discordService;
    private readonly IBracketManagerService _bracketManagerService;

    public MatchAdminMatchupService(TournamentDbContext dbContext, IMatchHubService matchHubService, IDiscordService discordService, IBracketManagerService bracketManagerService)
    {
        _dbContext = dbContext;
        _matchHubService = matchHubService;
        _discordService = discordService;
        _bracketManagerService = bracketManagerService;
    }

    public async Task SendSpinAsync(MatchInfo matchInfo, SendSpinRequestModel requestModel)
    {
        var postScoreMessage = false;
        var discordUsersTask = _discordService.GetUsersAsync(matchInfo.Stage.Bracket.Event.DiscordGuildId);
        //-- Clear out old data
        matchInfo.CompetitorInfo.ForEach(x => x.CompleteTime = null);
        matchInfo.TimerPausedRemainingTimeInSeconds = null;
        matchInfo.TimerPaused = false;
        
        //-- Set new data
        var currentMap = matchInfo.MapSelections.First(x => x.Id == requestModel.CurrentMapSelectionId);
        currentMap.MapJson = requestModel.RoundPayload;
        matchInfo.TimerStartsAt = requestModel.StartTime < DateTime.UtcNow ?
            DateTime.UtcNow :
            requestModel.StartTime;
        var postTimeMessage = currentMap.MapStartedAt != matchInfo.TimerStartsAt;
        matchInfo.TimerEndsAt = SetEndTime(matchInfo.TimerStartsAt, requestModel.RoundDuration);
        matchInfo.TimerFingerprint = Guid.NewGuid();
        currentMap.MapStartedAt = matchInfo.TimerStartsAt;

        var objectives = _dbContext.RouletteObjectives.Where(x => x.MatchInfoId == matchInfo.Id).ToList();
        foreach (var objective in objectives)
        {
            _dbContext.Remove(objective);
        }
        
        var lastCompletedMap = matchInfo.MapSelections.Where(x => x.ResultVerifiedAt.HasValue)
            .OrderByDescending(x => x.ResultVerifiedAt!.Value)
            .FirstOrDefault();
        // ReSharper disable once MergeIntoPattern
        if (lastCompletedMap != null && !lastCompletedMap.MapCompleteMessageId.HasValue)
        {
            postScoreMessage = true;
        }
        
        
        _dbContext.Update(currentMap);
        _dbContext.Update(matchInfo);
        await _dbContext.SaveChangesAsync();
        BackgroundJob.Enqueue<PostMidMatchMatchupMessage>(x => x.ProgramLogicAsync(matchInfo.Id, postTimeMessage, postScoreMessage));
        var discordUsers = await discordUsersTask;
        await _matchHubService.SendUpdatedMatchupAsync(matchInfo.Id, new MatchInfoResponseModel
        {
            StageType = matchInfo.Stage.Type,
            MapSelections = matchInfo.MapSelections.Select(MatchInfoMapSelectionViewModel.FromDbModel).ToList(),
            Competitors = matchInfo.CompetitorInfo.Select(x => MatchInfoCompetitorViewModel.FromDbModel(x, discordUsers)).ToList(),
            Timer = TimerViewModel.FromDbModel(matchInfo),
            PointsForVictory = matchInfo.Stage.PointsForVictories.FirstOrDefault(x => x.Round == matchInfo.Round)?.Points
        });
    }

    private static DateTime? SetEndTime(DateTime? startTime, int? roundDuration)
    {
        if (!roundDuration.HasValue || !startTime.HasValue)
        {
            return null;
        }
        
        //-- Add 1 second to account for network latency
        return startTime.Value.AddMinutes(roundDuration.Value).AddSeconds(1);
    }

    public async Task ClearSpinAsync(MatchInfo matchInfo)
    {
        var discordUsersTask = _discordService.GetUsersAsync(matchInfo.Stage.Bracket.Event.DiscordGuildId);
        matchInfo.CompetitorInfo.ForEach(x => x.CompleteTime = null);
        matchInfo.TimerPausedRemainingTimeInSeconds = null;
        matchInfo.TimerPaused = false;
        matchInfo.TimerStartsAt = null;
        matchInfo.TimerEndsAt = null;
        matchInfo.TimerFingerprint = Guid.NewGuid();
        
        var objectives = _dbContext.RouletteObjectives.Where(x => x.MatchInfoId == matchInfo.Id).ToList();
        foreach (var objective in objectives)
        {
            _dbContext.Remove(objective);
        }
        
        var currentMap = GetCurrentMap(matchInfo);
        if (currentMap != null)
        {
            currentMap.MapJson = null;
            currentMap.MapStartedAt = null;
            _dbContext.Update(currentMap);
        }

        _dbContext.Update(matchInfo);
        await _dbContext.SaveChangesAsync();
        var discordUsers = await discordUsersTask;
        await _matchHubService.SendUpdatedMatchupAsync(matchInfo.Id, new MatchInfoResponseModel
        {
            StageType = matchInfo.Stage.Type,
            MapSelections = matchInfo.MapSelections.Select(MatchInfoMapSelectionViewModel.FromDbModel).ToList(),
            Competitors = matchInfo.CompetitorInfo.Select(x => MatchInfoCompetitorViewModel.FromDbModel(x, discordUsers)).ToList(),
            Timer = TimerViewModel.FromDbModel(matchInfo),
            PointsForVictory = matchInfo.Stage.PointsForVictories.FirstOrDefault(x => x.Round == matchInfo.Round)?.Points
        });
    }

    private static MapSelection? GetCurrentMap(MatchInfo matchInfo)
    {
        return matchInfo.MapSelections.FirstOrDefault(x => x.State == MapSelectionState.Ready.Value);
    }

    public async Task ProcessCompletionAsync(MatchInfo matchInfo, ProcessCompletionRequestModel requestModel)
    {
        if (requestModel.VerifyResult == VerifyResult.Reject.Value)
        {
            var competitor = matchInfo.CompetitorInfo.First(x => x.Competitor.Id == requestModel.CompetitorId);
            competitor.CompleteTime = null;
        }
        else
        {
            var mapSelection = matchInfo.MapSelections.First(x => x.Id == requestModel.MapSelectionId);
            mapSelection.State = MapSelectionState.Complete.Value;
            if (requestModel.VerifyResult == VerifyResult.Accept.Value)
            {
                var competitor = matchInfo.CompetitorInfo.First(x => x.Competitor.Id == requestModel.CompetitorId);
                mapSelection.Winner = competitor.Competitor;
                mapSelection.WinnerFinishedAt = competitor.CompleteTime;
                mapSelection.ResultVerifiedAt = DateTime.UtcNow;
            } else if (requestModel.VerifyResult == VerifyResult.Draw.Value)
            {
                var finishTimes = matchInfo.CompetitorInfo.Select(x => x.CompleteTime).ToList();
                var earlierFinishTime = finishTimes.Count == 2 ? finishTimes.Min() : null;
                
                mapSelection.WinnerFinishedAt = earlierFinishTime;
                mapSelection.ResultVerifiedAt = DateTime.UtcNow;
            }
            matchInfo.CompetitorInfo.ForEach(x => x.CompleteTime = null);
        }

        _dbContext.Update(matchInfo);
        await _dbContext.SaveChangesAsync();
        
        var nextMap = matchInfo.MapSelections
            .Where(x => x.State == MapSelectionState.Ready.Value)
            .OrderBy(x => x.Id)
            .FirstOrDefault();
        if (nextMap is { Revealed: false })
        {
            nextMap.Revealed = true;
            _dbContext.Update(nextMap);
            await _dbContext.SaveChangesAsync();
        }

        if (requestModel.VerifyResult == VerifyResult.Accept.Value ||
            requestModel.VerifyResult == VerifyResult.Draw.Value)
        {
            _ = SendGlobalMatchAlertAsync(matchInfo);
            await ClearSpinAsync(matchInfo);
        }
        else
        {
            await _matchHubService.SendUpdatedMatchupAsync(matchInfo.Id, matchInfo);
        }
    }

    private async Task SendGlobalMatchAlertAsync(MatchInfo matchInfo)
    {
        try
        {
            await Task.Run(() =>
                {
                    var completedMaps = matchInfo.MapSelections
                        .Where(x => x.State == MapSelectionState.Complete.Value)
                        .OrderByDescending(x => x.ResultVerifiedAt)
                        .ToList();
                    var mostRecentMap = completedMaps.FirstOrDefault();
                    if (mostRecentMap == null)
                    {
                        //-- We're at 0-0, don't do anything
                        return;
                    }
                    
                    var firstCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 1).Competitor;
                    var secondCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 2).Competitor;
                    var mostRecentMapPointsGained = new
                    {
                        Left = mostRecentMap.Winner == null ? 1 : (mostRecentMap.Winner.Id == firstCompetitor.Id ? 2 : 0),
                        Right = mostRecentMap.Winner == null ? 1 : (mostRecentMap.Winner.Id == secondCompetitor.Id ? 2 : 0)
                    };

                    var firstCompetitorScore = (completedMaps.Count(x => x.Winner?.Id == firstCompetitor.Id) * 2) +
                                               completedMaps.Count(x => x.Winner == null);
                    var secondCompetitorScore = (completedMaps.Count(x => x.Winner?.Id == secondCompetitor.Id) * 2) +
                        completedMaps.Count(x => x.Winner == null);
                    var completionTime = !mostRecentMap.WinnerFinishedAt.HasValue
                        ? null
                        : (int?)(mostRecentMap.WinnerFinishedAt!.Value - mostRecentMap.MapStartedAt!.Value)
                        .TotalSeconds;
                    var responseModels = new GlobalScoreUpdateResponseModel
                    {
                        MatchId = matchInfo.Id,
                        Location = mostRecentMap.Mission.Location,
                        TimeInSeconds = completionTime,
                        Competitors = new List<GlobalScoreUpdateCompetitor>
                        {
                            new()
                            {
                                Name = firstCompetitor.ChallongeName!,
                                NewScore = firstCompetitorScore,
                                OldScore = firstCompetitorScore - mostRecentMapPointsGained.Left
                            },
                            new()
                            {
                                Name = secondCompetitor.ChallongeName!,
                                NewScore = secondCompetitorScore,
                                OldScore = secondCompetitorScore - mostRecentMapPointsGained.Right
                            }
                        }
                    };
                    _ = _matchHubService.SendGlobalScoreUpdateAsync(matchInfo.Id, responseModels);
                }
            );
        }
        catch (Exception)
        {
            //-- ignored
        }
        
    }

    public async Task RemoveLastResultAsync(MatchInfo matchInfo, RemoveLastResultRequestModel requestModel)
    {
        var mostRecentlyCompletedMap = matchInfo.MapSelections.Where(x => x.State == MapSelectionState.Complete.Value)
            .OrderByDescending(x => x.Id)
            .First();

        if (mostRecentlyCompletedMap.Id != requestModel.MapSelectionId)
        {
            throw new ConflictException<int>(mostRecentlyCompletedMap.Id, requestModel.MapSelectionId);
        }
        
        mostRecentlyCompletedMap.State = MapSelectionState.Ready.Value;
        mostRecentlyCompletedMap.Winner = null;
        mostRecentlyCompletedMap.ResultVerifiedAt = null;
        mostRecentlyCompletedMap.WinnerFinishedAt = null;
        mostRecentlyCompletedMap.MapStartedAt = null;
        //-- TODO Metric
        _dbContext.Update(matchInfo);
        await _dbContext.SaveChangesAsync();

        await _matchHubService.SendUpdatedMatchupAsync(requestModel.MatchId, matchInfo);
    }

    public async Task<Message> SendMessageAsync(MatchInfo matchInfo, AdminSendMessageRequestModel requestModel)
    {
        var competitor = matchInfo.CompetitorInfo.First(x => x.Competitor.Id == requestModel.CompetitorId);

        if (competitor.Messages.Any(x => !x.AcknowledgedAt.HasValue))
        {
            throw new InvalidOperationException("Cannot send a new message when one is still not acknowledged!");
        }

        var message = new Message
        {
            CompetitorInfo = competitor,
            Content = requestModel.Message,
            SentAt = DateTime.UtcNow
        };
        _dbContext.Update(message);
        await _dbContext.SaveChangesAsync();

        await _matchHubService.SendMessageAsync(matchInfo.Id, competitor.PublicId, message);

        return message;
    }

    public async Task UpdateObjectivesAsync(MatchInfo matchInfo, AdminUpdateObjectivesModel requestModel)
    {
        var existingObjectives = _dbContext.RouletteObjectives.Where(x => x.MatchInfoId == matchInfo.Id).ToList();
        existingObjectives.ForEach(x => _dbContext.RouletteObjectives.Remove(x));
        
        requestModel.Objectives.ForEach(x =>
        {
            _dbContext.Update(new RouletteObjective
            {
                MatchInfoId = matchInfo.Id,
                CompetitorIndex = x.CompetitorIndex,
                ObjectiveIndex = x.ObjectiveIndex,
                Completed = x.Completed
            });
        });
        await _dbContext.SaveChangesAsync();
        await _matchHubService.SendUpdatedObjectivesAsync(matchInfo.Id, new UpdatedObjectivesResponseModel
        {
            Objectives = requestModel.Objectives
        });
    }

    public UpdatedObjectivesResponseModel GetObjectives(int matchId)
    {
        var response = new UpdatedObjectivesResponseModel();
        var objectives = _dbContext.RouletteObjectives.AsNoTracking()
            .Where(x => x.MatchInfoId == matchId).ToList();
        objectives.ForEach(x => response.Objectives.Add(new Objective
        {
            Completed = x.Completed,
            CompetitorIndex = x.CompetitorIndex,
            ObjectiveIndex = x.ObjectiveIndex
        }));

        return response;
    }

    public async Task FinalizeMatchAsync(MatchInfo matchInfo)
    {
        matchInfo.MatchCompletedAt = DateTime.UtcNow;
        matchInfo.State = State.Complete.Value;
        
        // Reveal all random maps now that the match has ended
        matchInfo.MapSelections.ForEach(x => x.Revealed = true);
        
        _dbContext.Update(matchInfo);
        await _dbContext.SaveChangesAsync();
        BackgroundJob.Enqueue<PostMatchCompleteMessage>(x => x.Run(matchInfo.Id));
        
        // Update bracket
        if (new List<string> { "single-elimination", "double-elimination" }.Contains(matchInfo.Stage.Type))
        {
            await _bracketManagerService.FinalizeBracketMatchAsync(matchInfo);
            BackgroundJob.Enqueue<SyncMatchInfoWithBracket>(x => x.SyncMatchInfo(matchInfo.Stage.Id));
        }
    }
}