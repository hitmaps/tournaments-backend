using BusinessLogic.SignalR.RequestModels;
using BusinessLogic.SignalR.ResponseModels;
using DataAccess.Models;

namespace BusinessLogic.Hitmaps.MatchAdministration;

public interface IMatchAdminMatchupService
{
    Task SendSpinAsync(MatchInfo matchInfo, SendSpinRequestModel requestModel);
    Task ClearSpinAsync(MatchInfo matchInfo);
    Task ProcessCompletionAsync(MatchInfo matchInfo, ProcessCompletionRequestModel requestModel);
    Task RemoveLastResultAsync(MatchInfo matchInfo, RemoveLastResultRequestModel requestModel);
    Task<Message> SendMessageAsync(MatchInfo matchInfo, AdminSendMessageRequestModel requestModel);
    Task UpdateObjectivesAsync(MatchInfo matchInfo, AdminUpdateObjectivesModel requestModel);
    UpdatedObjectivesResponseModel GetObjectives(int matchId);
    Task FinalizeMatchAsync(MatchInfo matchInfo);
}