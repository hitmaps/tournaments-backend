namespace BusinessLogic.Hitmaps.MatchAdministration;

public record VerifyResult(string Value)
{
    public static VerifyResult Reject => new("Reject");
    public static VerifyResult Accept => new("Accept");
    public static VerifyResult Draw => new("Draw");
}