using BusinessLogic.Discord;
using BusinessLogic.Exceptions;
using BusinessLogic.Extensions;
using DataAccess.Data;
using DataAccess.Models;
using Discord.Rest;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Hitmaps;

public class EventService : IEventService
{
    private readonly TournamentDbContext _dbContext;
    private readonly IDiscordService _discordService;

    public EventService(TournamentDbContext dbContext, IDiscordService discordService)
    {
        _dbContext = dbContext;
        _discordService = discordService;
    }

    public List<Event> GetAllEvents()
    {
        return _dbContext.Events
            .OrderByDescending(x => x.EventEndsAt)
            .ToList();
    }

    public Event GetEventForSlug(string slug, EventIncludes includes)
    {
        IQueryable<Event> queryable = _dbContext.Events;

        if (FlagHelpers.IsFlagSet(includes, EventIncludes.SplitQuery))
        {
            queryable = queryable.AsSplitQuery();
        }
        if (FlagHelpers.IsFlagSet(includes, EventIncludes.DetachEntity))
        {
            queryable = queryable.AsNoTracking();
        }
        if (FlagHelpers.IsFlagSet(includes, EventIncludes.DiscordCasterRoleIds))
        {
            queryable = queryable.Include(x => x.DiscordCasterRoleIds);
        }
        if (FlagHelpers.IsFlagSet(includes, EventIncludes.DiscordAdminRoleIds))
        {
            queryable = queryable.Include(x => x.DiscordAdminRoleIds);
        }
        if (FlagHelpers.IsFlagSet(includes, EventIncludes.Brackets))
        {
            queryable = queryable.Include(x => x.Brackets);
        }

        if (FlagHelpers.IsFlagSet(includes, EventIncludes.Stages))
        {
            queryable = queryable.Include(x => x.Brackets).ThenInclude(x => x.Stages);
        }
        // TODO
        /*if (FlagHelpers.IsFlagSet(includes, EventIncludes.BracketPointsForVictories))
        {
            queryable = queryable.Include(x => x.Brackets)
                .ThenInclude(x => x.PointsForVictories);
        }*/
        if (FlagHelpers.IsFlagSet(includes, EventIncludes.MapPool))
        {
            queryable = queryable.Include(x => x.Brackets)
                .ThenInclude(x => x.Maps);
        }
        if (FlagHelpers.IsFlagSet(includes, EventIncludes.Competitors))
        {
            queryable = queryable.Include(x => x.Brackets)
                .ThenInclude(x => x.Competitors);
        }
        
        return queryable.First(x => x.Slug == slug);
    }
    
    public List<Competitor> GetCompetitorsForEventAndSnowflake(string eventSlug, ulong snowflake)
    {
        var theEvent = _dbContext.Events
            .Include(x => x.Brackets)
            .ThenInclude(x => x.Competitors)
            .First(x => x.Slug == eventSlug);

        return theEvent.Brackets
            .SelectMany(x => x.Competitors)
            .Where(x => x.DiscordId == snowflake)
            .ToList();
    }

    public async Task AssertEventManagementPermissionByBracketIdAsync(int bracketId, ulong discordSnowflake)
    {
        var bracket = _dbContext.Brackets
            .Include(x => x.Event)
            .ThenInclude(x => x.DiscordAdminRoleIds)
            .First(x => x.Id == bracketId);
        var discordUser = await _discordService.GetUserForGuildAsync(bracket.Event.DiscordGuildId, discordSnowflake);

        if (!bracket.Event.DiscordAdminRoleIds.Any(x => discordUser!.RoleIds.Contains(ulong.Parse(x.DiscordRoleId))))
        {
            throw new AccessForbiddenException();
        }
    }
    
    public async Task AssertEventManagementPermissionByStageIdAsync(int stageId, ulong discordSnowflake)
    {
        var stage = _dbContext.Stages
            .Include(x => x.Bracket)
            .ThenInclude(x => x.Event)
            .ThenInclude(x => x.DiscordAdminRoleIds)
            .First(x => x.Id == stageId);
        var discordUser = await _discordService.GetUserForGuildAsync(stage.Bracket.Event.DiscordGuildId, discordSnowflake);

        if (!stage.Bracket.Event.DiscordAdminRoleIds.Any(x => discordUser!.RoleIds.Contains(ulong.Parse(x.DiscordRoleId))))
        {
            throw new AccessForbiddenException();
        }
    }

    public async Task AssertEventManagementPermissionBySlugAsync(string eventSlug, ulong discordSnowflake)
    {
        var theEvent = _dbContext.Events
            .Include(x => x.DiscordAdminRoleIds)
            .First(x => x.Slug == eventSlug);
        var discordUser = await _discordService.GetUserForGuildAsync(theEvent.DiscordGuildId, discordSnowflake);
        
        if (!theEvent.DiscordAdminRoleIds.Any(x => discordUser!.RoleIds.Contains(ulong.Parse(x.DiscordRoleId))))
        {
            throw new AccessForbiddenException();
        }
    }

    public async Task<Dictionary<RestGuildUser, string?>> GetShoutcastersForEventAsync(string slug)
    {
        var theEvent = _dbContext.Events
            .Include(x => x.DiscordCasterRoleIds)
            .First(x => x.Slug == slug);
        var discordUsers = await _discordService.GetUsersWithRoleAsync(theEvent.DiscordGuildId,
            theEvent.DiscordCasterRoleIds.Select(x => x.DiscordRoleId).ToList());
        //@formatter:off
        var casters = _dbContext.Casters
            .FromSqlRaw("SELECT c.* " +
                        "FROM Caster as c " +
                        "INNER JOIN ( " +
                            "SELECT DiscordId, MAX(Id) AS MaxId " +
                            "FROM Caster " +
                            $"WHERE Type = 1 " +
                            "GROUP BY DiscordId " +
                        ") b ON c.Id = b.MaxId " +
                        "ORDER BY c.DiscordId").ToList();
        //@formatter:on

        var dictionary = new Dictionary<RestGuildUser, string?>();
        discordUsers.ForEach(discordUser => dictionary[discordUser] = casters.FirstOrDefault(x => x.DiscordId == discordUser.Id)?.StreamUrl);

        return dictionary;
    }
}