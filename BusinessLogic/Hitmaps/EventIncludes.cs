namespace BusinessLogic.Hitmaps;

[Flags]
public enum EventIncludes
{
    None = -1,
    DiscordCasterRoleIds = 1,
    DiscordAdminRoleIds = 2,
    Brackets = 4,
    BracketPointsForVictories = 8,
    MapPool = 16,
    Competitors = 32,
    DetachEntity = 64,
    SplitQuery = 128,
    Stages = 256,
    All = DiscordCasterRoleIds | DiscordAdminRoleIds | Brackets | BracketPointsForVictories | MapPool | Competitors | DetachEntity
}