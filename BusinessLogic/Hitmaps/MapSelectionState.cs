namespace BusinessLogic.Hitmaps;

public record MapSelectionState(string Value)
{
    public static MapSelectionState Ban => new("Ban");
    public static MapSelectionState Ready => new("Ready");
    public static MapSelectionState Complete => new("Complete");
    public static MapSelectionState Skipped => new("Skipped");
}