using BusinessLogic.Models;
using BusinessLogic.SignalR.RequestModels;
using BusinessLogic.SignalR.ResponseModels;
using DataAccess.Models;

namespace BusinessLogic.Hitmaps;

public interface IMatchupService
{
    Task<MatchInfo> ScheduleMatchAsync(int stageId, long matchId, ScheduleMatchRequestModel requestModel);
    void SetCasters(int stageId, long matchId, SetCastersRequestModel requestModel);

    List<MatchInfo> GetUpcomingMatches(string eventSlug);

    void SetGameModeId(int matchId, Guid gameModeId);

    MatchInfo GetMatchupForMetadata(int matchId);
    MatchInfo? GetMatchupForHeartbeat(int matchId, string publicId);
    Task<MatchInfo> GetMatchupForMetadataAsync(int matchId);
    Task<MatchInfo> GetMatchupForMatchHubAsync(int matchId);

    Task VerifyMatchAuthorityAsync(int matchId, ulong userSnowflake);

    Task SendScoreToChallongeAsync(int matchId, PostScoreRequestModel requestModel, ulong userSnowflake);
    void SetMatchAdmin(int stageId, long matchId, SetMatchAdminRequestModel requestModel);

    Task SetMatchState(int stageId, long matchId, MatchStateModel matchState);
    Task ResetMatchAsync(int stageId, long matchId);
    Message? GetPendingMessageForCompetitor(int matchId, string competitorPublicId);
    Message UpdateMessageStateForCompetitor(int matchId, string competitorPublicId, string state);
    Task MarkPlayerAsCompleteAsync(int matchId, CompetitorNotifyCompletionRequestModel requestModel, DateTime completionTime);
}