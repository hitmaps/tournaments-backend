using BusinessLogic.Models.MatchHistory;

namespace BusinessLogic.Hitmaps;

public interface IMatchHistoryService
{
    Task<List<MatchHistoryViewModel>> GetMatchHistoryAsync(string eventSlug, ulong discordId);
}