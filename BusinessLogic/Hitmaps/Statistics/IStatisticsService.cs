using DataAccess.Models;

namespace BusinessLogic.Hitmaps.Statistics;

public interface IStatisticsService
{
    public List<MapPickBanCounts> GetMapCountsByDate(string eventSlug, MissionType missionType);
    public List<MapWithCount> GetMapCounts(string eventSlug, MissionType missionType);
    public Task<List<ShoutcastStatistic>> GetShoutcasterStatisticsAsync(string eventSlug);
    public MatchHistoryStatistic GetMatchHistoryForEvent(string eventSlug);
    public Task<List<MatchAdminStatistic>> GetMatchAdminStatisticsAsync(string eventSlug);
}