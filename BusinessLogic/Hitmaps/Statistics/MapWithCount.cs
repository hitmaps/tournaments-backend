namespace BusinessLogic.Hitmaps.Statistics;

public class MapWithCount
{
    public string MapName { get; set; } = null!;
    public int Count { get; set; }
}