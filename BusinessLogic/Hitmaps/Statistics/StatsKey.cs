namespace BusinessLogic.Hitmaps.Statistics;

public class StatsKey
{
    public const string MapPicks = "MapPicks";
    public const string MapBans = "MapBans";
    public const string RandomMaps = "RandomMaps";
    public const string Shoutcasters = "Shoutcasters";
    public const string MatchHistory = "MatchHistory";
    public const string MatchAdmins = "MatchAdmins";
}