using BusinessLogic.Discord;
using BusinessLogic.Extensions;
using BusinessLogic.Models;
using DataAccess.Data;
using DataAccess.Models;
using Discord.Rest;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Hitmaps.Statistics;

public class StatisticsService : IStatisticsService
{
    private TournamentDbContext dbContext;
    private IDiscordService discordService;

    public StatisticsService(TournamentDbContext dbContext, IDiscordService discordService)
    {
        this.dbContext = dbContext;
        this.discordService = discordService;
    }

    public List<MapPickBanCounts> GetMapCountsByDate(string eventSlug, MissionType missionType)
    {
        //@formatter:off
        var completedMatches = dbContext.Matchups
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
                    .ThenInclude(x => x.Event)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Competitor)
            .Where(x => x.MatchCompletedAt.HasValue && x.Stage.Bracket.Event.Slug == eventSlug)
            .OrderBy(x => x.MatchScheduledAt)
            .ToList();
        var potentialMaps = dbContext.Brackets
            .Include(x => x.Event)
            .Include(x => x.Maps)
            .Where(x => x.Event.Slug == eventSlug)
            .ToList()
            .SelectMany(x => x.Maps)
            .Where(x => x.Type.HasFlag(missionType))
            .DistinctBy(x => x.HitmapsSlug)
            .ToList();
        //@formatter:on

        var dateToMapCount = new List<MapPickBanCounts>();
        foreach (var match in completedMatches)
        {
            var formattedDate = match.MatchScheduledAt!.Value.ToApiDateFormat();
            if (dateToMapCount.All(x => x.Date != formattedDate))
            {
                BuildEmptyMapCount(formattedDate, dateToMapCount, potentialMaps);
            }

            foreach (var mapSelection in match.MapSelections)
            {
                if (mapSelection.MissionType != missionType || mapSelection.Competitor == null)
                {
                    continue;
                }

                dateToMapCount.First(x => x.Date == formattedDate)
                    .MapCount.First(x => x.MapName == mapSelection.Mission.Name)
                    .Count++;
            }
        }

        return dateToMapCount;
    }

    private static void BuildEmptyMapCount(string formattedDate, List<MapPickBanCounts> dateToMapCount, List<MissionForPool> potentialMaps)
    {
        var mapPickBanCounts = new MapPickBanCounts
        {
            Date = formattedDate,
            MapCount = new List<MapWithCount>()
        };
        dateToMapCount.Add(mapPickBanCounts);
        potentialMaps.ForEach(x => mapPickBanCounts.MapCount.Add(new MapWithCount
        {
            MapName = x.Name,
            Count = 0
        }));
    }

    public List<MapWithCount> GetMapCounts(string eventSlug, MissionType missionType)
    {
        //@formatter:off
        var completedMatches = dbContext.Matchups
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
                    .ThenInclude(x => x.Event)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Competitor)
            .Where(x => x.MatchCompletedAt.HasValue && x.Stage.Bracket.Event.Slug == eventSlug)
            .ToList();
        var potentialMaps = dbContext.Brackets
            .Include(x => x.Event)
            .Include(x => x.Maps)
            .Where(x => x.Event.Slug == eventSlug)
            .ToList()
            .SelectMany(x => x.Maps)
            .Where(x => x.Type.HasFlag(missionType))
            .DistinctBy(x => x.HitmapsSlug)
            .OrderBy(x => x.Id)
            .ToList();
        //@formatter:on

        var maps = new List<MapWithCount>();
        potentialMaps.ForEach(map => maps.Add(new MapWithCount
        {
            MapName = map.Name,
            Count = 0
        }));

        foreach (var match in completedMatches)
        {
            match.MapSelections.Where(x => MissionType.Random.HasFlag(x.MissionType))
                .ToList()
                .ForEach(x => maps.First(y => y.MapName == x.Mission.Name).Count++);
        }

        return maps;
    }

    public async Task<List<ShoutcastStatistic>> GetShoutcasterStatisticsAsync(string eventSlug)
    {
        //@formatter:off
        var completedMatches = dbContext.Matchups
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
                    .ThenInclude(x => x.Event)
            .Include(x => x.Casters)
            .Where(x => x.MatchCompletedAt.HasValue && x.Stage.Bracket.Event.Slug == eventSlug)
            .OrderBy(x => x.MatchScheduledAt)
            .ToList();
        //@formatter:on

        var statistics = new List<ShoutcastStatistic>();
        foreach (var match in completedMatches)
        {
            foreach (var caster in match.Casters)
            {
                var casterIdAsString = caster.DiscordId.ToString();
                if (statistics.All(x => x.Name != casterIdAsString))
                {
                    statistics.Add(new ShoutcastStatistic
                    {
                        Name = casterIdAsString,
                        CocastCount = 0,
                        MainCastCount = 0
                    });
                }

                if (caster.Type == CasterType.Main)
                {
                    statistics.First(x => x.Name == casterIdAsString).MainCastCount++;
                }
                else
                {
                    statistics.First(x => x.Name == casterIdAsString).CocastCount++;
                }
            }
        }
        
        // Get actual names for users
        var users = await discordService.GetUsersAsync(dbContext.Events.First(x => x.Slug == eventSlug).DiscordGuildId);
        statistics.ForEach(x => x.Name = GetNameById(users, x.Name));

        return statistics
            .OrderByDescending(x => x.MainCastCount)
            .ThenByDescending(x => x.CocastCount)
            .ToList();
    }

    private static string GetNameById(IEnumerable<RestGuildUser> users, string discordId)
    {
        var user = users.FirstOrDefault(x => x.Id.ToString() == discordId);

        return user == null ? "[REDACTED]" : user.Username;
    }

    public MatchHistoryStatistic GetMatchHistoryForEvent(string eventSlug)
    {
        //@formatter:off
        var completedMatches =
            dbContext.Matchups
                .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
                    .ThenInclude(x => x.Competitor)
                .Include("MapSelections.Competitor")
                .Include("MapSelections.Mission")
                .Include(x => x.Stage)
                    .ThenInclude(x => x.Bracket)
                        .ThenInclude(x => x.Event)
                .Where(x => x.MatchCompletedAt.HasValue && x.Stage.Bracket.Event.Slug == eventSlug)
                .OrderBy(x => x.MatchScheduledAt)
                .ToList();
        //@formatter:on

        var model = new MatchHistoryStatistic();
        foreach (var completedMatch in completedMatches)
        {
            model.Matches.Add(CompletedMatchViewModel.FromDbModel(completedMatch));
        }

        return model;
    }

    public async Task<List<MatchAdminStatistic>> GetMatchAdminStatisticsAsync(string eventSlug)
    {
        //@formatter:off
        var completedMatches = dbContext.Matchups
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
                    .ThenInclude(x => x.Event)
            .Where(x => x.MatchCompletedAt.HasValue && 
                        x.Stage.Bracket.Event.Slug == eventSlug && 
                        x.MatchAdminDiscordId.HasValue)
            .OrderBy(x => x.MatchScheduledAt)
            .ToList();
        //@formatter:on

        var matchAdmins = new Dictionary<string, MatchAdminStatistic>();
        foreach (var match in completedMatches)
        {
            var matchAdminIdAsString = match.MatchAdminDiscordId!.Value.ToString();
            if (!matchAdmins.ContainsKey(matchAdminIdAsString))
            {
                matchAdmins[matchAdminIdAsString] = new MatchAdminStatistic
                {
                    Name = matchAdminIdAsString,
                    MatchCount = 0
                };
            }

            matchAdmins[matchAdminIdAsString].MatchCount++;
        }
        
        // Get actual names for users
        var users = await discordService.GetUsersAsync(dbContext.Events.First(x => x.Slug == eventSlug).DiscordGuildId);
        var matchAdminValues = matchAdmins.Values.ToList();
        matchAdminValues.ForEach(x => x.Name = GetNameById(users, x.Name));

        return matchAdminValues
            .OrderByDescending(x => x.MatchCount)
            .ToList();
    }
}