namespace BusinessLogic.Hitmaps.Statistics;

public class ShoutcastStatistic
{
    public string Name { get; set; } = null!;
    public int MainCastCount { get; set; }
    public int CocastCount { get; set; }
}