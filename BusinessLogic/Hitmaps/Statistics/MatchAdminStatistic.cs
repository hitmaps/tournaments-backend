namespace BusinessLogic.Hitmaps.Statistics;

public class MatchAdminStatistic
{
    public string Name { get; set; } = null!;
    public int MatchCount { get; set; }
}