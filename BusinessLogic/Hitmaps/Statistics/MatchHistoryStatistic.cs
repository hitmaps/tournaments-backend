using BusinessLogic.Models;

namespace BusinessLogic.Hitmaps.Statistics;

public class MatchHistoryStatistic
{
    public List<CompletedMatchViewModel> Matches { get; set; } = new();
}