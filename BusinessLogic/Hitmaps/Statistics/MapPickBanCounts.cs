namespace BusinessLogic.Hitmaps.Statistics;

public class MapPickBanCounts
{
    public string Date { get; set; } = null!;
    public List<MapWithCount> MapCount { get; set; } = new();
}