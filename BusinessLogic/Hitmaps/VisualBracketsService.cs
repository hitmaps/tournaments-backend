using System.Diagnostics;
using System.Net;
using DataAccess.Data;
using Microsoft.Extensions.Configuration;

namespace BusinessLogic.Hitmaps;

public class VisualBracketsService : IVisualBracketsService
{
    private readonly TournamentDbContext dbContext;
    private readonly IConfiguration configuration;
    private readonly HttpClientWrapper httpClientWrapper;

    public VisualBracketsService(TournamentDbContext dbContext, IConfiguration configuration, HttpClientWrapper httpClientWrapper)
    {
        this.dbContext = dbContext;
        this.configuration = configuration;
        this.httpClientWrapper = httpClientWrapper;
    }

    public string GetVisualBracket(int bracketId)
    {
        var bracket = dbContext.Brackets.FirstOrDefault(x => x.Id == bracketId);
        if (bracket == null)
        {
            return "Tournament not found.";
        }

        using var request = new HttpRequestMessage(HttpMethod.Get,
            $"https://www.challonge.com/{bracket.ChallongeName}.svg");
        var response = httpClientWrapper.GetClient().Send(request);

        return response.StatusCode != HttpStatusCode.OK ? 
            "Challonge failed to return the bracket" : 
            response.Content.ReadAsStringAsync().Result;
    }
}