namespace BusinessLogic.Hitmaps.Swiss;

public class SwissStageSettings : StageSettings
{
    public List<Round> Rounds { get; set; } = new();
    public int NumberOfAdvancements { get; set; }
}

public class Round
{
    public int Number { get; set; }
    public List<int> Competitors { get; set; } = new();
    public bool Locked { get; set; }
}