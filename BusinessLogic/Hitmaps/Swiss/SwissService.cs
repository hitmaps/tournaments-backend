using System.Net.Http.Json;
using System.Security.Cryptography;
using System.Text.Json;
using BusinessLogic.Extensions;
using BusinessLogic.Hitmaps.Swiss.Generator;
using BusinessLogic.Hitmaps.Swiss.Standings;
using BusinessLogic.Models;
using BusinessLogic.Models.Stage;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BusinessLogic.Hitmaps.Swiss;

public class SwissService : ISwissService
{
    private readonly IEventService eventService;
    private readonly TournamentDbContext context;
    private readonly HttpClientWrapper httpClientWrapper;
    private readonly string swissPairerUrl;
    private readonly string swissPairerAccessKey;

    public SwissService(IEventService eventService, TournamentDbContext context, HttpClientWrapper httpClientWrapper,
        IConfiguration configuration)
    {
        this.eventService = eventService;
        this.context = context;
        this.httpClientWrapper = httpClientWrapper;
        swissPairerUrl = configuration.GetValue<string>("Swiss:PairerUrl")!;
        swissPairerAccessKey = configuration.GetValue<string>("Swiss:AccessKey")!;
    }

    public async Task UpdatePlayerParticipationAsync(int stageId,
        UpdateSwissPlayerParticipationRequestModel requestModel,
        ulong discordSnowflake)
    {
        await eventService.AssertEventManagementPermissionByStageIdAsync(stageId, discordSnowflake);

        var stage = context.Stages.First(x => x.Id == stageId);
        var settings = stage.SettingsJson.DeserializeFromJsonCaseInsensitive<SwissStageSettings>()!;
        var competitorsForRound = settings.Rounds.First(x => x.Number == requestModel.Round).Competitors;

        if (requestModel.Participating && competitorsForRound.All(x => x != requestModel.CompetitorId))
        {
            competitorsForRound.Add(requestModel.CompetitorId);
        }
        else
        {
            competitorsForRound.Remove(requestModel.CompetitorId);
        }

        stage.SettingsJson = settings.SerializeToJsonCamelCase();
        context.Update(stage);
        await context.SaveChangesAsync();
    }

    public async Task StartSwissRoundAsync(int stageId, StartSwissRoundRequestModel requestModel,
        ulong discordSnowflake)
    {
        await eventService.AssertEventManagementPermissionByStageIdAsync(stageId, discordSnowflake);

        var stage = context.Stages.First(x => x.Id == stageId);
        var settings = stage.SettingsJson.DeserializeFromJsonCaseInsensitive<SwissStageSettings>()!;
        var competitorIdsForRound = settings.Rounds.First(x => x.Number == requestModel.Round).Competitors;
        var competitorsForStage = context.Competitors.Where(x => competitorIdsForRound.Contains(x.Id)).ToList();
        //@formatter:off
        var completedMatches = context.Matchups
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Winner)
            .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Competitor)
            .Where(x => x.Stage.Id == stageId && x.State == State.Complete.Value)
            .ToList();
        //@formatter:on

        var competitorsForPairingService = new List<SwissRoundGeneratorApiModel>();
        var byeCompetitors = new HashSet<string>();
        foreach (var competitorId in competitorIdsForRound)
        {
            //-- Calculate their Swiss score (win = 1 pt, tie = .5 pt, loss = 0 pt)
            var completedMatchesForCompetitor = completedMatches
                .Where(match =>
                    match.CompetitorInfo.Any(competitorInfo => competitorInfo.Competitor.Id == competitorId))
                .ToList();
            var pairingServiceModel = new SwissRoundGeneratorApiModel
            {
                Id = competitorId,
                Name = competitorsForStage.First(x => x.Id == competitorId).ChallongeName!
            };
            foreach (var completedMatch in completedMatchesForCompetitor)
            {
                // 1. Check for BYE if competitors for this round is odd (1 competitor record)
                if (completedMatch.CompetitorInfo.Count == 1)
                {
                    byeCompetitors.Add(competitorId.ToString());
                    pairingServiceModel.Points += 1;

                    // If number of competitors for this round is odd, include the BYE opponent to prevent re-matches
                    if (competitorIdsForRound.Count % 2 != 0)
                    {
                        pairingServiceModel.Opponents.Add("BYE");
                    }

                    continue;
                }

                var competitor = completedMatch.CompetitorInfo.First(x => x.Competitor.Id == competitorId);
                var opponent = completedMatch.CompetitorInfo.First(x => x.Competitor.Id != competitorId);
                pairingServiceModel.Opponents.Add(opponent.Competitor.ChallongeName!);

                // 2. Check if loss by forfeit (no points)
                if (competitor.Forfeit)
                {
                    continue;
                }

                // 3. Check if won by forfeit
                if (opponent.Forfeit)
                {
                    pairingServiceModel.Points += 1;
                    continue;
                }

                var competitorWins = completedMatch.MapSelections
                    .Count(x => x.Winner?.Id == competitorId && x.State == State.Complete.Value);
                var ties = completedMatch.MapSelections
                    .Count(x => x.Winner == null && x.State == State.Complete.Value);
                var opponentWins = completedMatch.MapSelections
                    .Count(x => x.Winner?.Id == opponent.Competitor.Id && x.State == State.Complete.Value);
                var competitorPoints = (competitorWins * 2) + ties;
                var opponentPoints = (opponentWins * 2) + ties;

                // 4. Check if natural win or tie (as loss is +0)
                if (competitorPoints > opponentPoints)
                {
                    pairingServiceModel.Points += 1;
                }
                else if (competitorPoints == opponentPoints)
                {
                    pairingServiceModel.Points += 0.5M;
                }
            }

            competitorsForPairingService.Add(pairingServiceModel);
        }

        // Add BYE participant if needed
        if (competitorsForPairingService.Count % 2 != 0)
        {
            competitorsForPairingService.Add(new SwissRoundGeneratorApiModel
            {
                Id = -1,
                Name = "BYE",
                Points = 0,
                Opponents = byeCompetitors
            });
        }

        var pairings = await GeneratePairingsAsync(competitorsForPairingService);

        // Store pairings for round
        foreach (var pairing in pairings.Pairings)
        {
            var matchup = new MatchInfo
            {
                Stage = stage,
                GroupStage = false,
                Round = requestModel.Round,
                State = MatchInfo.StateNeedsScheduling
            };
            if (pairing.PlayerOne.Name == "BYE" || pairing.PlayerTwo.Name == "BYE")
            {
                var nonByePlayer = pairing.PlayerOne.Name == "BYE" ? pairing.PlayerTwo : pairing.PlayerOne;
                matchup.CompetitorInfo.Add(new MatchCompetitorInfo
                {
                    Competitor = competitorsForStage.First(x => x.Id == nonByePlayer.Id),
                    Order = 1,
                    Forfeit = false,
                    PublicId = GenerateRandomHexString()
                });
            }
            else
            {
                matchup.CompetitorInfo.Add(new MatchCompetitorInfo
                {
                    Competitor = competitorsForStage.First(x => x.Id == pairing.PlayerOne.Id),
                    Order = 1,
                    Forfeit = false,
                    PublicId = GenerateRandomHexString()
                });
                matchup.CompetitorInfo.Add(new MatchCompetitorInfo
                {
                    Competitor = competitorsForStage.First(x => x.Id == pairing.PlayerTwo.Id),
                    Order = 2,
                    Forfeit = false,
                    PublicId = GenerateRandomHexString()
                });
            }

            context.Update(matchup);
        }

        settings.Rounds.First(x => x.Number == requestModel.Round).Locked = true;
        stage.SettingsJson = settings.SerializeToJsonCamelCase();
        context.Update(stage);
        await context.SaveChangesAsync();
    }

    private async Task<SwissRoundGeneratorApiResponseModel> GeneratePairingsAsync(
        List<SwissRoundGeneratorApiModel> competitorModels)
    {
        using var request = new HttpRequestMessage(HttpMethod.Post,
            $"{swissPairerUrl}/api/matches?access-key={swissPairerAccessKey}");
        request.Content = JsonContent.Create(competitorModels);

        var response = await httpClientWrapper.GetClient().SendAsync(request);
        response.EnsureSuccessStatusCode();

        return (await response.Content.ReadFromJsonAsync<SwissRoundGeneratorApiResponseModel>())!;
    }

    private static string GenerateRandomHexString()
    {
        var value = RandomNumberGenerator.GetBytes(8);

        return string.Join("", value.Select(x => x.ToString("x2")));
    }
}