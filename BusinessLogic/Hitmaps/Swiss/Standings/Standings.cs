namespace BusinessLogic.Hitmaps.Swiss.Standings;

public class Standings
{
    public List<Participant> Participants { get; set; } = new();
}

public class Participant
{
    public int CompetitorId { get; set; }
    public string Name { get; set; } = null!;
    public decimal Points { get; set; }
    public decimal TheoreticalMaxPoints { get; set; }
    public decimal Wins { get; set; }
    public int MatchesCompleted { get; set; }
    public decimal BuchholzScore { get; set; }
    public decimal PointsDifferential { get; set; }
    public decimal SonnebornBergerScore { get; set; }
    public bool Advanced { get; set; }
    public bool Eliminated { get; set; }
    public List<int> PlayedOpponents { get; set; } = new();
}