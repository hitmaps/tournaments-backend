using BusinessLogic.Extensions;
using BusinessLogic.Models;
using DataAccess.Data;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Hitmaps.Swiss.Standings;

public class SwissStandingsBuilder
{
    private readonly TournamentDbContext context;

    public SwissStandingsBuilder(TournamentDbContext context)
    {
        this.context = context;
    }

    public Standings GetStandings(int stageId)
    {
        var stage = context.Stages
            .Include(x => x.Bracket)
            .First(x => x.Id == stageId);
        var stageSettings = stage.SettingsJson.DeserializeFromJsonCaseInsensitive<SwissStageSettings>()!;
        var competitorsForStage = context.Competitors
            .Where(x => x.Bracket == stage.Bracket)
            .ToList();
        //@formatter:off
        var completedMatches = context.Matchups
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Winner)
            .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Competitor)
            .Where(x => x.Stage.Id == stageId && x.State == State.Complete.Value)
            .ToList();
        //@formatter:on

        var standings = new Standings();
        foreach (var competitor in competitorsForStage)
        {
            //-- Calculate their Swiss score (win = 1 pt, tie = .5 pt, loss = 0 pt)
            var completedMatchesForCompetitor = completedMatches
                .Where(match =>
                    match.CompetitorInfo.Any(competitorInfo => competitorInfo.Competitor.Id == competitor.Id))
                .ToList();
            var standingsParticipant = new Participant
            {
                CompetitorId = competitor.Id,
                Name = competitor.ChallongeName!
            };
            foreach (var completedMatch in completedMatchesForCompetitor)
            {
                // 1. Check for BYE if competitors for this round is odd (1 competitor record)
                if (completedMatch.CompetitorInfo.Count == 1)
                {
                    standingsParticipant.MatchesCompleted++;
                    standingsParticipant.Points += 1;
                    continue;
                }

                var competitorInfo = completedMatch.CompetitorInfo.First(x => x.Competitor.Id == competitor.Id);
                var opponent = completedMatch.CompetitorInfo.First(x => x.Competitor.Id != competitor.Id);

                // 2. Check if loss by forfeit (no points)
                if (competitorInfo.Forfeit)
                {
                    standingsParticipant.MatchesCompleted++;
                    continue;
                }

                // 3. Check if won by forfeit
                if (opponent.Forfeit)
                {
                    standingsParticipant.Points += 1;
                    standingsParticipant.MatchesCompleted++;
                    continue;
                }
                standingsParticipant.PlayedOpponents.Add(opponent.Competitor.Id);

                var competitorWins = completedMatch.MapSelections
                    .Count(x => x.Winner?.Id == competitor.Id && x.State == State.Complete.Value);
                var ties = completedMatch.MapSelections
                    .Count(x => x.Winner == null && x.State == State.Complete.Value);
                var opponentWins = completedMatch.MapSelections
                    .Count(x => x.Winner?.Id == opponent.Competitor.Id && x.State == State.Complete.Value);
                var competitorPoints = (competitorWins * 2) + ties;
                var opponentPoints = (opponentWins * 2) + ties;
                standingsParticipant.PointsDifferential += competitorPoints - opponentPoints;

                // 4. Check if natural win or tie (as loss is +0)
                standingsParticipant.MatchesCompleted++;
                if (competitorPoints > opponentPoints)
                {
                    standingsParticipant.Wins += 1;
                    standingsParticipant.Points += 1;
                }
                else if (competitorPoints == opponentPoints)
                {
                    standingsParticipant.Wins += 0.5M;
                    standingsParticipant.Points += 0.5M;
                }
            }

            standings.Participants.Add(standingsParticipant);
        }
        
        // Fix some MatchesCompleted for players who skipped a round
        var completedRounds = stageSettings.Rounds
            .Count(round => round.Locked && 
                            context.Matchups
                                .Where(x => x.Stage.Id == stageId && x.Round == round.Number)
                                .All(x => x.State == State.Complete.Value));
        standings.Participants.ForEach(x =>
        {
            x.MatchesCompleted = Math.Max(completedRounds, x.MatchesCompleted);
            x.TheoreticalMaxPoints = x.Points +
                                     (stageSettings.Rounds.Count -
                                      x.MatchesCompleted);
        });

        // Buchholz / Points Differential average
        standings.Participants.ForEach(participant =>
        {
            var opponentPointsTotal = participant.PlayedOpponents
                .Select(x => standings.Participants.First(y => y.CompetitorId == x).Points)
                .Sum();
            if (participant.PlayedOpponents.Count == 0)
            {
                return;
            }
            participant.BuchholzScore = opponentPointsTotal / participant.PlayedOpponents.Count;
            participant.PointsDifferential /= participant.PlayedOpponents.Count;
        });

        // Sort results
        standings.Participants.Sort((a, b) =>
        {
            // 1. Raw points
            if (a.Points != b.Points)
            {
                return a.Points.CompareTo(b.Points) * -1;
            }
            
            // 2. Buchholz
            if (a.BuchholzScore != b.BuchholzScore)
            {
                return a.BuchholzScore.CompareTo(b.BuchholzScore) * -1;
            }
            
            // 3. Point Differential
            if (a.PointsDifferential != b.PointsDifferential)
            {
                return a.PointsDifferential.CompareTo(b.PointsDifferential) * -1;
            }
            
            // 4. Direct comparison
            var relevantMatch = completedMatches
                .Where(x => x.CompetitorInfo.Any(ci => ci.Competitor.Id == a.CompetitorId))
                .FirstOrDefault(x => x.CompetitorInfo.Any(ci => ci.Competitor.Id == b.CompetitorId));
            
            // ReSharper disable once InvertIf
            if (relevantMatch != null)
            {
                var aWins = relevantMatch.MapSelections.Count(x => x.State == State.Complete.Value && x.Winner?.Id == a.CompetitorId);
                var bWins = relevantMatch.MapSelections.Count(x => x.State == State.Complete.Value && x.Winner?.Id == b.CompetitorId);
                var ties = relevantMatch.MapSelections.Count(x => x.Winner == null && x.State == State.Complete.Value);

                var aScore = (aWins * 2) + ties;
                var bScore = (bWins * 2) + ties;

                if (aScore != bScore)
                {
                    return aScore.CompareTo(bScore) * -1;
                }
            }
            
            // 5. Sonneborn-Berger
            // TODO
            return string.Compare(a.Name, b.Name, StringComparison.Ordinal); // Sort them alphabetically for now
        });

        // Track advancements / eliminations
        var numberOfAdvancements = stageSettings.NumberOfAdvancements;
        var allMatchesComplete = completedRounds == stageSettings.Rounds.Count;
        var index = 0;
        foreach (var participant in standings.Participants)
        {
            var sortedTheoreticals =
                standings.Participants.Select(x => x.TheoreticalMaxPoints).OrderBy(x => x).ToList();
            sortedTheoreticals.Remove(participant.TheoreticalMaxPoints);
            sortedTheoreticals.Add(participant.Points);
            sortedTheoreticals = sortedTheoreticals.OrderBy(x => x).ToList();
            if (allMatchesComplete)
            {
                if (index < numberOfAdvancements)
                {
                    participant.Advanced = true;
                }
                else if (index > numberOfAdvancements - 1)
                {
                    // Anyone lower than guaranteed spots are eliminated if they're worse than the lowest possible's points
                    participant.Eliminated =
                        participant.Points < standings.Participants[numberOfAdvancements - 1].Points ||
                        (participant.Points == standings.Participants[numberOfAdvancements - 1].Points && 
                         participant.BuchholzScore < standings.Participants[numberOfAdvancements - 1].BuchholzScore) ||
                        (participant.Points == standings.Participants[numberOfAdvancements - 1].Points && 
                         participant.BuchholzScore == standings.Participants[numberOfAdvancements - 1].BuchholzScore &&
                         participant.PointsDifferential < standings.Participants[numberOfAdvancements - 1].PointsDifferential);
                }
                else
                {
                    if (standings.Participants.Count == numberOfAdvancements)
                    {
                        // Not enough participants to eliminate anyone
                        participant.Advanced = true;
                    }
                    else
                    {
                        // Last slot - Advanced if better than the next participant in line
                        participant.Advanced = participant.Points > standings.Participants[numberOfAdvancements].Points;
                    }
                }
            }
            else if (participant.TheoreticalMaxPoints < standings.Participants[numberOfAdvancements - 1].Points)
            {
                // Participant cannot possibly reach 4th's current point count
                participant.Eliminated = true;
            }
            else if (sortedTheoreticals.IndexOf(participant.Points) >=
                     sortedTheoreticals.Count - numberOfAdvancements)
            {
                // If the participant's current point count is in 4th position or better, they're advancing
                // indexOf will return the lowest value, so it's safe to assume 4th == advancing.
                participant.Advanced = true;
            }

            index++;
        }

        return standings;
    }
}