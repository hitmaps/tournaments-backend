namespace BusinessLogic.Hitmaps.Swiss.Generator;

public class SwissRoundGeneratorApiResponseModel
{
    public List<Pairing> Pairings { get; set; } = new();
}

public class Pairing
{
    public Player PlayerOne { get; set; } = null!;
    public Player PlayerTwo { get; set; } = null!;
}

public class Player
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public decimal Score { get; set; }
}