namespace BusinessLogic.Hitmaps.Swiss.Generator;

public class SwissRoundGeneratorApiModel
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public decimal Points { get; set; } = 0;
    public HashSet<string> Opponents { get; set; } = new();
}