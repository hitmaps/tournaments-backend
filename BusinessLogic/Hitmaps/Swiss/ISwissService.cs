using BusinessLogic.Models.Stage;

namespace BusinessLogic.Hitmaps.Swiss;

public interface ISwissService
{
    Task UpdatePlayerParticipationAsync(int stageId, UpdateSwissPlayerParticipationRequestModel requestModel, ulong discordSnowflake);
    Task StartSwissRoundAsync(int stageId, StartSwissRoundRequestModel requestModel, ulong discordSnowflake);
}