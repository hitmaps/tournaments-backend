using DataAccess.Models;
using Discord.Rest;

namespace BusinessLogic.Hitmaps;

public interface IEventService
{
    List<Event> GetAllEvents();
    Event GetEventForSlug(string slug, EventIncludes includes);
    List<Competitor> GetCompetitorsForEventAndSnowflake(string eventSlug, ulong snowflake);
    Task AssertEventManagementPermissionByBracketIdAsync(int bracketId, ulong discordSnowflake);
    Task AssertEventManagementPermissionByStageIdAsync(int stageId, ulong discordSnowflake);
    Task AssertEventManagementPermissionBySlugAsync(string eventSlug, ulong discordSnowflake);
    Task<Dictionary<RestGuildUser, string?>> GetShoutcastersForEventAsync(string slug);
}