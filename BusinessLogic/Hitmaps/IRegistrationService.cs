using BusinessLogic.Models;
using DataAccess.Models;

namespace BusinessLogic.Hitmaps;

public interface IRegistrationService
{
    Task<Competitor> RegisterUserAsync(string eventSlug, RegisterPlayerViewModel viewModel, ulong discordSnowflake);

    Task WithdrawUserAsync(string eventSlug, WithdrawPlayerViewModel viewModel, ulong discordSnowflake);

    Task PostSignupMessageAsync(Event theEvent);
}