namespace BusinessLogic.Hitmaps;

public interface IChallongeStandingsBuilder
{
    Task<List<StandingsParticipant>> GetStandingsAsync(int bracketId);
}