namespace BusinessLogic.Hitmaps;

public interface IVisualBracketsService
{
    string GetVisualBracket(int bracketId);
}