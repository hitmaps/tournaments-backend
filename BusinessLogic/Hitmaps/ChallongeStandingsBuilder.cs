using BusinessLogic.Challonge;
using Challonge.Objects;
using DataAccess.Data;

namespace BusinessLogic.Hitmaps;

public class ChallongeStandingsBuilder : IChallongeStandingsBuilder
{
    private readonly TournamentDbContext dbContext;
    private readonly IChallongeService challongeService;

    public ChallongeStandingsBuilder(TournamentDbContext dbContext, 
        IChallongeService challongeService)
    {
        this.dbContext = dbContext;
        this.challongeService = challongeService;
    }

    public async Task<List<StandingsParticipant>> GetStandingsAsync(int bracketId)
    {
        var bracket = dbContext.Brackets.First(x => x.Id == bracketId);
        var tournament = await challongeService.FetchTournamentsWithExtendedInfoAsync(bracket.ChallongeName!);
        
        // Too lazy to make this better
        var groupIdToLetter = new Dictionary<long, char>();
        var participantNameToIds = new Dictionary<string, List<long>>();
        var idsToScores = new Dictionary<long, int>();
        var idsToGroup = new Dictionary<long, char>();
        var idsToWins = new Dictionary<long, int>();
        var idsToLosses = new Dictionary<long, int>();
        var idsToTies = new Dictionary<long, int>();

        InitializeParticipants(tournament.Participants, participantNameToIds, idsToScores, idsToWins, idsToLosses, idsToTies);

        var nextLetter = 'A';
        foreach (var match in tournament.Matches.Where(match => match.GroupId.HasValue))
        {
            if (!groupIdToLetter.ContainsKey(match.GroupId!.Value))
            {
                groupIdToLetter[match.GroupId.Value] = nextLetter++;
            }
            
            idsToGroup[match.Player1Id!.Value] = groupIdToLetter[match.GroupId.Value];
            idsToGroup[match.Player2Id!.Value] = groupIdToLetter[match.GroupId.Value];

            if (match.State != MatchState.Complete)
            {
                continue;
            }

            
            foreach (var (playerOneScore, playerTwoScore) in match.Scores)
            {
                idsToScores[match.Player1Id.Value] += playerOneScore;
                idsToScores[match.Player2Id.Value] += playerTwoScore;
            }

            if (match.WinnerId.HasValue)
            {
                idsToWins[match.WinnerId.Value] += 1;
                idsToLosses[match.LoserId!.Value] += 1;
            }
            else
            {
                idsToTies[match.Player1Id.Value] += 1;
                idsToTies[match.Player2Id.Value] += 1;
            }
        }
        
        // Re-mesh scores and names
        var results = new List<StandingsParticipant>();
        // ReSharper disable once ReplaceSubstringWithRangeIndexer
        foreach (var (name, ids) in participantNameToIds.Where(x => x.Key.Substring(0) != "-"))
        {
            var points = 0;
            var wins = 0;
            var losses = 0;
            var ties = 0;
            var group = string.Empty;
            var matchesForParticipant = tournament.Matches
                .Count(x => (x.Player1Id.HasValue && ids.Contains(x.Player1Id.Value)) ||
                            (x.Player2Id.HasValue && ids.Contains(x.Player2Id.Value)));
            foreach (var id in ids)
            {
                points += idsToScores[id];
                wins += idsToWins[id];
                losses += idsToLosses[id];
                ties += idsToTies[id];

                if (idsToGroup.TryGetValue(id, out var value))
                {
                    group = value.ToString();
                }
            }

            var matchesPlayed = wins + losses + ties;
            results.Add(new StandingsParticipant
            {
                Name = name,
                Points = points,
                TheoreticalMaxPoints = points + 6 * (matchesForParticipant - matchesPlayed),
                Group = group,
                Wins = wins,
                Losses = losses,
                Ties = ties,
                WinningPercentage = matchesPlayed == 0 ? 0.0 : (wins + .5 * ties) / matchesPlayed
            });
        }
        
        results.Sort((a, b) =>
        {
            // ReSharper disable once InvertIf
            if (a.Group == b.Group)
            {
                if (a.Points == b.Points)
                {
                    return a.WinningPercentage.CompareTo(b.WinningPercentage) * -1;
                }

                return a.Points.CompareTo(b.Points) * -1;
            }

            return string.Compare(a.Group, b.Group, StringComparison.Ordinal);
        });

        return results;
    }

    private static void InitializeParticipants(IEnumerable<Participant> participants, IDictionary<string, List<long>> participantNameToIds,
        IDictionary<long, int> idsToScores, IDictionary<long, int> idsToWins, IDictionary<long, int> idsToLosses, IDictionary<long, int> idsToTies)
    {
        foreach (var participant in participants)
        {
            participantNameToIds[participant.Name] = new List<long> { participant.Id };
            idsToScores[participant.Id] = 0;
            idsToWins[participant.Id] = 0;
            idsToLosses[participant.Id] = 0;
            idsToTies[participant.Id] = 0;

            foreach (var groupId in participant.GroupPlayerIds)
            {
                participantNameToIds[participant.Name].Add(groupId);
                idsToScores[groupId] = 0;
                idsToWins[groupId] = 0;
                idsToLosses[groupId] = 0;
                idsToTies[groupId] = 0;
            }
        }
    }
}