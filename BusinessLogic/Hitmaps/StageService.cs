using System.Net.Http.Json;
using System.Text.Json;
using BusinessLogic.Exceptions;
using BusinessLogic.Extensions;
using BusinessLogic.Hitmaps.BracketGenerator;
using BusinessLogic.Hitmaps.GroupStage;
using BusinessLogic.Hitmaps.Swiss;
using BusinessLogic.Hitmaps.Swiss.Standings;
using BusinessLogic.Models;
using BusinessLogic.Models.GroupStage;
using BusinessLogic.Models.Stage;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ForfeitMatchRequestModel = BusinessLogic.Models.ForfeitMatchRequestModel;

namespace BusinessLogic.Hitmaps;

public class StageService : IStageService
{
    private readonly TournamentDbContext _context;
    private readonly IEventService _eventService;
    private readonly IGroupStageService _groupStageService;
    private readonly SwissStandingsBuilder _swissStandingsBuilder;
    private readonly HttpClientWrapper _httpClientWrapper;
    private readonly string _bracketManagerUrl;

    public StageService(TournamentDbContext context, 
        IEventService eventService, 
        IGroupStageService groupStageService, 
        SwissStandingsBuilder swissStandingsBuilder,
        HttpClientWrapper httpClientWrapper, 
        IConfiguration configuration)
    {
        _context = context;
        _eventService = eventService;
        _groupStageService = groupStageService;
        _swissStandingsBuilder = swissStandingsBuilder;
        _httpClientWrapper = httpClientWrapper;
        _bracketManagerUrl = configuration["BracketManagerUrl"];
    }

    public async Task BuildGroupStageAsync(int stageId, BuildGroupStageRequestModel requestModel)
    {
        var stage = _context.Stages
            .Include(x => x.Bracket)
                .ThenInclude(x => x.Event)
            .FirstOrDefault(x => x.Id == stageId);
        if (stage == null)
        {
            throw new Exception($"Could not find stage with id: {stageId}");
        }

        if (stage.State != State.Pending.Value)
        {
            throw new Exception($"Cannot build the group stage. Stage is in state '{stage.State}'");
        }
        
        // 1. Insert group stage members
        foreach (var group in requestModel.Groups)
        {
            foreach (var member in group.CompetitorIds)
            {
                var groupStageMember = new GroupStageMember
                {
                    Stage = stage,
                    GroupId = group.GroupId,
                    CompetitorId = member
                };
                _context.Update(groupStageMember);
            }
        }
        await _context.SaveChangesAsync();
        
        var allCompetitors = _context.Competitors.Where(x => x.Bracket.Id == stage.Bracket.Id).ToList();
        // 2. Insert MatchInfo records for each group
        foreach (var group in requestModel.Groups)
        {
            var rounds = GenerateRoundRobinSchedule(group.CompetitorIds);
            // Build matches for each round
            foreach (var round in rounds)
            {
                foreach (var match in round)
                {
                    _context.Update(new MatchInfo
                    {
                        Stage = stage,
                        GroupStage = true,
                        CompetitorInfo = new List<MatchCompetitorInfo>
                        {
                            new()
                            {
                                Competitor = allCompetitors.First(x => x.Id == match.Item1),
                                Order = 1
                            },
                            new()
                            {
                                Competitor = allCompetitors.First(x => x.Id == match.Item2),
                                Order = 2
                            }
                        },
                        State = MatchInfo.StateNeedsScheduling
                    });
                }
            }
        }

        await _context.SaveChangesAsync();
        
        // Mark the stage and the event as underway
        stage.State = State.Underway.Value;
        stage.Bracket.Event.State = State.Underway.Value;
        await _context.SaveChangesAsync();
    }

    private static List<List<(int, int)>> GenerateRoundRobinSchedule(List<int> competitors)
    {
        if (competitors.Count % 2 != 0)
        {
            // "Bye" competitor
            competitors.Add(-1);
        }
        
        var rounds = new List<List<(int, int)>>();

        for (int round = 0; round < competitors.Count - 1; round++)
        {
            var matches = new List<(int, int)>();

            for (int i = 0; i < competitors.Count / 2; i++)
            {
                var home = competitors[i];
                var away = competitors[competitors.Count - 1 - i];

                if (home != -1 && away != -1)
                {
                    matches.Add((home, away));
                }
            }
            
            rounds.Add(matches);
            
            // Rotate, keeping the first one fixed
            var firstCompetitor = competitors[0];
            competitors.RemoveAt(0);
            competitors.Insert(competitors.Count - 1, firstCompetitor);
        }

        return rounds;
    }

    public async Task FinalizeStageAsync(int stageId, ulong discordSnowflake)
    {
        await _eventService.AssertEventManagementPermissionByStageIdAsync(stageId, discordSnowflake);
        
        // @formatter:off
        var stage = _context.Stages
            .Include(x => x.Bracket)
                .ThenInclude(x => x.Stages.OrderBy(y => y.Id))
            .Include(x => x.Bracket)
                .ThenInclude(x => x.Event)
            .First(x => x.Id == stageId);
        // @formatter:on
        stage.State = State.Complete.Value;

        var stages = stage.Bracket.Stages;
        if (stage.Id == stages.Last().Id)
        {
            stage.Bracket.Event.State = State.Complete.Value;
        } else if (stage.Type.Contains("group-stage"))
        {
            //-- Handle group stage advancements
            var nextStage = stages[stages.IndexOf(stage) + 1];
            var placements = _groupStageService.GetStandingsForGroupStage(stage.Id);
            var index = 0;
            foreach (var group in placements)
            {
                foreach (var player in group.Participants.Where(x => x.Advanced).ToList())
                {
                    _context.Update(new StageCompetitorSeeding
                    {
                        StageId = nextStage.Id,
                        CompetitorId = player.CompetitorId,
                        Seeding = ++index
                    });
                }
            }

            nextStage.State = State.Pending.Value;
            _context.Update(nextStage);
        } else if (stage.Type.Contains("swiss"))
        {
            //-- Handle swiss advancements
            var nextStage = stages[stages.IndexOf(stage) + 1];
            var placements = _swissStandingsBuilder.GetStandings(stage.Id);
            var index = 0;
            foreach (var player in placements.Participants.Where(x => x.Advanced).ToList())
            {
                _context.Update(new StageCompetitorSeeding
                {
                    StageId = nextStage.Id,
                    CompetitorId = player.CompetitorId,
                    Seeding = ++index
                });
            }

            nextStage.State = State.Pending.Value;
            _context.Update(nextStage);
        }
        else
        {
            throw new NotImplementedException("Cannot handle going from non-group stage stage to another!");
        }
        
        _context.Update(stage);
        await _context.SaveChangesAsync();
    }

    public List<StageCompetitorSeeding> GetSeedings(int stageId)
    {
        return _context.StageCompetitorSeedings.Where(x => x.StageId == stageId).ToList();
    }

    public List<Competitor> GetCompetitorsForStage(int stageId)
    {
        var stage = _context.Stages
            .Include(x => x.Bracket)
            .ThenInclude(x => x.Competitors)
            .FirstOrDefault(x => x.Id == stageId);
        if (stage == null)
        {
            throw new NotFoundException<Stage, int>(stageId);
        }
        
        return stage.Bracket.Competitors;
    }

    public async Task<string> GetBracketPreviewAsync(int stageId)
    {
        var stage = _context.Stages.First(x => x.Id == stageId);
        
        return await GenerateBracketAsync(stage, true);
    }

    private async Task<string> GenerateBracketAsync(Stage stage, bool previewOnly = false)
    {
        var seedings = _context.StageCompetitorSeedings
            .Where(x => x.StageId == stage.Id)
            .OrderBy(x => x.Seeding)
            .ToList();
        var competitorIds = seedings.Select(x => x.CompetitorId).ToList();
        var competitors = _context.Competitors
            .Where(x => competitorIds.Contains(x.Id))
            .ToList();
        
        var requestPath = previewOnly ? "bracket-preview" : "brackets";
        var seedOrdering = new List<string> { "inner_outer" };
        if (stage.Type == "double-elimination")
        {
            // Add second seeding for loser's bracket
            seedOrdering.Add("inner_outer");
        }
        using var request = new HttpRequestMessage(HttpMethod.Post, $"{_bracketManagerUrl}/{requestPath}");
        request.Content = JsonContent.Create(new CreateBracketRequestModel
        {
            TournamentId = stage.Id,
            Name = stage.Name,
            Type = stage.Type.Replace("-", "_"),
            Seeding = seedings.Select(x => competitors.First(y => y.Id == x.CompetitorId).ChallongeName!).ToList(),
            Settings = new Settings
            {
                SeedOrdering = seedOrdering,
                Size = ClosestPowerOfTwo(seedings.Count),
                GrandFinal = "simple"
            }
        });
        
        var response = await _httpClientWrapper.GetClient().SendAsync(request);
        if (!response.IsSuccessStatusCode)
        {
            throw new Exception(response.ReasonPhrase);
        }

        return await response.Content.ReadAsStringAsync();
    }

    private static int ClosestPowerOfTwo(int number)
    {
        // If the number is already a power of two, return it
        if ((number & (number - 1)) == 0)
        {
            return number;
        }

        // Otherwise, find the closest power of two greater than or equal to the number
        var power = 1;
        while (power < number)
        {
            power *= 2;
        }

        return power;
    }

    public async Task UpdateSeedingsAsync(int stageId, List<StageSeedingViewModel> seedings, ulong discordSnowflake)
    {
        await _eventService.AssertEventManagementPermissionByStageIdAsync(stageId, discordSnowflake);

        var currentSeedings = _context.StageCompetitorSeedings.Where(x => x.StageId == stageId).ToList();
        foreach (var currentSeeding in currentSeedings)
        {
            var newSeed = seedings.First(x => x.CompetitorId == currentSeeding.CompetitorId).Seed;
            if (currentSeeding.Seeding == newSeed)
            {
                continue;
            }
            
            currentSeeding.Seeding = newSeed;
            _context.Update(currentSeeding);
        }
        
        await _context.SaveChangesAsync();
    }

    public async Task UpdateStageInfoAsync(int stageId, UpdateStageRequestModel requestModel, ulong discordSnowflake)
    {
        await _eventService.AssertEventManagementPermissionByStageIdAsync(stageId, discordSnowflake);

        var stage = _context.Stages
            .Include(x => x.Bracket)
                .ThenInclude(x => x.Competitors)
            .First(x => x.Id == stageId);
        if (requestModel.State != null)
        {
            var state = State.FromValue(requestModel.State);
            stage.State = state.Value;

            if (state == State.Underway && new List<string> { "single-elimination", "double-elimination" }.Contains(stage.Type))
            {
                // Generate bracket
                await GenerateBracketAsync(stage);
            } else if (state == State.Underway && stage.Type == "swiss")
            {
                // By default, add all competitors to each stage
                var stageSettings = stage.SettingsJson.DeserializeFromJsonCaseInsensitive<SwissStageSettings>()!;
                var competitorIds = stage.Bracket.Competitors.Select(x => x.Id).ToList();
                stageSettings.Rounds.ForEach(round => round.Competitors = competitorIds);
                stage.SettingsJson = stageSettings.SerializeToJsonCamelCase();
            }
        }
        
        _context.Update(stage);
        await _context.SaveChangesAsync();
    }

    public async Task<T> GetBracketAsync<T>(int stageId)
    {
        using var request = new HttpRequestMessage(HttpMethod.Get, $"{_bracketManagerUrl}/brackets/{stageId}");
        
        var response = await _httpClientWrapper.GetClient().SendAsync(request);
        if (!response.IsSuccessStatusCode)
        {
            throw new Exception(response.ReasonPhrase);
        }

        return (await JsonSerializer.DeserializeAsync<T>(await response.Content.ReadAsStreamAsync(), new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        }))!;
    }

    public async Task<Stage?> GetStageAsync(int stageId)
    {
        return await _context.Stages.FirstOrDefaultAsync(x => x.Id == stageId);
    }

    public async Task ForfeitMatchAsync(int stageId, long matchId, ForfeitMatchRequestModel requestModel, ulong discordSnowflake)
    {
        await _eventService.AssertEventManagementPermissionByStageIdAsync(stageId, discordSnowflake);
        
        var matchup = await _context.Matchups
            .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Competitor)
            .FirstOrDefaultAsync(x => x.Id == matchId);
        if (matchup == null)
        {
            throw new Exception("Matchup not found");
        }

        requestModel.ForfeitPlayerIds.ForEach(x => matchup.CompetitorInfo.First(competitor => competitor.Competitor.Id == x).Forfeit = true);
        matchup.State = State.Complete.Value;
        
        _context.Update(matchup);
        
        await _context.SaveChangesAsync();
    }
}