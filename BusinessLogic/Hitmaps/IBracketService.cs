using BusinessLogic.Challonge;
using DataAccess.Models;

namespace BusinessLogic.Hitmaps;

public interface IBracketService
{
    Task<ExtendedTournamentInfo?> GetChallongeTournamentAsync(int bracketId);

    Bracket GetBracketInformation(int bracketId);
}