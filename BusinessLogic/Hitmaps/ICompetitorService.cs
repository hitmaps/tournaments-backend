using BusinessLogic.Models;
using DataAccess.Models;

namespace BusinessLogic.Hitmaps;

public interface ICompetitorService
{
    Competitor? FetchMostRecentRegistration(ulong discordSnowflake, string platform);
    Competitor FetchCompetitorByDiscordAndStage(ulong discordSnowflake, int stageId);
    Task EditCompetitorAsync(int competitorId, EditCompetitorRequestModel requestModel);
}