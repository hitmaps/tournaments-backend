namespace BusinessLogic.Hitmaps.GroupStage;

public class GroupStageViewModel
{
    public List<GroupViewModel> Groups { get; set; } = new();

    public static GroupStageViewModel FromBusinessModel(GroupStage groupStage)
    {
        return new GroupStageViewModel
        {
            Groups = groupStage.Groups.Select(x => new GroupViewModel
            {
                GroupId = x.GroupId,
                CompetitorIds = x.CompetitorIds
            }).ToList()
        };
    }
}

public class GroupViewModel
{
    public int GroupId { get; set; }
    public List<int> CompetitorIds { get; set; } = new();
}