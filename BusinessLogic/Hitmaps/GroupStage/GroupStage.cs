using BusinessLogic.Models.GroupStage;

namespace BusinessLogic.Hitmaps.GroupStage;

public class GroupStage
{
    public List<Group> Groups { get; set; } = new();
}