namespace BusinessLogic.Hitmaps.GroupStage;

public interface IGroupStageService
{
    GroupStage GetGroupStageInfoForStage(int stageId);
    List<GroupStandingsResult> GetStandingsForGroupStage(int stageId);
}