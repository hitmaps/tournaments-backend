namespace BusinessLogic.Hitmaps.GroupStage;

public class GroupStageSettings : StageSettings
{
    public int PlayersPerGroup { get; set; }
    public int AdvancementsPerGroup { get; set; }
}