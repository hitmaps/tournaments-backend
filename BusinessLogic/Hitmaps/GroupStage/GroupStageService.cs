using System.Text.Json;
using BusinessLogic.Models.GroupStage;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Hitmaps.GroupStage;

public class GroupStageService : IGroupStageService
{
    private readonly TournamentDbContext _context;

    public GroupStageService(TournamentDbContext context)
    {
        _context = context;
    }

    public GroupStage GetGroupStageInfoForStage(int stageId)
    {
        var stage = _context.Stages.FirstOrDefault(x => x.Id == stageId);
        if (stage == null)
        {
            throw new Exception($"Stage with id {stageId} not found");
        }

        var groupStageInfo = new GroupStage();
        var groupStageMembers = _context.GroupStageMembers.Where(x => x.Stage == stage);
        foreach (var member in groupStageMembers)
        {
            if (groupStageInfo.Groups.All(x => x.GroupId != member.GroupId))
            {
                groupStageInfo.Groups.Add(new Group
                {
                    GroupId = member.GroupId
                });
            }
            groupStageInfo.Groups.First(x => x.GroupId == member.GroupId).CompetitorIds.Add(member.CompetitorId);
        }
        
        groupStageInfo.Groups.Sort((x, y) => x.GroupId.CompareTo(y.GroupId));
        return groupStageInfo;
    }

    public List<GroupStandingsResult> GetStandingsForGroupStage(int stageId)
    {
        // @formatter:off
        var theEvent = _context.Events
            .Include(x => x.Brackets)
                .ThenInclude(x => x.Stages)
                    .ThenInclude(x => x.ScheduledMatches)
                        .ThenInclude(x => x.CompetitorInfo)
                            .ThenInclude(x => x.Competitor)
            .Include("Brackets.Competitors")
            .Include("Brackets.Stages.ScheduledMatches.MapSelections")
            .Include("Brackets.Stages.ScheduledMatches.MapSelections.Winner")
            .AsSplitQuery()
            .FirstOrDefault(x => x.Brackets.Any(y => y.Stages.Any(z => z.Id == stageId)));
        // @formatter:on

        if (theEvent == null)
        {
            return new List<GroupStandingsResult>();
        }
        
        // Too lazy to make this better
        var competitorToScores = new Dictionary<Competitor, int>();
        var competitorToGroup = new Dictionary<Competitor, char>();
        var competitorToWins = new Dictionary<Competitor, int>();
        var competitorToLosses = new Dictionary<Competitor, int>();
        var competitorToDraws = new Dictionary<Competitor, int>();
        
        // Grab completed matches
        var bracket = theEvent.Brackets.First(x => x.Stages.Any(y => y.Id == stageId));
        var stage = bracket.Stages.First(x => x.Id == stageId);
        var completedMatches = stage
            .ScheduledMatches.Where(x => x.MatchCompletedAt.HasValue && x.GroupStage)
            .ToList();
        var groupStageMembers = _context.GroupStageMembers.Where(x => x.Stage == stage).ToList();
        
        // Seed competitors
        foreach (var member in groupStageMembers)
        {
            var competitor = bracket.Competitors.First(x => x.Id == member.CompetitorId);
            competitorToScores.Add(competitor, 0);
            competitorToGroup.Add(competitor, GetLetterForGroup(member, groupStageMembers));
            competitorToWins.Add(competitor, 0);
            competitorToLosses.Add(competitor, 0);
            competitorToDraws.Add(competitor, 0);
        }

        foreach (var match in completedMatches)
        {
            var firstCompetitor = match.CompetitorInfo.First(x => x.Order == 1);
            var secondCompetitor = match.CompetitorInfo.First(x => x.Order == 2);

            var playerOneScore = 0;
            var playerTwoScore = 0;
            foreach (var map in match.MapSelections)
            {
                if (map.Winner == null)
                {
                    playerOneScore++;
                    playerTwoScore++;
                } else if (map.Winner.Id == firstCompetitor.Competitor.Id)
                {
                    playerOneScore += 2;
                }
                else
                {
                    playerTwoScore += 2;
                }
            }
            competitorToScores[firstCompetitor.Competitor] += playerOneScore;
            competitorToScores[secondCompetitor.Competitor] += playerTwoScore;

            if (playerOneScore == playerTwoScore)
            {
                competitorToDraws[firstCompetitor.Competitor]++;
                competitorToDraws[secondCompetitor.Competitor]++;
            } else if (playerOneScore > playerTwoScore)
            {
                competitorToWins[firstCompetitor.Competitor]++;
                competitorToLosses[secondCompetitor.Competitor]++;
            }
            else
            {
                competitorToWins[secondCompetitor.Competitor]++;
                competitorToLosses[firstCompetitor.Competitor]++;
            }
        }
        
        // Data aggregated, format for response
        var results = new List<GroupStandingsResult>();
        foreach (var competitor in competitorToScores.Keys)
        {
            var groupName = competitorToGroup[competitor];
            var groupResults = results.FirstOrDefault(x => x.GroupName == groupName);
            if (groupResults == null)
            {
                groupResults = new GroupStandingsResult
                {
                    GroupName = groupName
                };
                results.Add(groupResults);
            }
            
            var points = competitorToScores[competitor];
            var wins = competitorToWins[competitor];
            var losses = competitorToLosses[competitor];
            var ties = competitorToDraws[competitor];
            var matchesPlayed = wins + losses + ties;
            var matchesForParticipant = groupStageMembers.Count(x => x.GroupId == GetNumberForGroup(groupName)) - 1;
            groupResults.Participants.Add(new StandingsParticipant
            {
                CompetitorId = competitor.Id,
                Name = competitor.ChallongeName!,
                Points = points,
                TheoreticalMaxPoints = points + 6 * (matchesForParticipant - matchesPlayed),
                Group = groupName.ToString(),
                GroupId = GetNumberForGroup(groupName),
                Wins = wins,
                Losses = losses,
                Ties = ties,
                WinningPercentage = matchesPlayed == 0 ? 0.0 : (wins + .5 * ties) / matchesPlayed
            });
        }
        
        // Sort results
        foreach (var group in results)
        {
            // Sort highest to lowest
            group.Participants.Sort((a, b) =>
            {
                if (a.Points == b.Points)
                {
                    return a.WinningPercentage.CompareTo(b.WinningPercentage) * -1;
                }
                
                return a.Points.CompareTo(b.Points) * -1;
            });
            
            // Track advancements / eliminations
            var numberOfAdvancements = JsonSerializer.Deserialize<GroupStageSettings>(stage.SettingsJson, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            })!.AdvancementsPerGroup;
            var allMatchesComplete = group.Participants.All(x => x.Points == x.TheoreticalMaxPoints);
            var index = 0;
            foreach (var participant in group.Participants)
            {
                var sortedTheoreticals = group.Participants.Select(x => x.TheoreticalMaxPoints).OrderBy(x => x).ToList();
                sortedTheoreticals.Remove(participant.TheoreticalMaxPoints);
                sortedTheoreticals.Add(participant.Points);
                sortedTheoreticals = sortedTheoreticals.OrderBy(x => x).ToList();
                if (allMatchesComplete)
                {
                    if (index < numberOfAdvancements - 1)
                    {
                        participant.Advanced = true;
                    } else if (index > numberOfAdvancements - 1)
                    {
                        // Anyone lower than guaranteed spots are eliminated if they're worse than the lowest possible's points
                        participant.Eliminated = participant.Points < group.Participants[numberOfAdvancements - 1].Points;
                    }
                    else
                    {
                        
                        if (group.Participants.Count == numberOfAdvancements)
                        {
                            // Not enough participants to eliminate anyone
                            participant.Advanced = true;
                        }
                        else
                        {
                            // Last slot - Advanced if better than the next participant in line
                            participant.Advanced = participant.Points > group.Participants[numberOfAdvancements].Points;                            
                        }
                    }
                } else if (participant.TheoreticalMaxPoints < group.Participants[numberOfAdvancements - 1].Points)
                {
                    // Participant cannot possibly reach 4th's current point count
                    participant.Eliminated = true;
                } else if (sortedTheoreticals.IndexOf(participant.Points) >= sortedTheoreticals.Count - numberOfAdvancements)
                {
                    // If the participant's current point count is in 4th position or better, they're advancing
                    // indexOf will return the lowest value, so it's safe to assume 4th == advancing.
                    participant.Advanced = true;
                }

                index++;
            }
        }
        
        return results;
    }

    private static char GetLetterForGroup(GroupStageMember groupMember, List<GroupStageMember> groupStageMembers)
    {
        return (char)(65 + groupMember.GroupId);
    }

    private static int GetNumberForGroup(char letter)
    {
        return letter - 65;
    }
}