namespace BusinessLogic.Hitmaps.GroupStage;

public class GroupStandingsResult
{
    public char GroupName { get; set; }
    public int GroupId => GroupName - 65;
    public List<StandingsParticipant> Participants { get; set; } = new();
}