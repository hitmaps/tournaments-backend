using BusinessLogic.Models;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Hitmaps;

public class CompetitorService : ICompetitorService
{
    private readonly TournamentDbContext dbContext;

    public CompetitorService(TournamentDbContext dbContext)
    {
        this.dbContext = dbContext;
    }

    public Competitor? FetchMostRecentRegistration(ulong discordSnowflake, string platform)
    {
        return dbContext.Competitors
            .Include(x => x.Bracket)
            .Where(x => x.DiscordId == discordSnowflake && x.Bracket.Platform == platform)
            .OrderByDescending(x => x.Id)
            .FirstOrDefault();
    }

    public Competitor FetchCompetitorByDiscordAndStage(ulong discordSnowflake, int stageId)
    {
        var stage = dbContext.Stages
            .Include("Bracket.Competitors")
            .Include("Bracket.Competitors.ScheduledMatches")
            .Include("Bracket.Competitors.ScheduledMatches.Match.CompetitorInfo")
            .Include("Bracket.Competitors.ScheduledMatches.Match.Stage")
            .Include("Bracket.Competitors.ScheduledMatches.Match.MapSelections")
            .Include("Bracket.Competitors.ScheduledMatches.Match.MapSelections.Mission")
            .First(x => x.Id == stageId);
        
        return stage.Bracket.Competitors.First(x => x.DiscordId == discordSnowflake);
    }

    public async Task EditCompetitorAsync(int competitorId, EditCompetitorRequestModel requestModel)
    {
        var competitor = dbContext.Competitors.FirstOrDefault(x => x.Id == competitorId);
        if (competitor == null)
        {
            throw new Exception("Competitor not found");
        }
        
        competitor.CountryCode = requestModel.Country;
        competitor.ChallongeName = requestModel.Name;
        competitor.StreamUrl = requestModel.StreamUrl;
        dbContext.Update(competitor);
        await dbContext.SaveChangesAsync();
    }
}