namespace BusinessLogic.Hitmaps.BracketGenerator;

public class CreateBracketRequestModel
{
    public int TournamentId { get; set; }
    public string Name { get; set; } = null!;
    public string Type { get; set; } = null!;
    public List<string> Seeding { get; set; } = new();
    public Settings Settings { get; set; } = null!;
}

public class Settings
{
    public List<string> SeedOrdering { get; set; } = new();
    public int Size { get; set; }
    public string GrandFinal { get; set; } = null!;
}