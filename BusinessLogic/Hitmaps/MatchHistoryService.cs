using BusinessLogic.Challonge;
using BusinessLogic.Extensions;
using BusinessLogic.Models;
using BusinessLogic.Models.MatchHistory;
using Challonge.Objects;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Hitmaps;

public class MatchHistoryService : IMatchHistoryService
{
    private readonly TournamentDbContext dbContext;
    private readonly IChallongeService challongeService;

    public MatchHistoryService(TournamentDbContext dbContext, IChallongeService challongeService)
    {
        this.dbContext = dbContext;
        this.challongeService = challongeService;
    }

    public async Task<List<MatchHistoryViewModel>> GetMatchHistoryAsync(string eventSlug, ulong discordId)
    {
        //@formatter:off
        var theEvent = dbContext.Events
            .Include(x => x.Brackets)
                .ThenInclude(x => x.Maps)
            .Include(x => x.Brackets)
                .ThenInclude(x => x.Stages)
                    .ThenInclude(x => x.ScheduledMatches)
                        .ThenInclude(x => x.CompetitorInfo)
                            .ThenInclude(x => x.Competitor)
            .Include(x => x.Brackets)
                .ThenInclude(x => x.Stages)
                    .ThenInclude(x => x.ScheduledMatches)
                        .ThenInclude(x => x.MapSelections)
            .First(x => x.Slug == eventSlug);
        //@formatter:on

        if (theEvent.UsesChallonge)
        {
            return await GetChallongeMatchHistory(theEvent, discordId);
        }

        var viewmodel = new MatchHistoryViewModel
        {
            TournamentName = theEvent.Name
        };
        var allMatches = theEvent.Brackets
            .SelectMany(x => x.Stages)
            .SelectMany(x => x.ScheduledMatches)
            .Where(x => x.CompetitorInfo.Any(y => y.Competitor.DiscordId == discordId))
            .OrderBy(x => x.MatchScheduledAt)
            .ToList();
        foreach (var match in allMatches)
        {
            var status = "not-scheduled";
            if (match.MatchCompletedAt.HasValue)
            {
                status = "complete";
            } else if (match.MatchScheduledAt.HasValue)
            {
                status = "scheduled";
            }

            viewmodel.Matches.Add(new MatchupViewModel
            {
                GroupStage = match.GroupStage,
                Round = match.Round,
                Status = status,
                OpponentDiscordId = match.CompetitorInfo.First(x => x.Competitor.DiscordId != discordId).Competitor.DiscordId.ToString(),
                MatchScheduledAt = match.MatchScheduledAt?.ToApiDateTimeFormat(),
                Maps = match.MapSelections.Select(x => MatchupMapSelectionViewModel.FromDbModel(x)!).ToList()
            });
        }

        return new List<MatchHistoryViewModel> { viewmodel };
    }

    private async Task<List<MatchHistoryViewModel>> GetChallongeMatchHistory(Event theEvent, ulong discordId)
    {
        var challongeBrackets = await GetChallongeBracketsAsync(theEvent);
        var challongePlayerIdToDiscordId = GetChallongePlayerToDiscordIdMapping(theEvent, challongeBrackets);
        var challongePlayerIds = challongePlayerIdToDiscordId
            .Where(x => x.Value == discordId)
            .Select(x => x.Key)
            .ToList();
        var matchInfoForCompetitor = dbContext.Matchups
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Where(x => x.CompetitorInfo.Any(y => y.Competitor.DiscordId == discordId))
            .ToList();
        var challongeMatchesForUser = GetMatchesForUser(challongeBrackets, challongePlayerIds);

        var results = new List<MatchHistoryViewModel>();
        foreach (var (tournamentName, matches) in challongeMatchesForUser)
        {
            var matchModels = matches.Select(match =>
            {
                var opponentPlayerId = match.Player1Id.HasValue && challongePlayerIds.Contains(match.Player1Id.Value)
                    ? match.Player2Id
                    : match.Player1Id;
                var model = new MatchupViewModel
                {
                    ChallongeId = match.Id,
                    GroupStage = match.GroupId.HasValue,
                    Round = match.GroupId.HasValue ? 0 : match.Round
                };

                if (match.State == MatchState.Complete)
                {
                    var scheduledMatchInfo = matchInfoForCompetitor.FirstOrDefault(x => x.ChallongeMatchId == match.Id);
                    model.Status = "complete";
                    model.OpponentDiscordId = challongePlayerIdToDiscordId[opponentPlayerId!.Value].ToString();

                    if (scheduledMatchInfo != null)
                    {
                        model.Maps = scheduledMatchInfo.MapSelections
                            .Select(MatchupMapSelectionViewModel.FromDbModel)
                            .ToList();
                    }
                } else if (match.State == MatchState.Open)
                {
                    model.OpponentDiscordId = challongePlayerIdToDiscordId[opponentPlayerId!.Value].ToString();
                    
                    var scheduledMatchInfo = matchInfoForCompetitor.FirstOrDefault(x => x.ChallongeMatchId == match.Id);
                    if (scheduledMatchInfo != null)
                    {
                        model.Status = "scheduled";
                        model.MatchScheduledAt = scheduledMatchInfo.MatchScheduledAt?.ToApiDateTimeFormat();
                        model.Maps = scheduledMatchInfo.MapSelections
                            .Select(MatchupMapSelectionViewModel.FromDbModel)
                            .ToList();
                    }
                    else
                    {
                        model.Status = "not-scheduled";
                    }
                } else if (match.State == MatchState.Pending)
                {
                    model.Status = "not-schedulable";
                }

                return model;
            }).ToList();
            
            results.Add(new MatchHistoryViewModel
            {
                TournamentName = tournamentName,
                Matches = matchModels
            });
        }

        return results;
    }
    
    private async Task<List<ExtendedTournamentInfo>> GetChallongeBracketsAsync(Event theEvent)
    {
        var challongeBrackets = new List<ExtendedTournamentInfo>();
        foreach (var bracket in theEvent.Brackets)
        {
            challongeBrackets.Add(
                await challongeService.FetchTournamentsWithExtendedInfoAsync(bracket.ChallongeName!));
        }

        return challongeBrackets;
    }

    private IDictionary<long?, ulong> GetChallongePlayerToDiscordIdMapping(Event theEvent, List<ExtendedTournamentInfo> challongeTournaments)
    {
        var competitors = theEvent.Brackets
            .SelectMany(x => x.Competitors)
            .ToDictionary(x => x.ChallongePlayerId, x => x.DiscordId);

        var participants = challongeTournaments
            .SelectMany(x => x.Participants)
            .ToList();
        foreach (var participant in participants)
        {
            var discordId = competitors[participant.Id];
            foreach (var groupId in participant.GroupPlayerIds)
            {
                competitors[groupId] = discordId;
            }
        }

        return competitors;
    }

    private IDictionary<string, List<Match>> GetMatchesForUser(List<ExtendedTournamentInfo> challongeTournaments, List<long?> playerIds)
    {
        var results = new Dictionary<string, List<Match>>();
        foreach (var challongeTournament in challongeTournaments)
        {
            var matches = challongeTournament.Matches
                .Where(x => (x.Player1Id.HasValue && playerIds.Contains(x.Player1Id.Value)) || 
                            (x.Player2Id.HasValue && playerIds.Contains(x.Player2Id.Value)))
                .ToList();
            results[challongeTournament.Bracket.Name] = matches;
        }

        return results;
    }
}