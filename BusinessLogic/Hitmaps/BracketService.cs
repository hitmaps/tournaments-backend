using BusinessLogic.Challonge;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Hitmaps;

public class BracketService : IBracketService
{
    private readonly IChallongeService challongeService;
    private readonly TournamentDbContext dbContext;

    public BracketService(IChallongeService challongeService, TournamentDbContext dbContext)
    {
        this.challongeService = challongeService;
        this.dbContext = dbContext;
    }

    public async Task<ExtendedTournamentInfo?> GetChallongeTournamentAsync(int bracketId)
    {
        var bracket = dbContext.Brackets.First(x => x.Id == bracketId);
        if (bracket.ChallongeName == null)
        {
            return null;
        }

        return await challongeService.FetchTournamentsWithExtendedInfoAsync(bracket.ChallongeName!);
    }

    public Bracket GetBracketInformation(int bracketId)
    {
        //@formatter:off
        return dbContext.Brackets
            .Include(x => x.Event)
            .Include(x => x.Competitors)
            .Include(x => x.Stages)
                .ThenInclude(x => x.ScheduledMatches)
                    .ThenInclude(x => x.CompetitorInfo.OrderBy(y => y.Order))
                        .ThenInclude(x => x.Competitor)
            .Include(x => x.Stages)
                .ThenInclude(x => x.ScheduledMatches)
                    .ThenInclude(x => x.Casters)
            .Include(x => x.Stages)
                .ThenInclude(x => x.ScheduledMatches)
                    .ThenInclude(x => x.MapSelections)
                        .ThenInclude(x => x.Mission)
            .Include(x => x.Stages)
                .ThenInclude(x => x.PointsForVictories)
            .AsSplitQuery()
            .First(x => x.Id == bracketId);
        //@formatter:on
    }
}