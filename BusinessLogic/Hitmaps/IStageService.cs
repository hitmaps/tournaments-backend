using BusinessLogic.Models;
using BusinessLogic.Models.GroupStage;
using BusinessLogic.Models.Stage;
using BusinessLogic.SignalR.RequestModels;
using DataAccess.Models;
using Bracket = BusinessLogic.BracketManager.Bracket;
using ForfeitMatchRequestModel = BusinessLogic.Models.ForfeitMatchRequestModel;

namespace BusinessLogic.Hitmaps;

public interface IStageService
{
    Task BuildGroupStageAsync(int stageId, BuildGroupStageRequestModel requestModel);
    Task FinalizeStageAsync(int stageId, ulong discordSnowflake);
    List<StageCompetitorSeeding> GetSeedings(int stageId);
    List<Competitor> GetCompetitorsForStage(int stageId);
    Task<string> GetBracketPreviewAsync(int stageId);
    Task UpdateSeedingsAsync(int stageId, List<StageSeedingViewModel> seedings, ulong discordSnowflake);
    Task UpdateStageInfoAsync(int stageId, UpdateStageRequestModel requestModel, ulong discordSnowflake);
    Task<T> GetBracketAsync<T>(int stageId);
    Task<Stage?> GetStageAsync(int stageId);
    Task ForfeitMatchAsync(int stageId, long matchId, ForfeitMatchRequestModel requestModel, ulong discordSnowflake);
}