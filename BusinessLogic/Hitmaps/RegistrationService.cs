using BusinessLogic.Challonge;
using BusinessLogic.Discord;
using BusinessLogic.Models;
using DataAccess.Data;
using DataAccess.Models;
using Discord;
using Discord.Net;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BusinessLogic.Hitmaps;

public class RegistrationService : IRegistrationService
{
    private readonly TournamentDbContext dbContext;
    private readonly IChallongeService challongeService;
    private readonly IDiscordService discordService;
    private readonly ThreadSafeExecutor threadSafeExecutor;
    private readonly IConfiguration configuration;

    public RegistrationService(TournamentDbContext dbContext, 
        IChallongeService challongeService, 
        IDiscordService discordService,
        ThreadSafeExecutor threadSafeExecutor,
        IConfiguration configuration)
    {
        this.dbContext = dbContext;
        this.challongeService = challongeService;
        this.discordService = discordService;
        this.threadSafeExecutor = threadSafeExecutor;
        this.configuration = configuration;
    }

    public async Task<Competitor> RegisterUserAsync(string eventSlug, RegisterPlayerViewModel viewModel, ulong discordSnowflake)
    {
        var theEvent = dbContext.Events
            .Include(x => x.Brackets)
            .Include(x => x.DiscordAdminRoleIds)
            .AsSplitQuery()
            .First(x => x.Slug == eventSlug);
        var userInfo = await discordService.GetUserForGuildAsync(theEvent.DiscordGuildId, discordSnowflake);
        if (theEvent.DiscordBannedCompetitorRoleId.HasValue &&
            userInfo!.RoleIds.Contains(theEvent.DiscordBannedCompetitorRoleId.Value))
        {
            throw new Exception("User is banned from competing in this event.");
        }

        if (theEvent.RegistrationOpensAt > DateTime.UtcNow &&
            !userInfo!.RoleIds.Any(userRole => theEvent.DiscordAdminRoleIds.Select(x => x.DiscordRoleId).Contains(userRole.ToString())))
        {
            throw new Exception("User is not permitted to register at this time.");
        }
        
        var bracket = theEvent.Brackets.First(x => x.Platform == viewModel.Platform);
        long? challongePlayerId = null;
        if (theEvent.UsesChallonge)
        {
            challongePlayerId = await challongeService.RegisterUserAsync(bracket.ChallongeName!, viewModel.Name);
        }

        var competitor = new Competitor
        {
            ChallongeName = viewModel.Name,
            ChallongePlayerId = challongePlayerId,
            DiscordId = discordSnowflake,
            Bracket = bracket,
            ProfileName = viewModel.ProfileName ?? string.Empty,
            StreamUrl = BuildStreamUrl(viewModel),
            CountryCode = viewModel.Country,
            TimeZone = viewModel.TimeZone
        };

        dbContext.Update(competitor);
        await dbContext.SaveChangesAsync();

        await discordService.AddRoleToUserAsync(theEvent.DiscordGuildId, 
            discordSnowflake,
            theEvent.ParticipantDiscordRoleId);
        
        var embedBuilder = new EmbedBuilder()
            .WithTitle(bracket.Platform == "All" ? theEvent.Name : $"{theEvent.Name} - {bracket.Platform}")
            .WithFooter("Best of luck, agent.")
            .WithThumbnailUrl(await discordService.GetAvatarUrlAsync(theEvent.DiscordGuildId, discordSnowflake))
            .AddField("Discord", $"<@{discordSnowflake}>");
        if (bracket.Platform != "All")
        {
            embedBuilder.AddField("Bracket", bracket.Platform);
        }
        if (!string.IsNullOrEmpty(competitor.StreamUrl))
        {
            embedBuilder.AddField("Stream URL", competitor.StreamUrl);
        }
        if (viewModel.ProfileName != null)
        {
            embedBuilder.AddField("Profile Name", viewModel.ProfileName);
        }
        
        await discordService.SendMessageAsync(theEvent.DiscordGuildId, 
            theEvent.DiscordSignupChannelId, 
            $"<@{discordSnowflake}> has signed up for {theEvent.Name}!", embedBuilder.Build());
        await PostSignupMessageAsync(theEvent);

        return competitor;
    }

    private static string BuildStreamUrl(RegisterPlayerViewModel viewModel)
    {
        var streamInfo = viewModel.StreamPlatform.Split("||");
        
        return streamInfo[0] switch
        {
            "twitch" => $"https://www.twitch.tv/{streamInfo[2]}",
            "youtube" => $"https://www.youtube.com/channel/{streamInfo[1]}",
            _ => string.Empty
        };
    }

    public async Task WithdrawUserAsync(string eventSlug, WithdrawPlayerViewModel viewModel, ulong discordSnowflake)
    {
        var theEvent = dbContext.Events
            .Include(x => x.Brackets)
                .ThenInclude(x => x.Competitors)
            .First(x => x.Slug == eventSlug);
        var bracket = theEvent.Brackets.First(x => x.Platform == viewModel.Platform);
        var competitor = bracket.Competitors.First(x => x.DiscordId == discordSnowflake);

        if (theEvent.UsesChallonge)
        {
            await challongeService.WithdrawUserAsync(bracket.ChallongeName!, (long)competitor.ChallongePlayerId!);            
        }
        
        
        bracket.Competitors.Remove(competitor);
        await dbContext.SaveChangesAsync();

        if (!theEvent.Brackets.Any(x => x.Competitors.Any(y => y.DiscordId == discordSnowflake)))
        {
            await discordService.RemoveRoleFromUserAsync(theEvent.DiscordGuildId, 
                discordSnowflake,
                theEvent.ParticipantDiscordRoleId);
        }

        var tourneyDisplay = bracket.Platform == "All" ? theEvent.Name : $"{theEvent.Name} - {bracket.Platform}";
        await discordService.SendMessageAsync(theEvent.DiscordGuildId, 
            theEvent.DiscordSignupChannelId, 
            $"<@{discordSnowflake}> has withdrawn from {tourneyDisplay}.");
        await PostSignupMessageAsync(theEvent);
    }

    public async Task PostSignupMessageAsync(Event theEvent)
    {
        // "\uD83D\uDCCB" == 📋
        var discordButton = new ComponentBuilder()
            .WithButton($"Sign up for {theEvent.Name}",
                style: ButtonStyle.Link,
                url: $"{configuration["FrontendUrl"]}/events/{theEvent.Slug}/register",
                emote: new Emoji("\uD83D\uDCCB"));

        await threadSafeExecutor.DoInThreadSafety(async () =>
        {
            // Attempt to delete old signup message if one exists
            if (theEvent.SignupMessageId.HasValue)
            {
                await DeleteOldMessageAsync(theEvent);
            }
            
            var message = await discordService.SendMessageAsync(theEvent.DiscordGuildId,
                theEvent.DiscordSignupChannelId,
                string.Empty, //\u200b
                component: discordButton.Build());

            theEvent.SignupMessageId = message.Id;
            dbContext.Update(theEvent);
            await dbContext.SaveChangesAsync();
        });
        
    }

    private async Task DeleteOldMessageAsync(Event theEvent)
    {
        try
        {
            await discordService.DeleteMessageAsync(theEvent.DiscordGuildId, theEvent.DiscordSignupChannelId,
                theEvent.SignupMessageId!.Value);
        }
        catch (HttpException e)
        {
            // Suppress "UnknownMessage" in case the old message was already deleted
            if (e.DiscordCode != DiscordErrorCode.UnknownMessage)
            {
                throw;
            }
        }
    }
}