using System.Security.Cryptography;
using System.Text.Json;
using BusinessLogic.Challonge;
using BusinessLogic.Discord;
using BusinessLogic.Exceptions;
using BusinessLogic.Extensions;
using BusinessLogic.Hitmaps.GroupStage;
using BusinessLogic.Models;
using BusinessLogic.SignalR;
using BusinessLogic.SignalR.RequestModels;
using DataAccess.Data;
using DataAccess.Models;
using DataAccess.Repositories;
using Discord;
using Microsoft.EntityFrameworkCore;
using MatchInfo = DataAccess.Models.MatchInfo;

namespace BusinessLogic.Hitmaps;

public class MatchupService : IMatchupService
{
    private readonly TournamentDbContext dbContext;
    private readonly Random random;
    private readonly IDiscordService discordService;
    private readonly IChallongeService challongeService;
    private readonly IEventService eventService;
    private readonly MatchupRepository matchupRepository;
    private readonly IMatchHubService matchHubService;

    public MatchupService(TournamentDbContext dbContext, 
        IDiscordService discordService, 
        IChallongeService challongeService,
        IEventService eventService, 
        MatchupRepository matchupRepository, 
        IMatchHubService matchHubService)
    {
        this.dbContext = dbContext;
        this.discordService = discordService;
        random = new Random();
        this.challongeService = challongeService;
        this.eventService = eventService;
        this.matchupRepository = matchupRepository;
        this.matchHubService = matchHubService;
    }

    public async Task<MatchInfo> ScheduleMatchAsync(int stageId, long matchId, ScheduleMatchRequestModel requestModel)
    {
        var matchInfo = await SaveMatchup(stageId, matchId, requestModel);
        await PostMatchupAsync(matchInfo);

        return matchInfo;
    }

    public void SetCasters(int stageId, long matchId, SetCastersRequestModel requestModel)
    {
        var matchInfo = dbContext.Matchups
            .Include(x => x.Casters)
            .First(x => x.Stage.Id == stageId && x.Id == matchId);

        //-- Clear cast info if either main cast detail is blank.
        if (requestModel.MainCastDiscordId == null || requestModel.MainCastStreamUrl == null)
        {
            matchInfo.Casters = new List<Caster>();
            
            dbContext.Update(matchInfo);
            dbContext.SaveChanges();
            return;
        }

        matchInfo.Casters = new List<Caster>
        {
            new()
            {
                Type = CasterType.Main,
                DiscordId = ulong.Parse(requestModel.MainCastDiscordId),
                StreamUrl = requestModel.MainCastStreamUrl
            }
        };
        requestModel.CocastDiscordIds.ForEach(x => matchInfo.Casters.Add(new Caster
        {
            DiscordId = ulong.Parse(x),
            Type = CasterType.Cocast
        }));

        dbContext.Update(matchInfo);
        dbContext.SaveChanges();
    }

    public List<MatchInfo> GetUpcomingMatches(string eventSlug)
    {
        var allMatches = dbContext.Matchups.Include(x => x.Stage)
            .ThenInclude(x => x.Bracket)
            .Include("Stage.Bracket.Event")
            .Include("Stage.PointsForVictories")
            .Include(x => x.Casters)
            .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
                .ThenInclude(x => x.Competitor)
            .Include(x => x.MapSelections.OrderBy(y => y.Id))
                .ThenInclude(x => x.Mission)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Competitor)
            .Where(x => x.Stage.Bracket.Event.Slug == eventSlug && x.State == MatchInfo.StateScheduled)
            .AsSplitQuery()
            .AsNoTracking()
            .ToList();

        var upcomingMatches = allMatches
            .Where(x => !x.MatchCompletedAt.HasValue && x.MatchScheduledAt > DateTime.UtcNow.AddHours(-3))
            .ToList();
        
        upcomingMatches.ForEach(match => match.CompetitorInfo = match.CompetitorInfo.OrderBy(x => x.Order).ToList());

        return upcomingMatches;
    }

    public void SetGameModeId(int matchId, Guid gameModeId)
    {
        var match = dbContext.Matchups.First(x => x.Id == matchId);

        if (match.GameModeMatchId.HasValue && gameModeId != match.GameModeMatchId.Value)
        {
            throw new InvalidOperationException($"Match {matchId} already has a game mode ID!");
        }

        match.GameModeMatchId = gameModeId;

        dbContext.Update(match);
        dbContext.SaveChanges();
    }

    public MatchInfo GetMatchupForMetadata(int matchId)
    {
        // @formatter:off
        return dbContext.Matchups
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Competitor)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
                .ThenInclude(x => x.Competitor)
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
                    .ThenInclude(x => x.Event)
            .First(x => x.Id == matchId);
        // @formatter:on
    }

    public MatchInfo? GetMatchupForHeartbeat(int matchId, string publicId)
    {
        // @formatter:off
        var matchInfo = dbContext.Matchups
            .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Competitor)
            .FirstOrDefault(x => x.Id == matchId);
        // @formatter:on

        if (matchInfo != null)
        {
            var competitor = matchInfo.CompetitorInfo.FirstOrDefault(x => x.PublicId == publicId);
            competitor.LastPing = DateTime.UtcNow;
            dbContext.Update(competitor);
            dbContext.SaveChanges();
        }

        return matchInfo;
    }

    public Task<MatchInfo> GetMatchupForMetadataAsync(int matchId)
    {
        // @formatter:off
        return dbContext.Matchups
            .AsSplitQuery()
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Competitor)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
                .ThenInclude(x => x.Competitor)
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
                    .ThenInclude(x => x.Event)
            .Include(x => x.Stage)
                .ThenInclude(x => x.PointsForVictories)
            .FirstAsync(x => x.Id == matchId);
        // @formatter:on
    }
    
    public Task<MatchInfo> GetMatchupForMatchHubAsync(int matchId)
    {
        // @formatter:off
        return dbContext.Matchups
            .AsSplitQuery()
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Competitor)
            .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
            .Include(x => x.CompetitorInfo.OrderBy(y => y.Order))
                .ThenInclude(x => x.Competitor)
            .Include(x => x.Stage)
                .ThenInclude(x => x.PointsForVictories)
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
                    .ThenInclude(x => x.Event)
            .FirstAsync(x => x.Id == matchId);
        // @formatter:on
    }

    private async Task<MatchInfo> SaveMatchup(int stageId, long matchId, ScheduleMatchRequestModel requestModel)
    {
        var stage = dbContext.Stages
            .Include(x => x.Bracket)
                .ThenInclude(x => x.Maps)
            .Include(x => x.Bracket)
                .ThenInclude(x => x.Event)
            .First(x => x.Id == stageId);
        // TODO Challonge
        //var challongeMatchInfo = await challongeService.FetchMatchAsync(bracket.ChallongeName!, matchId);
        var matchInfo =
            dbContext.Matchups
                .Include(x => x.CompetitorInfo)
                    .ThenInclude(x => x.Competitor)
                .Include(x => x.MapSelections)
                    .ThenInclude(x => x.Mission)
                .First(x => x.Id == matchId);
        var firstCompetitorInfo = matchInfo.CompetitorInfo.First(x => x.Order == 1);
        var secondCompetitorInfo = matchInfo.CompetitorInfo.First(x => x.Order == 2);
        firstCompetitorInfo.PublicId = GenerateRandomHexString();
        secondCompetitorInfo.PublicId = GenerateRandomHexString();
        var firstCompetitor = firstCompetitorInfo.Competitor;
        var secondCompetitor = secondCompetitorInfo.Competitor;
        
        matchInfo.MatchScheduledAt = requestModel.MatchScheduledAt;
        matchInfo.State = MatchInfo.StateScheduled;

        if (stage.Bracket.Maps.Any())
        {
            var randomMapPool = stage.Bracket.Maps.ToList();

            for (var i = 0; i < requestModel.MapSelections.Count; i++)
            {
                var newMap = requestModel.MapSelections[i];
                var existingSelection = false;
                MapSelection mapSelection;
                
                if (matchInfo.MapSelections.Count >= i + 1)
                {
                    mapSelection = matchInfo.MapSelections[i];
                    existingSelection = true;
                }
                else
                {
                    mapSelection = new MapSelection();
                    matchInfo.MapSelections.Add(mapSelection);
                }

                if (newMap.CompetitorId.HasValue)
                {
                    mapSelection.Competitor =
                        firstCompetitor.Id == newMap.CompetitorId ? firstCompetitor : secondCompetitor;                    
                }
                else
                {
                    mapSelection.Competitor = null;
                }
                
                if (existingSelection &&
                    IsFlagSet(Enum.Parse<MissionType>(newMap.SelectionType), MissionType.Random) &&
                    IsFlagSet(mapSelection.MissionType, MissionType.Random))
                {
                    mapSelection.MissionType = Enum.Parse<MissionType>(newMap.SelectionType);
                }
                else
                {
                    mapSelection.MissionType = Enum.Parse<MissionType>(newMap.SelectionType);

                    mapSelection.Mission = IsFlagSet(Enum.Parse<MissionType>(newMap.SelectionType), MissionType.Random) ? 
                        randomMapPool[random.Next(randomMapPool.Count)] : 
                        stage.Bracket.Maps.First(x => x.HitmapsSlug == newMap.MapSlug);

                    randomMapPool = randomMapPool.Where(x => x.HitmapsSlug != mapSelection.Mission.HitmapsSlug).ToList();
                }
                
                mapSelection.RoundType = stage.Bracket.Event.Type == "mixed" ? newMap.SelectionType : stage.Bracket.Event.Type;

                // ReSharper disable once InvertIf
                if (!existingSelection)
                {
                    var revealRandomMaps = true;
                    //var revealRandomMaps = JsonSerializer.Deserialize<StageSettings>(stage.SettingsJson)!.RevealRandomMaps;
                    
                    mapSelection.State = mapSelection.MissionType == MissionType.Ban ? MapSelectionState.Ban.Value : MapSelectionState.Ready.Value;
                    mapSelection.Revealed = mapSelection.MissionType != MissionType.Random || revealRandomMaps;
                }
            }
        }

        if (matchInfo.MatchScheduledAt > DateTime.UtcNow.AddMinutes(15))
        {
            matchInfo.FifteenMinuteWarningPosted = false;
        }

        dbContext.Update(matchInfo);
        await dbContext.SaveChangesAsync();

        return matchInfo;
    }
    
    private static bool IsFlagSet(MissionType type, MissionType expected)
    {
        return (type & expected) > 0;
    }

    private static string GenerateRandomHexString()
    {
        var value = RandomNumberGenerator.GetBytes(8);

        return string.Join("", value.Select(x => x.ToString("x2")));
    }

    private async Task PostMatchupAsync(MatchInfo matchInfo)
    {
        var theEvent = matchInfo.Stage.Bracket.Event;
        if (!theEvent.DiscordMatchupChannelId.HasValue)
        {
            return;
        }

        var firstCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 1).Competitor;
        var secondCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 2).Competitor;
        var round = await GetRoundForMatchAsync(matchInfo);
        var embed = new EmbedBuilder()
            .WithTitle(matchInfo.Stage.Bracket.Platform == "All"
                ? $"{theEvent.Name} - {round}"
                : $"{theEvent.Name} - {matchInfo.Stage.Bracket.Platform} {round}")
            .WithFooter("Best of luck, agents.")
            .AddField(":calendar_spiral: Date / Time", $"<t:{matchInfo.MatchScheduledAt!.Value.ToUnixTimestamp()}> (<t:{matchInfo.MatchScheduledAt!.Value.ToUnixTimestamp()}:R>)");

        if (matchInfo.Stage.Bracket.Maps.Any())
        {
            embed.AddField(":map: Picked Maps",
                FormatMapsForDiscordEmbed(matchInfo, MissionType.Pick | MissionType.Random));
            embed.AddField(":no_entry_sign: Banned Maps", FormatMapsForDiscordEmbed(matchInfo, MissionType.Ban));
        }

        embed.AddField(":tv: Stream URLs", $"{firstCompetitor.StreamUrl}\n{secondCompetitor.StreamUrl}");

        await discordService.SendMessageAsync(theEvent.DiscordGuildId, 
            theEvent.DiscordMatchupChannelId.Value,
            $"Match between <@{firstCompetitor.DiscordId}> and <@{secondCompetitor.DiscordId}> has been scheduled!", 
            embed.Build());
    }

    private async Task<string> GetRoundForMatchAsync(MatchInfo matchInfo)
    {
        if (matchInfo.GroupStage)
        {
            var groupStageCompetitor = dbContext.GroupStageMembers.First(x =>
                x.Stage.Id == matchInfo.Stage.Id && x.CompetitorId == matchInfo.CompetitorInfo.First().Competitor.Id);

            var groupLetter = (char)(groupStageCompetitor.GroupId + 65);
            
            return $"Group {groupLetter}";
        }

        if (matchInfo.Round.HasValue)
        {
            return $"Round {matchInfo.Round.Value}";
        }
        
        if (matchInfo.Stage.Bracket.ChallongeName == null || !matchInfo.Round.HasValue)
        {
            return string.Empty;
        }

        // TODO Challonge
        var challongeTournament = await challongeService.FetchTournamentAsync(matchInfo.Stage.Bracket.ChallongeName);
        var roundLabels = await challongeService.FetchRoundLabelsAsync(challongeTournament.Id);

        var actualRound = matchInfo.Round.Value;
        var roundLabel = roundLabels.FirstOrDefault(x => x.Number == actualRound);
        if (roundLabel != null)
        {
            return roundLabel.Title;
        }
        
        return actualRound < 0 ? 
            $"LB Round {Math.Abs(actualRound)}" : 
            $"WB Round {actualRound}";
    }

    private static string FormatMapsForDiscordEmbed(MatchInfo matchInfo, MissionType missionTypes)
    {
        var output = new List<string>();

        var maps = matchInfo.MapSelections.Where(x => IsFlagSet(x.MissionType, missionTypes));
        var hiddenRandomMapCount = 0;
        foreach (var map in maps)
        {
            var formattedCompetitor = string.Empty;

            if (map.Competitor != null)
            {
                formattedCompetitor = $" (<@!{map.Competitor.DiscordId}>'s Choice)";
            }

            if (map is { MissionType: MissionType.Random, Revealed: false })
            {
                hiddenRandomMapCount++;
            }
            else
            {
                output.Add($"{map.Mission.Location} - {map.Mission.Name}{formattedCompetitor}");                
            }
        }

        if (hiddenRandomMapCount > 0)
        {
            output.Add($"*+{hiddenRandomMapCount} hidden random maps*");
        }

        return output.Any() ? string.Join("\n", output) : "N/A";
    }

    public async Task VerifyMatchAuthorityAsync(int matchId, ulong userSnowflake)
    {
        var match = dbContext.Matchups.Include(x => x.Stage).ThenInclude(x => x.Bracket).First(x => x.Id == matchId);
        
        await eventService.AssertEventManagementPermissionByBracketIdAsync(match.Stage.Bracket.Id, userSnowflake);
    }

    public async Task SendScoreToChallongeAsync(int matchId, PostScoreRequestModel requestModel, ulong userSnowflake)
    {
        var match = dbContext.Matchups
            .Include(x => x.Stage)
                .ThenInclude(x => x.Bracket)
            .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Competitor)
            .First(x => x.Id == matchId);

        await eventService.AssertEventManagementPermissionByBracketIdAsync(match.Stage.Bracket.Id, userSnowflake);

        var scoreHistory = requestModel.ScoreHistory;
        long? winnerId = null;
        var player0Sum = scoreHistory.Sum(x => x.Score0);
        var player1Sum = scoreHistory.Sum(x => x.Score1);
        if (player0Sum != player1Sum)
        {
            winnerId = player0Sum > player1Sum
                ? match.CompetitorInfo.First(x => x.Order == 1).Competitor.ChallongePlayerId
                : match.CompetitorInfo.First(x => x.Order == 2).Competitor.ChallongePlayerId;

            if (match.GroupStage)
            {
                var challongeMatch =
                    await challongeService.FetchMatchAsync(match.Stage.Bracket.ChallongeName!, match.ChallongeMatchId!.Value);

                winnerId = player0Sum > player1Sum ? challongeMatch.Player1Id!.Value : challongeMatch.Player2Id!.Value;
            }
        }

        await challongeService.SendScoreAsync(match.Stage.Bracket.ChallongeName!, match.ChallongeMatchId!.Value,
            scoreHistory, winnerId);

        match.MatchCompletedAt = DateTime.UtcNow;
        dbContext.Update(match);
        await dbContext.SaveChangesAsync();
    }

    public void SetMatchAdmin(int stageId, long matchId, SetMatchAdminRequestModel requestModel)
    {
        var matchInfo = dbContext.Matchups.First(x => x.Stage.Id == stageId && x.Id == matchId);

        matchInfo.MatchAdminDiscordId = requestModel.MatchAdminDiscordId == null
            ? null
            : ulong.Parse(requestModel.MatchAdminDiscordId);

        dbContext.Update(matchInfo);
        dbContext.SaveChanges();
    }

    public async Task SetMatchState(int stageId, long matchId, MatchStateModel matchState)
    {
        var matchInfo =
            dbContext.Matchups
                .Include(x => x.Stage)
                    .ThenInclude(x => x.Bracket)
                        .ThenInclude(x => x.Event)
                .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Competitor)
                .Include(x => x.MapSelections)
                .ThenInclude(x => x.Mission)
                .FirstOrDefault(x => x.Id == matchId);
        
        if (matchInfo == null || matchInfo.Stage.Id != stageId)
        {
            throw new Exception($"Match {matchId} not found");
        }

        var changing = matchInfo.State != matchState.State;

        if (changing)
        {
            matchInfo.State = matchState.State;          
            dbContext.Update(matchInfo);
            await dbContext.SaveChangesAsync();

            await PostUpdatedMatchStateMessageAsync(matchInfo);
        }
    }

    public async Task ResetMatchAsync(int stageId, long matchId)
    {
        var matchInfo = dbContext.Matchups
            .FirstOrDefault(x => x.Id == matchId && x.Stage.Id == stageId);
        
        if (matchInfo == null)
        {
            throw new Exception($"Match {matchId} not found");
        }

        await matchupRepository.ResetMatchupAsync(matchInfo.Id);
    }

    private async Task PostUpdatedMatchStateMessageAsync(MatchInfo matchInfo)
    {
        var theEvent = matchInfo.Stage.Bracket.Event;
        if (!theEvent.DiscordMatchupChannelId.HasValue)
        {
            return;
        }
        
        var firstCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 1).Competitor;
        var secondCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 2).Competitor;
        var embedMessage = matchInfo.State == MatchInfo.StatePostponed ? 
            $":warning: Match between <@{firstCompetitor.DiscordId}> and <@{secondCompetitor.DiscordId}> has been postponed at this time." : 
            $":white_check_mark: Match between <@{firstCompetitor.DiscordId}> and <@{secondCompetitor.DiscordId}> is no longer postponed.";
        var embedTitle = matchInfo.State == MatchInfo.StatePostponed ? 
            "Match Postponed" : 
            "Match No Longer Postponed";
        var embed = new EmbedBuilder()
            .WithTitle(embedTitle)
            .WithDescription(embedMessage);
        
        await discordService.SendMessageAsync(theEvent.DiscordGuildId,
            theEvent.DiscordMatchupChannelId.Value,
            embed: embed.Build());
    }

    public Message? GetPendingMessageForCompetitor(int matchId, string competitorPublicId)
    {
        // @formatter:off
        var matchInfo = dbContext.Matchups
            .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Messages)
            .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Competitor)
            .First(x => x.Id == matchId);
        // @formatter:on
        
        return matchInfo.CompetitorInfo.First(x => x.PublicId == competitorPublicId).Messages
            .FirstOrDefault(x => x.AcknowledgedAt == null);
    }

    public Message UpdateMessageStateForCompetitor(int matchId, string competitorPublicId, string state)
    {
        // @formatter:off
        var matchInfo = dbContext.Matchups
            .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Messages)
            .Include(x => x.CompetitorInfo)
                .ThenInclude(x => x.Competitor)
            .First(x => x.Id == matchId);
        // @formatter:on

        var message = matchInfo.CompetitorInfo.First(x => x.PublicId == competitorPublicId).Messages.First(x => x.AcknowledgedAt == null);
        if (state == "Receive")
        {
            message.ReceivedAt = DateTime.UtcNow;            
        }
        else if (state == "Acknowledge")
        {
            message.AcknowledgedAt = DateTime.UtcNow;
        }
        else
        {
            throw new Exception($"Invalid state: {state}");
        }
        dbContext.Update(message);
        dbContext.SaveChanges();

        return message;
    }

    public async Task MarkPlayerAsCompleteAsync(int matchId, CompetitorNotifyCompletionRequestModel requestModel, DateTime completionTime)
    {
        var matchInfo = await GetMatchupForMetadataAsync(matchId);

        if (matchInfo.TimerFingerprint!.Value != requestModel.TimerFingerprint)
        {
            throw new ConflictException<Guid>(matchInfo.TimerFingerprint.Value, requestModel.TimerFingerprint);
        }

        var competitor = matchInfo.CompetitorInfo.First(x => x.PublicId == requestModel.PublicId);
        if (competitor.CompleteTime.HasValue)
        {
            //-- Don't replace their finish time
            return;
        }
        
        competitor.CompleteTime = completionTime;
        dbContext.Update(competitor);
        await dbContext.SaveChangesAsync();
        
        await matchHubService.SendUpdatedMatchupAsync(matchId, matchInfo);
    }
}