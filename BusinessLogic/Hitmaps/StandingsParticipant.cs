namespace BusinessLogic.Hitmaps;

public class StandingsParticipant
{
    public int CompetitorId { get; set; }
    public string Name { get; set; } = null!;
    public int Points { get; set; }
    public int TheoreticalMaxPoints { get; set; }
    public string Group { get; set; } = null!;
    public int GroupId { get; set; }
    public int Wins { get; set; }
    public int Losses { get; set; }
    public int Ties { get; set; }
    public double WinningPercentage { get; set; }
    public bool Advanced { get; set; }
    public bool Eliminated { get; set; }
}