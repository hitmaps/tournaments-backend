using System.Net.Http.Json;
using BusinessLogic.Hitmaps;
using DataAccess.Data;
using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BusinessLogic.BracketManager;

public class BracketManagerService : IBracketManagerService
{
    private readonly string _bracketManagerUrl;
    private readonly HttpClientWrapper _httpClientWrapper;
    private readonly TournamentDbContext _dbContext;

    public BracketManagerService(IConfiguration configuration, HttpClientWrapper httpClientWrapper, TournamentDbContext dbContext)
    {
        _httpClientWrapper = httpClientWrapper;
        _dbContext = dbContext;
        _bracketManagerUrl = configuration["BracketManagerUrl"];
    }

    public async Task FinalizeBracketMatchAsync(MatchInfo matchInfo)
    {
        var firstCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 1).Competitor.Id;
        var secondCompetitor = matchInfo.CompetitorInfo.First(x => x.Order == 2).Competitor.Id;
        var firstCompetitorWins = matchInfo.MapSelections.Count(x => x.Winner?.Id == firstCompetitor && x.State == MapSelectionState.Complete.Value);
        var secondCompetitorWins = matchInfo.MapSelections.Count(x => x.Winner?.Id == secondCompetitor && x.State == MapSelectionState.Complete.Value);
        var draws = matchInfo.MapSelections.Count(x => x.Winner == null && x.State == MapSelectionState.Complete.Value);

        var firstCompetitorScore = firstCompetitorWins * 2 + draws;
        var secondCompetitorScore = secondCompetitorWins * 2 + draws;
        
        using var request = new HttpRequestMessage(HttpMethod.Patch, $"{_bracketManagerUrl}/match");
        request.Content = JsonContent.Create(new FinalizeMatchRequestModel
        {
            Id = (int)matchInfo.ChallongeMatchId!.Value,
            OpponentOne = new BracketParticipantResult
            {
                Score = firstCompetitorScore,
                Result = firstCompetitorScore > secondCompetitorScore ? BracketParticipantResult.ResultWin : BracketParticipantResult.ResultLoss
            },
            OpponentTwo = new BracketParticipantResult
            {
                Score = secondCompetitorScore,
                Result = secondCompetitorScore > firstCompetitorScore ? BracketParticipantResult.ResultWin : BracketParticipantResult.ResultLoss
            }
        });
        
        var result = await _httpClientWrapper.GetClient().SendAsync(request);
        if (!result.IsSuccessStatusCode)
        {
            throw new Exception($"Unable to get result: {result.StatusCode}");
        }
    }
}