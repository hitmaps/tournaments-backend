using System.Text.Json.Serialization;

namespace BusinessLogic.BracketManager;

public class FinalizeMatchRequestModel
{
    /// <summary>
    /// The bracket manager's ID; not the HITMAPS MatchInfo.Id
    /// </summary>
    public int Id { get; set; }
    public BracketParticipantResult OpponentOne { get; set; } = null!;
    public BracketParticipantResult OpponentTwo { get; set; } = null!;
}

public class BracketParticipantResult
{
    public const string ResultWin = "win";
    public const string ResultLoss = "loss";
    public const string ResultDraw = "draw";
    
    public int Score { get; set; }
    public string Result { get; set; } = null!;
}