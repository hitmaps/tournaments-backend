using System.Text.Json.Serialization;

namespace BusinessLogic.BracketManager;

public class Bracket
{
    public List<BracketStage> Stage { get; set; } = new();
    public List<BracketGroup> Group { get; set; } = new();
    public List<BracketRound> Round { get; set; } = new();
    public List<BracketMatch> Match { get; set; } = new();
    [JsonPropertyName("match_game")]
    public List<object> MatchGame { get; set; } = new();
    public List<BracketParticipant> Participant { get; set; } = new();
}

#region Stage
public class BracketStage
{
    public int Id { get; set; }
    [JsonPropertyName("tournament_id")]
    public int TournamentId { get; set; }

    public string Name { get; set; } = null!;
    public string Type { get; set; } = null!;
    public BracketStageSettings Settings { get; set; } = null!;
}

public class BracketStageSettings
{
    public List<string> SeedOrdering { get; set; } = new();
    public int Size { get; set; }
    public string GrandFinal { get; set; } = null!;
    public bool ConsolationFinal { get; set; }
    public int MatchesChildCount { get; set; }
}
#endregion
#region Group
public class BracketGroup
{
    public int Id { get; set; }
    [JsonPropertyName("stage_id")]
    public int StageId { get; set; }
    public int Number { get; set; }
}
#endregion
#region Round
public class BracketRound
{
    public int Id { get; set; }
    public int Number { get; set; }
    [JsonPropertyName("stage_id")]
    public int StageId { get; set; }
    [JsonPropertyName("group_id")]
    public int GroupId { get; set; }
}
#endregion
#region Match
public class BracketMatch
{
    public int Id { get; set; }
    public int Number { get; set; }
    [JsonPropertyName("stage_id")]
    public int StageId { get; set; }
    [JsonPropertyName("group_id")]
    public int GroupId { get; set; }
    [JsonPropertyName("round_id")]
    public int RoundId { get; set; }
    [JsonPropertyName("child_count")]
    public int ChildCount { get; set; }
    public int Status { get; set; }
    [JsonPropertyName("opponent1")]
    public BracketMatchOpponent OpponentOne { get; set; } = null!;
    [JsonPropertyName("opponent2")]
    public BracketMatchOpponent OpponentTwo { get; set; } = null!;
}

public class BracketMatchOpponent
{
    public int? Id { get; set; }
    public int? Position { get; set; }
    public int? Score { get; set; }
    public string? Result { get; set; }
}
#endregion
#region Participant
public class BracketParticipant
{
    public int Id { get; set; }
    [JsonPropertyName("tournament_id")]
    public int TournamentId { get; set; }
    public string Name { get; set; } = null!;
}
#endregion