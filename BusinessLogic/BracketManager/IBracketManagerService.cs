using DataAccess.Models;

namespace BusinessLogic.BracketManager;

public interface IBracketManagerService
{
    Task FinalizeBracketMatchAsync(MatchInfo matchInfo);
}