namespace BusinessLogic.BracketManager;

public class BracketPreviewRequestModel
{
    public int TournamentId { get; set; }
    public string Name => "TEMP";
    public string Type { get; set; } = null!;
    public List<string> Seeding { get; set; } = new();
    public BracketSettings Settings { get; set; } = null!;
}

public class BracketSettings
{
    public List<string>? SeedOrdering { get; set; }
    public int? Size { get; set; }
    public string? GrandFinal { get; set; }
}