using DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Data;

public class TournamentDbContext : DbContext
{
    public TournamentDbContext(DbContextOptions<TournamentDbContext> options) : base(options)
    {
        
    }

    public DbSet<Event> Events { get; set; }
    public DbSet<Bracket> Brackets { get; set; }
    public DbSet<Stage> Stages { get; set; }
    public DbSet<Competitor> Competitors { get; set; }
    public DbSet<MatchInfo> Matchups { get; set; }
    public DbSet<Caster> Casters { get; set; }
    public DbSet<GroupStageMember> GroupStageMembers { get; set; }
    public DbSet<RouletteObjective> RouletteObjectives { get; set; }
    public DbSet<StageCompetitorSeeding> StageCompetitorSeedings { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Event>().ToTable("Event")
            .HasIndex(x => x.Slug)
            .IsUnique();
        modelBuilder.Entity<Bracket>().ToTable("Bracket")
            .HasIndex(x => x.ChallongeName)
            .IsUnique();
        modelBuilder.Entity<Stage>().ToTable("Stage");
        modelBuilder.Entity<Competitor>().ToTable("Competitor")
            .HasIndex(x => x.DiscordId);

        modelBuilder.Entity<GroupStageMember>().ToTable("Member", "GroupStage");
        modelBuilder.Entity<MatchInfo>().ToTable("MatchInfo")
            .HasIndex(x => x.ChallongeMatchId);
        modelBuilder.Entity<MatchInfo>()
            .HasIndex(x => x.GameModeMatchId);
        modelBuilder.Entity<Caster>().ToTable("Caster");

        modelBuilder.Entity<MapSelection>().ToTable("MapSelection");
        modelBuilder.Entity<MapSelection>()
            .HasOne(x => x.MatchInfo)
            .WithMany(x => x.MapSelections)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<MissionForPool>().ToTable("MissionForPool");
        modelBuilder.Entity<MapSelection>()
            .HasOne(x => x.Mission)
            .WithMany(x => x.MapSelections)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<MatchCompetitorInfo>()
            .HasOne(x => x.Match)
            .WithMany(x => x.CompetitorInfo)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<RouletteObjective>().ToTable("RouletteObjective");
        modelBuilder.Entity<StageCompetitorSeeding>().ToTable("StageCompetitorSeeding");
    }
}