namespace DataAccess.Models;

public class Message
{
    public int Id { get; set; }
    public MatchCompetitorInfo CompetitorInfo { get; set; }
    public DateTime SentAt { get; set; }
    public string Content { get; set; } = null!;
    public DateTime? ReceivedAt { get; set; }
    public DateTime? AcknowledgedAt { get; set; }
}