using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace DataAccess.Models;

public class Event
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public string TileUrl { get; set; } = null!;
    public string Type { get; set; } = null!;
    public string Slug { get; set; } = null!;
    public DateTime RegistrationOpensAt { get; set; }
    public DateTime RegistrationEndsAt { get; set; }
    public DateTime EventEndsAt { get; set; }
    public string Rules { get; set; } = null!;
    public string OverlayBackgroundUrl { get; set; } = null!;
    public string OverlayLogoUrl { get; set; } = null!;
    public ulong ParticipantDiscordRoleId { get; set; }
    public ulong DiscordGuildId { get; set; }
    public ulong? DiscordMatchupChannelId { get; set; }
    public ulong DiscordSignupChannelId { get; set; }
    public ulong? DiscordBannedCompetitorRoleId { get; set; }
    public ulong? SignupMessageId { get; set; }
    public bool UsesChallonge { get; set; }
    
    [MaxLength(20)]
    public string State { get; set; } = null!;
    public List<EventAdminRole> DiscordAdminRoleIds { get; set; } = new();
    public List<Bracket> Brackets { get; set; } = new();
    public List<EventCasterRole> DiscordCasterRoleIds { get; set; } = new();
}