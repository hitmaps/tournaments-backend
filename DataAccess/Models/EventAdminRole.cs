namespace DataAccess.Models;

public class EventAdminRole
{
    public int Id { get; set; }
    public Event Event { get; set; } = null!;
    public string DiscordRoleId { get; set; } = null!;
}