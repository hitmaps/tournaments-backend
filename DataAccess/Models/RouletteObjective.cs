namespace DataAccess.Models;

public class RouletteObjective
{
    public int Id { get; set; }
    public int MatchInfoId { get; set; }
    public int ObjectiveIndex { get; set; }
    public int CompetitorIndex { get; set; }
    public bool Completed { get; set; }
}