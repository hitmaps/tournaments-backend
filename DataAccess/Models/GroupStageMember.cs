namespace DataAccess.Models;

public class GroupStageMember
{
    public int Id { get; set; }
    public Stage Stage { get; set; } = null!;
    public int GroupId { get; set; }
    // Intentionally not bothering with database mappings so EF doesn't get overly confused
    public int CompetitorId { get; set; }
}