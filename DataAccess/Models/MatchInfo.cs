namespace DataAccess.Models;

public class MatchInfo
{
    public const string StateNeedsScheduling = "Needs Scheduling";
    public const string StateScheduled = "Scheduled";
    public const string StatePostponed = "Postponed";
    public const string StateComplete = "Complete";
    
    public int Id { get; set; }
    public Stage Stage { get; set; } = null!;
    public long? ChallongeMatchId { get; set; }
    public DateTime? MatchScheduledAt { get; set; }
    public DateTime? MatchCompletedAt { get; set; }
    public List<MatchCompetitorInfo> CompetitorInfo { get; set; } = new();
    public List<MapSelection> MapSelections { get; set; } = new();
    public Guid? GameModeMatchId { get; set; }
    public int? Round { get; set; }
    public List<Caster> Casters { get; set; } = new();
    public bool FifteenMinuteWarningPosted { get; set; }
    public ulong? MatchAdminDiscordId { get; set; }
    public bool GroupStage { get; set; }
    public string State { get; set; } = null!;
    //region Mid-Match
    public Guid? TimerFingerprint { get; set; }
    public bool TimerPaused { get; set; }
    public int? TimerPausedRemainingTimeInSeconds { get; set; }
    public DateTime? TimerStartsAt { get; set; }
    public DateTime? TimerEndsAt { get; set; }
    //endregion
}