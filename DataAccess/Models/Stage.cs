using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models;

public class Stage
{
    public int Id { get; set; }
    public Bracket Bracket { get; set; } = null!;
    [MaxLength(255)]
    public string Name { get; set; } = null!;
    public string Type { get; set; } = null!;
    [MaxLength(20)]
    public string State { get; set; } = null!;
    public string SettingsJson { get; set; } = null!;
    public List<PointsForVictory> PointsForVictories { get; set; } = new();
    public List<MatchInfo> ScheduledMatches { get; set; } = new();

    protected bool Equals(Stage other)
    {
        return Id == other.Id;
    }

    public override bool Equals(object? obj)
    {
        if (obj is null) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((Stage)obj);
    }

    public override int GetHashCode()
    {
        return Id;
    }

    public bool IsBracketStage()
    {
        return new List<string> { "single-elimination", "double-elimination" }.Contains(Type);
    }
}