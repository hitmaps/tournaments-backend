using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models;

public class MatchCompetitorInfo
{
    public int Id { get; set; }
    public MatchInfo Match { get; set; } = null!;
    public Competitor Competitor { get; set; } = null!;
    public int Order { get; set; }
    public DateTime? LastPing { get; set; }
    public DateTime? CompleteTime { get; set; }
    public bool Forfeit { get; set; }
    [MaxLength(16)]
    public string PublicId { get; set; } = "";
    public List<Message> Messages { get; set; } = new();
}