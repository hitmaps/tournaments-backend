namespace DataAccess.Models;

public class StageCompetitorSeeding
{
    public int Id { get; set; }
    // Intentionally ignoring EF due to FK cascade issues
    public int StageId { get; set; }
    public int CompetitorId { get; set; }
    public int Seeding { get; set; }
}