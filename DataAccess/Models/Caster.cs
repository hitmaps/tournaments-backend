namespace DataAccess.Models;

public class Caster
{
    public int Id { get; set; }
    public MatchInfo MatchInfo { get; set; } = null!;
    public ulong DiscordId { get; set; }
    public string? StreamUrl { get; set; }
    public CasterType Type { get; set; }
}