namespace DataAccess.Models;

public class Bracket
{
    public int Id { get; set; }
    public Event Event { get; set; } = null!;
    public string? ChallongeName { get; set; }
    public int ParticipantLimit { get; set; }
    public string Platform { get; set; } = null!;
    public string PlatformIcons { get; set; } = null!;
    public List<Stage> Stages { get; set; } = new();
    public List<MissionForPool> Maps { get; set; } = new();
    public List<Competitor> Competitors { get; set; } = new();
}