using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models;

public class MapSelection
{
    public int Id { get; set; }
    public MatchInfo MatchInfo { get; set; } = null!;
    public Competitor? Competitor { get; set; }
    public MissionForPool Mission { get; set; } = null!;
    public MissionType MissionType { get; set; }
    [MaxLength(255)]
    public string RoundType { get; set; } = null!;
    [MaxLength(20)]
    public string State { get; set; } = null!;
    public Competitor? Winner { get; set; }
    public string? MapJson { get; set; }
    public DateTime? MapStartedAt { get; set; }
    public DateTime? ResultVerifiedAt { get; set; }
    public DateTime? WinnerFinishedAt { get; set; }
    public bool Revealed { get; set; }
    public ulong? MapCompleteMessageId { get; set; }
}