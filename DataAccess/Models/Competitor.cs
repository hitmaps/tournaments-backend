namespace DataAccess.Models;

public class Competitor
{
    public int Id { get; set; }
    public Bracket Bracket { get; set; } = null!;
    public long? ChallongePlayerId { get; set; }
    public ulong DiscordId { get; set; }
    public string StreamUrl { get; set; } = null!;
    public string? ProfileName { get; set; }
    public string? ChallongeName { get; set; }
    public string? CountryCode { get; set; }
    public string TimeZone { get; set; } = null!;
    public List<MatchCompetitorInfo> ScheduledMatches { get; set; } = new();
}