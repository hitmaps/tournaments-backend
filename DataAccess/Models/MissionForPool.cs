namespace DataAccess.Models;

public class MissionForPool
{
    public int Id { get; set; }
    public Bracket Bracket { get; set; } = null!;
    public string HitmapsSlug { get; set; } = null!;
    public string Location { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string Game { get; set; } = null!;
    public MissionType Type { get; set; }
    public List<MapSelection> MapSelections { get; set; } = new();
}