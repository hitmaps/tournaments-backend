namespace DataAccess.Models;

public class PointsForVictory
{
    public int Id { get; set; }
    public Stage Stage { get; set; } = null!;
    public int Round { get; set; }
    public int Points { get; set; }
}