namespace DataAccess.Models;

[Flags]
public enum MissionType
{
    Pick = 1,
    Ban = 2,
    Random = 4
}