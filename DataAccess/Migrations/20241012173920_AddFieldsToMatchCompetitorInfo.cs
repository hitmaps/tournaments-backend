﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddFieldsToMatchCompetitorInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastPing",
                table: "MatchCompetitorInfo",
                type: "datetime2",
                nullable: true);
            
            migrationBuilder.AddColumn<DateTime>(
                name: "CompleteTime",
                table: "MatchCompetitorInfo",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Forfeit",
                table: "MatchCompetitorInfo",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompleteTime",
                table: "MatchCompetitorInfo");

            migrationBuilder.DropColumn(
                name: "Forfeit",
                table: "MatchCompetitorInfo");

            migrationBuilder.DropColumn(
                name: "LastPing",
                table: "MatchCompetitorInfo");
        }
    }
}
