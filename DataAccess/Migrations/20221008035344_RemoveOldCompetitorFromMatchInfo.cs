﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class RemoveOldCompetitorFromMatchInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompetitorMatchInfo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CompetitorMatchInfo",
                columns: table => new
                {
                    CompetitorsId = table.Column<int>(type: "int", nullable: false),
                    ScheduledMatchesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitorMatchInfo", x => new { x.CompetitorsId, x.ScheduledMatchesId });
                    table.ForeignKey(
                        name: "FK_CompetitorMatchInfo_Competitor_CompetitorsId",
                        column: x => x.CompetitorsId,
                        principalTable: "Competitor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompetitorMatchInfo_MatchInfo_ScheduledMatchesId",
                        column: x => x.ScheduledMatchesId,
                        principalTable: "MatchInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompetitorMatchInfo_ScheduledMatchesId",
                table: "CompetitorMatchInfo",
                column: "ScheduledMatchesId");
        }
    }
}
