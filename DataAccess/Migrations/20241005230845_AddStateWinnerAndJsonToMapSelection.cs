﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddStateWinnerAndJsonToMapSelection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MapJson",
                table: "MapSelection",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "MapSelection",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "WinnerId",
                table: "MapSelection",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MapSelection_WinnerId",
                table: "MapSelection",
                column: "WinnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_MapSelection_Competitor_WinnerId",
                table: "MapSelection",
                column: "WinnerId",
                principalTable: "Competitor",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MapSelection_Competitor_WinnerId",
                table: "MapSelection");

            migrationBuilder.DropIndex(
                name: "IX_MapSelection_WinnerId",
                table: "MapSelection");

            migrationBuilder.DropColumn(
                name: "MapJson",
                table: "MapSelection");

            migrationBuilder.DropColumn(
                name: "State",
                table: "MapSelection");

            migrationBuilder.DropColumn(
                name: "WinnerId",
                table: "MapSelection");
        }
    }
}
