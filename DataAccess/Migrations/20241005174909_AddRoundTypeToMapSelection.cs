﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddRoundTypeToMapSelection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RoundType",
                table: "MapSelection",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");
            
            migrationBuilder.Sql("UPDATE MapSelection SET RoundType = 'roulette-rivals'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RoundType",
                table: "MapSelection");
        }
    }
}
