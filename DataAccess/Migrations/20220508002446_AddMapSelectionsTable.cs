﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddMapSelectionsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MapSelection",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MatchInfoId = table.Column<int>(type: "int", nullable: false),
                    CompetitorId = table.Column<int>(type: "int", nullable: false),
                    MissionId = table.Column<int>(type: "int", nullable: false),
                    MissionType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MapSelection", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MapSelection_Competitor_CompetitorId",
                        column: x => x.CompetitorId,
                        principalTable: "Competitor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MapSelection_MatchInfo_MatchInfoId",
                        column: x => x.MatchInfoId,
                        principalTable: "MatchInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MapSelection_MissionForPool_MissionId",
                        column: x => x.MissionId,
                        principalTable: "MissionForPool",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MapSelection_CompetitorId",
                table: "MapSelection",
                column: "CompetitorId");

            migrationBuilder.CreateIndex(
                name: "IX_MapSelection_MatchInfoId",
                table: "MapSelection",
                column: "MatchInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_MapSelection_MissionId",
                table: "MapSelection",
                column: "MissionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MapSelection");
        }
    }
}
