﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddCompetitorInfoToMatchInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MatchCompetitorInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MatchId = table.Column<int>(type: "int", nullable: false),
                    CompetitorId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchCompetitorInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MatchCompetitorInfo_Competitor_CompetitorId",
                        column: x => x.CompetitorId,
                        principalTable: "Competitor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MatchCompetitorInfo_MatchInfo_MatchId",
                        column: x => x.MatchId,
                        principalTable: "MatchInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MatchCompetitorInfo_CompetitorId",
                table: "MatchCompetitorInfo",
                column: "CompetitorId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchCompetitorInfo_MatchId",
                table: "MatchCompetitorInfo",
                column: "MatchId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MatchCompetitorInfo");
        }
    }
}
