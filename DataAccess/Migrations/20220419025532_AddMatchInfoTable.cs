﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddMatchInfoTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RegistrationOpens",
                table: "Event",
                newName: "RegistrationOpensAt");

            migrationBuilder.RenameColumn(
                name: "RegistrationEnds",
                table: "Event",
                newName: "RegistrationEndsAt");

            migrationBuilder.RenameColumn(
                name: "EventEnds",
                table: "Event",
                newName: "EventEndsAt");

            migrationBuilder.CreateTable(
                name: "MatchInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BracketId = table.Column<int>(type: "int", nullable: false),
                    ChallongeMatchId = table.Column<int>(type: "int", nullable: false),
                    MatchCompletedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    GameModeMatchId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MatchInfo_Bracket_BracketId",
                        column: x => x.BracketId,
                        principalTable: "Bracket",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompetitorMatchInfo",
                columns: table => new
                {
                    CompetitorsId = table.Column<int>(type: "int", nullable: false),
                    ScheduledMatchesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitorMatchInfo", x => new { x.CompetitorsId, x.ScheduledMatchesId });
                    table.ForeignKey(
                        name: "FK_CompetitorMatchInfo_Competitor_CompetitorsId",
                        column: x => x.CompetitorsId,
                        principalTable: "Competitor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompetitorMatchInfo_MatchInfo_ScheduledMatchesId",
                        column: x => x.ScheduledMatchesId,
                        principalTable: "MatchInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompetitorMatchInfo_ScheduledMatchesId",
                table: "CompetitorMatchInfo",
                column: "ScheduledMatchesId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchInfo_BracketId",
                table: "MatchInfo",
                column: "BracketId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchInfo_ChallongeMatchId",
                table: "MatchInfo",
                column: "ChallongeMatchId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchInfo_GameModeMatchId",
                table: "MatchInfo",
                column: "GameModeMatchId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompetitorMatchInfo");

            migrationBuilder.DropTable(
                name: "MatchInfo");

            migrationBuilder.RenameColumn(
                name: "RegistrationOpensAt",
                table: "Event",
                newName: "RegistrationOpens");

            migrationBuilder.RenameColumn(
                name: "RegistrationEndsAt",
                table: "Event",
                newName: "RegistrationEnds");

            migrationBuilder.RenameColumn(
                name: "EventEndsAt",
                table: "Event",
                newName: "EventEnds");
        }
    }
}
