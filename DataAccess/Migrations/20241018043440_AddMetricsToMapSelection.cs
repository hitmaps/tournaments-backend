﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddMetricsToMapSelection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "MapStartedAt",
                table: "MapSelection",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ResultVerifiedAt",
                table: "MapSelection",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "WinnerFinishedAt",
                table: "MapSelection",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MapStartedAt",
                table: "MapSelection");

            migrationBuilder.DropColumn(
                name: "ResultVerifiedAt",
                table: "MapSelection");

            migrationBuilder.DropColumn(
                name: "WinnerFinishedAt",
                table: "MapSelection");
        }
    }
}
