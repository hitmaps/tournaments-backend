﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class UpdateTableDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "DiscordId",
                table: "Competitor",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "ChallongeName",
                table: "Bracket",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Competitor_DiscordId",
                table: "Competitor",
                column: "DiscordId");

            migrationBuilder.CreateIndex(
                name: "IX_Bracket_ChallongeName",
                table: "Bracket",
                column: "ChallongeName",
                unique: true,
                filter: "[ChallongeName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Competitor_DiscordId",
                table: "Competitor");

            migrationBuilder.DropIndex(
                name: "IX_Bracket_ChallongeName",
                table: "Bracket");

            migrationBuilder.AlterColumn<string>(
                name: "DiscordId",
                table: "Competitor",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<string>(
                name: "ChallongeName",
                table: "Bracket",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);
        }
    }
}
