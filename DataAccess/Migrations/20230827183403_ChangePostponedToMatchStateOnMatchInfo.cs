﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class ChangePostponedToMatchStateOnMatchInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Postponed",
                table: "MatchInfo");

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "MatchInfo",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "State",
                table: "MatchInfo");

            migrationBuilder.AddColumn<bool>(
                name: "Postponed",
                table: "MatchInfo",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
