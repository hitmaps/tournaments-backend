﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddStateToStage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Stage",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: false,
                defaultValue: "");
            
            migrationBuilder.Sql("UPDATE dbo.Stage SET State = 'Complete'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "State",
                table: "Stage");
        }
    }
}
