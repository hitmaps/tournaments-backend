﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddFieldsToMatchInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TimerFingerprint",
                table: "MatchInfo",
                type: "uniqueidentifier",
                nullable: true);
            
            migrationBuilder.AddColumn<bool>(
                name: "TimerPaused",
                table: "MatchInfo",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "TimerPausedRemainingTimeInSeconds",
                table: "MatchInfo",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TimerStartsAt",
                table: "MatchInfo",
                type: "datetime2",
                nullable: true);
            
            migrationBuilder.AddColumn<DateTime>(
                name: "TimerEndsAt",
                table: "MatchInfo",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimerEndsAt",
                table: "MatchInfo");

            migrationBuilder.DropColumn(
                name: "TimerFingerprint",
                table: "MatchInfo");

            migrationBuilder.DropColumn(
                name: "TimerPaused",
                table: "MatchInfo");

            migrationBuilder.DropColumn(
                name: "TimerPausedRemainingTimeInSeconds",
                table: "MatchInfo");

            migrationBuilder.DropColumn(
                name: "TimerStartsAt",
                table: "MatchInfo");
        }
    }
}
