﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class MoveMatchesAndPointsForVictoryToStages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MatchInfo_Bracket_BracketId",
                table: "MatchInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_PointsForVictory_Bracket_BracketId",
                table: "PointsForVictory");

            migrationBuilder.RenameColumn(
                name: "BracketId",
                table: "PointsForVictory",
                newName: "StageId");

            migrationBuilder.RenameIndex(
                name: "IX_PointsForVictory_BracketId",
                table: "PointsForVictory",
                newName: "IX_PointsForVictory_StageId");

            migrationBuilder.RenameColumn(
                name: "BracketId",
                table: "MatchInfo",
                newName: "StageId");

            migrationBuilder.RenameIndex(
                name: "IX_MatchInfo_BracketId",
                table: "MatchInfo",
                newName: "IX_MatchInfo_StageId");
            
            //-- These columns still point to the old bracket IDs
            migrationBuilder.Sql(@"UPDATE PointsForVictory
                SET PointsForVictory.StageId = Stage.Id
                FROM dbo.PointsForVictory AS PointsForVictory
                INNER JOIN dbo.Bracket
                    ON PointsForVictory.StageId = Bracket.Id
                INNER JOIN dbo.Stage
                    ON Stage.BracketId = Bracket.Id");
            migrationBuilder.Sql(@"UPDATE MatchInfo
                SET MatchInfo.StageId = Stage.Id
                FROM dbo.MatchInfo AS MatchInfo
                INNER JOIN dbo.Bracket
                    ON MatchInfo.StageId = Bracket.Id
                INNER JOIN dbo.Stage
                    ON Stage.BracketId = Bracket.Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MatchInfo_Stage_StageId",
                table: "MatchInfo",
                column: "StageId",
                principalTable: "Stage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PointsForVictory_Stage_StageId",
                table: "PointsForVictory",
                column: "StageId",
                principalTable: "Stage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MatchInfo_Stage_StageId",
                table: "MatchInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_PointsForVictory_Stage_StageId",
                table: "PointsForVictory");

            migrationBuilder.RenameColumn(
                name: "StageId",
                table: "PointsForVictory",
                newName: "BracketId");

            migrationBuilder.RenameIndex(
                name: "IX_PointsForVictory_StageId",
                table: "PointsForVictory",
                newName: "IX_PointsForVictory_BracketId");

            migrationBuilder.RenameColumn(
                name: "StageId",
                table: "MatchInfo",
                newName: "BracketId");

            migrationBuilder.RenameIndex(
                name: "IX_MatchInfo_StageId",
                table: "MatchInfo",
                newName: "IX_MatchInfo_BracketId");
            
            //-- These columns still point to the old stage IDs
            migrationBuilder.Sql(@"UPDATE PointsForVictory
                SET PointsForVictory.BracketId = Bracket.Id
                FROM dbo.PointsForVictory AS PointsForVictory
                INNER JOIN dbo.Stage
                    ON PointsForVictory.BracketId = Stage.Id
                INNER JOIN dbo.Bracket
                    ON Stage.BracketId = Bracket.Id");
            migrationBuilder.Sql(@"UPDATE MatchInfo
                SET MatchInfo.BracketId = Bracket.Id
                FROM dbo.MatchInfo AS MatchInfo
                INNER JOIN dbo.Stage
                    ON MatchInfo.BracketId = Stage.Id
                INNER JOIN dbo.Bracket
                    ON Stage.BracketId = Bracket.Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MatchInfo_Bracket_BracketId",
                table: "MatchInfo",
                column: "BracketId",
                principalTable: "Bracket",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PointsForVictory_Bracket_BracketId",
                table: "PointsForVictory",
                column: "BracketId",
                principalTable: "Bracket",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
