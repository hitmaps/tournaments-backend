﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddEventTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Event",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TileUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RegistrationOpens = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RegistrationEnds = table.Column<DateTime>(type: "datetime2", nullable: false),
                    EventEnds = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Rules = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OverlayBackgroundUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OverlayLogoUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DiscordGuildId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DiscordMatchupChannelId = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Event", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EventAdminRole",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EventId = table.Column<int>(type: "int", nullable: false),
                    DiscordRoleId = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventAdminRole", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EventAdminRole_Event_EventId",
                        column: x => x.EventId,
                        principalTable: "Event",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Event_Slug",
                table: "Event",
                column: "Slug",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EventAdminRole_EventId",
                table: "EventAdminRole",
                column: "EventId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventAdminRole");

            migrationBuilder.DropTable(
                name: "Event");
        }
    }
}
