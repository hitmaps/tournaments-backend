﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddStageIdToGroupStageMember : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StageId",
                schema: "GroupStage",
                table: "Member",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Member_StageId",
                schema: "GroupStage",
                table: "Member",
                column: "StageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Member_Stage_StageId",
                schema: "GroupStage",
                table: "Member",
                column: "StageId",
                principalTable: "Stage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Member_Stage_StageId",
                schema: "GroupStage",
                table: "Member");

            migrationBuilder.DropIndex(
                name: "IX_Member_StageId",
                schema: "GroupStage",
                table: "Member");

            migrationBuilder.DropColumn(
                name: "StageId",
                schema: "GroupStage",
                table: "Member");
        }
    }
}
