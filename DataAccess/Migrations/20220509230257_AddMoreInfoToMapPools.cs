﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddMoreInfoToMapPools : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MapSelection_Competitor_CompetitorId",
                table: "MapSelection");

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "MissionForPool",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "MissionForPool",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "CompetitorId",
                table: "MapSelection",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<decimal>(
                name: "DiscordMatchupChannelId",
                table: "Event",
                type: "decimal(20,0)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(20,0)");

            migrationBuilder.AddForeignKey(
                name: "FK_MapSelection_Competitor_CompetitorId",
                table: "MapSelection",
                column: "CompetitorId",
                principalTable: "Competitor",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MapSelection_Competitor_CompetitorId",
                table: "MapSelection");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "MissionForPool");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "MissionForPool");

            migrationBuilder.AlterColumn<int>(
                name: "CompetitorId",
                table: "MapSelection",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DiscordMatchupChannelId",
                table: "Event",
                type: "decimal(20,0)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(20,0)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MapSelection_Competitor_CompetitorId",
                table: "MapSelection",
                column: "CompetitorId",
                principalTable: "Competitor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
