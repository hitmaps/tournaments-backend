﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddStagesToBrackets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Stage",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BracketId = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Stage_Bracket_BracketId",
                        column: x => x.BracketId,
                        principalTable: "Bracket",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Stage_BracketId",
                table: "Stage",
                column: "BracketId");

            migrationBuilder.Sql(@"INSERT INTO dbo.Stage (BracketId, Type)
                SELECT Bracket.Id, 'challonge'
                FROM dbo.Bracket AS Bracket
                INNER JOIN dbo.Event
                    ON Bracket.EventId = Event.Id
                WHERE Event.Type IN ('roulette-rivals', 'ghost-mode')");
            migrationBuilder.Sql(@"INSERT INTO dbo.Stage (BracketId, Type)
                SELECT Bracket.Id, 'list-only'
                FROM dbo.Bracket AS Bracket
                INNER JOIN dbo.Event
                    ON Bracket.EventId = Event.Id
                WHERE Event.Type = 'speedrun-competition'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Stage");
        }
    }
}
