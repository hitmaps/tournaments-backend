using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace DataAccess.Repositories;

public class RepositoryBase
{
    private readonly IConfiguration configuration;

    public RepositoryBase(IConfiguration configuration)
    {
        this.configuration = configuration;
    }

    internal SqlConnection GetConnection()
    {
        return new SqlConnection(configuration.GetConnectionString("Tournament"));
    }
}