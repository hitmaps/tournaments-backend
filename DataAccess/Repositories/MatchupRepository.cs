using Dapper;
using DataAccess.Models;
using Microsoft.Extensions.Configuration;

namespace DataAccess.Repositories;

public class MatchupRepository : RepositoryBase
{
    // @formatter:off
    private const string UpdateMatchupSql = $@"UPDATE MatchInfo SET State = '{MatchInfo.StateNeedsScheduling}', 
                                                    MatchScheduledAt = NULL, 
                                                    FifteenMinuteWarningPosted = 0, 
                                                    MatchAdminDiscordId = NULL 
                                                WHERE Id = @MatchId";
    // @formatter: on
    private const string DeleteMapSelectionSql = "DELETE FROM MapSelection WHERE MatchInfoId = @MatchId";
    private const string DeleteCasterSql = "DELETE FROM Caster WHERE MatchInfoId = @MatchId";
    
    public MatchupRepository(IConfiguration configuration) : base(configuration)
    {
    }

    public async Task ResetMatchupAsync(int matchupId)
    {
        await using var connection = GetConnection();
        connection.Open();

        await connection.ExecuteAsync(DeleteCasterSql, new { MatchId = matchupId });
        await connection.ExecuteAsync(DeleteMapSelectionSql, new { MatchId = matchupId });
        await connection.ExecuteAsync(UpdateMatchupSql, new { MatchId = matchupId });
    }
}